package com.zeronebits.iptv.util.common;

import android.app.AlertDialog;
import android.content.Context;

import com.zeronebits.iptv.R;

public class AlertDialogManager {

	public void showAlertDialog(Context context, String title, String message,
			Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		if (status != null)
			alertDialog
					.setIcon((status) ? R.drawable.success : R.drawable.fail);
		alertDialog.show();

	}
}
