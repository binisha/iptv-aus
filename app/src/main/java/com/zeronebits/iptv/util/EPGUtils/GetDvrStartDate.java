package com.zeronebits.iptv.util.EPGUtils;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.zeronebits.iptv.R;
import com.zeronebits.iptv.entity.Channel;
import com.zeronebits.iptv.util.common.DateUtils;
import com.zeronebits.iptv.util.common.DownloadUtil;
import com.zeronebits.iptv.util.common.LinkConfig;
import com.zeronebits.iptv.util.common.logger.Logger;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;

/**
 * Created by sadip_000 on 17/10/2016.
 */

public abstract class GetDvrStartDate extends AsyncTask<Void,Void,String>{
    private static final String TAG = GetDvrStartDate.class.getSimpleName();
    private Channel channel;
    private Context context;
    private String dvrListLink;
    private ArrayList<String> dateList;
    public GetDvrStartDate(Context context, Channel channel){
        this.context=context;
        this.channel = channel;
        dvrListLink = LinkConfig.getString(context,LinkConfig.DVR_VIDEO_URL);
    }
    public void setDvrStartTime(){
        if(channel.getDvrStartCalendar()!=null)
            onSuccess();
        else
            execute();

    }
    @Override
    protected String doInBackground(Void... params) {
        DownloadUtil getUtc = new DownloadUtil(LinkConfig.GET_UTC,
                                               context);
        String utc = getUtc.downloadStringContent();

        if (!utc.equals(DownloadUtil.NotOnline)
                && !utc.equals(DownloadUtil.ServerUnrechable)) {
            dvrListLink+= "?" + LinkConfig.getHashCode(utc)
                    + "&" + "hasDVR"
                    + "&" + "channelId=" + channel.getId();

            DownloadUtil dUtil = new DownloadUtil(dvrListLink, context);
            Logger.d("DVRLink", dvrListLink);
            return dUtil.downloadStringContent();
        } else
            return utc;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if(result.equalsIgnoreCase(DownloadUtil.NotOnline)||result.equalsIgnoreCase(DownloadUtil.ServerUnrechable)){
            onErrorDvr(R.string.err_code_server_unreachable);
        }else{
            try {
                JSONObject jo = new JSONObject(result);
                channel.setDvrStartCalendar(DateUtils.createCalendar(jo.getString("startDate"),jo.getString("startTime")));
                Logger.d("DVR start time",DateUtils.dateAndTime.format(channel.getDvrStartCalendar().getTime()));

            } catch (JSONException | ParseException e) {
                e.printStackTrace();
                channel.setDvrStartCalendar(null);
                onErrorDvr(R.string.err_code_dvr_not_found);

                Toast.makeText(context,"Dvr Not Available",Toast.LENGTH_SHORT).show();
            }
            onSuccess();
        }
    }

    public abstract void onSuccess();
    public abstract void onErrorDvr(int errorCode) ;
}
