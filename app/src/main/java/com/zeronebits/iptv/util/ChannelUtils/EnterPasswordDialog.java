package com.zeronebits.iptv.util.ChannelUtils;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.zeronebits.iptv.R;
import com.zeronebits.iptv.VideoPlayer.TVPlayCustomController_;
import com.zeronebits.iptv.common.PropertyGetSet;
import com.zeronebits.iptv.entity.Channel;

public class EnterPasswordDialog {
    /**
     * enter password to view locked channel
     *
     * @param context
     * @param channel
     * @param currentCategory
     * @param flag_to_end_activity
     */

    public static void showParentalControlPasswordDialogToPlayChannel(final Context context,
                                                                      final Channel channel, final String currentCategory,
                                                                      final boolean flag_to_end_activity) {

        final Dialog passwordDialog = buildEnterPasswordInterface(context);
        passwordDialog.findViewById(R.id.et_buy_passowrd)
                .setVisibility(View.GONE);
        TextView title = (TextView) passwordDialog
                .findViewById(R.id.dialog_heading);
        title.setText(context.getString(R.string.title_password_parental));
        Button confirm = (Button) passwordDialog.findViewById(R.id.btn_confirm);
        final EditText et_parental_password = (EditText) passwordDialog
                .findViewById(R.id.et_parental_passowrd);

        confirm.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String enteredPassword = et_parental_password.getText()
                        .toString().trim();
                String parentalPassword = PropertyGetSet.getProperty(
                        PropertyGetSet.PARENTAL_PASSWORD).trim();
                if (enteredPassword.equals(parentalPassword)) {
                    passwordDialog.dismiss();
                    Bundle basket = new Bundle();
                    basket.putString("categoryName", currentCategory);

                    Intent i = new Intent(context, TVPlayCustomController_.class);
                    i.putExtras(basket);
                    i.putExtra("currentChannelId", channel.getId());
                    context.startActivity(i);

                    /*LinkConfig.loadChannelLink(context, channel,
                            currentCategory, flag_to_end_activity);*/

                } else {
                    Toast.makeText(context,
                                   "Password Missmatch...Please Try Again",
                                   Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
    /**
     * enter password to unlock parental lock
     *
     * @param context
     * @param channel
     */
/*    public static void showParentalControlPasswordDialogToChangeLockStatus(final Context context,
                                                                           final Channel channel) {
        final Dialog passwordDialog = buildEnterPasswordInterface(context);
        ((EditText) passwordDialog.findViewById(R.id.et_buy_passowrd))
                .setVisibility(View.GONE);
        TextView title = (TextView) passwordDialog
                .findViewById(R.id.dialog_heading);
        title.setText(context.getString(R.string.title_password_parental));
        final EditText et_parental_password = (EditText) passwordDialog
                .findViewById(R.id.et_parental_passowrd);
        Button btnConfirm = (Button) passwordDialog
                .findViewById(R.id.btn_confirm);
        btnConfirm.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (PropertyGetSet.getProperty(
                        PropertyGetSet.PARENTAL_PASSWORD.trim()).equals(
                        et_parental_password.getText().toString().trim())) {
                    passwordDialog.dismiss();
                    ParentalLockUtils.changeChannelParentalStatus(context,
                            channel);

                } else {
                    Toast.makeText(context, "Password Missmatch...Try Again",
                            Toast.LENGTH_SHORT).show();
                }

            }
        });
    }*/

    /**
     * enter password before buy
     */

    /*public static void showBuyPasswordDialog(final Context context,
                                             final Channel channel, final String durationNumber) {
        final Dialog passwordDialog = buildEnterPasswordInterface(context);
        passwordDialog.findViewById(R.id.et_parental_passowrd)
                .setVisibility(View.GONE);
        TextView title = (TextView) passwordDialog
                .findViewById(R.id.dialog_heading);
        title.setText(context.getString(R.string.title_password_buy));
        Button btnConfirm = (Button) passwordDialog
                .findViewById(R.id.btn_confirm);
        final EditText passwordField = (EditText) passwordDialog
                .findViewById(R.id.et_buy_passowrd);

        btnConfirm.setText(context.getString(R.string.btn_confirm));

        btnConfirm.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (new SecurityUtils()
                        .getDecryptedToken(FileManager.getUserPassword()).trim()
                        .equals(passwordField.getText().toString().trim())) {
                    BuyUtils.buyChannel(context,channel,durationNumber);
                    passwordDialog.dismiss();

                } else {
                    Toast.makeText(context, "Password MisMatch",
                            Toast.LENGTH_LONG).show();
                }

            }
        });
    }*/
    public static Dialog buildEnterPasswordInterface(Context context) {
        final Dialog passwordDialog = new Dialog(context);
        passwordDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        passwordDialog.setContentView(R.layout.dialog_enter_password);
        TextView title = (TextView) passwordDialog
                .findViewById(R.id.dialog_heading);
        title.setText(context.getString(R.string.app_name));
        passwordDialog.show();

        passwordDialog.findViewById(R.id.btn_dismiss)
                .setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        passwordDialog.dismiss();

                    }
                });

        return passwordDialog;

    }

    /*public static void showParentalControlPasswordDialogToPlayDVR(final Context context, final Channel channel,final String currentCategory){
        final Dialog passwordDialog = buildEnterPasswordInterface(context);
        passwordDialog.findViewById(R.id.et_buy_passowrd)
                .setVisibility(View.GONE);
        TextView title = (TextView) passwordDialog
                .findViewById(R.id.dialog_heading);
        title.setText(context.getString(R.string.title_password_parental));
        Button confirm = (Button) passwordDialog.findViewById(R.id.btn_confirm);
        final EditText et_parental_password = (EditText) passwordDialog
                .findViewById(R.id.et_parental_passowrd);

        confirm.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String enteredPassword = et_parental_password.getText()
                        .toString().trim();
                String parentalPassword = PropertyGetSet.getProperty(
                        PropertyGetSet.PARENTAL_PASSWORD).trim();
                if (enteredPassword.equals(parentalPassword)) {
                    passwordDialog.dismiss();
                    DVRListLoad dvrListLoad = new DVRListLoad(context, channel, currentCategory,6);
                    dvrListLoad.loadDVRList();

                } else {
                    Toast.makeText(context,
                            "Password Missmatch...Please Try Again",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }*/
}
