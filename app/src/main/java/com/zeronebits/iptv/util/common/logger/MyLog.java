package com.zeronebits.iptv.util.common.logger;

import android.app.Activity;
import android.os.Environment;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;

public class MyLog {
	private static File file;
	private static final String fileName = "TOTAL_LOG";
	
	
	public static void d( String tag, String message, Activity activity ) {
		Logger.d(tag, message);
		
		try {
			file = new File( Environment.getExternalStorageDirectory() , fileName );
			
			Toast.makeText( activity, "Writing to file at " + file.toString(), Toast.LENGTH_LONG ).show();
				
			Calendar calendar = Calendar.getInstance();
			
			String msgString = calendar.getTime().toString() + "\t" + calendar.getTimeInMillis() + "\t" + ( calendar.getTimeInMillis() % 10000000 ) + "\t" + tag + "\t" + message + "\n";
			
			file.createNewFile();
			
			BufferedWriter writer = new BufferedWriter( new FileWriter( file, true ) );
			
			writer.append( msgString );
			
			writer.flush();
			
			writer.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Logger.e("MYLOG", "Cannot create the log file." );
			Toast.makeText( activity, "Exception: " + e.getMessage(), Toast.LENGTH_LONG ).show();
		}
	}
}
