package com.zeronebits.iptv.util;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.widget.Toast;

import com.zeronebits.iptv.common.ApkDownloader;
import com.zeronebits.iptv.entity.MarketApp;
import com.zeronebits.iptv.util.common.CustomDialogManager;
import com.zeronebits.iptv.util.common.PackageUtils;
import com.zeronebits.iptv.util.common.logger.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MarketAppDetailParser {
    public static String MyAccountAppPackage = "com.totalcable.myaccount"
            .trim();
    public static String MyNITVSettings = "com.totalcable.mynitvsetting";
    public static HashMap<String, MarketApp> APKs;
    public static ArrayList<String> packageNames;
    private Context context;
    private String jsonstr;

    public MarketAppDetailParser(Context context, String jsonstr) {
        this.context = context;
        this.jsonstr = jsonstr;
        APKs = new HashMap<String, MarketApp>();
        packageNames = new ArrayList<String>();
    }

    public static ArrayList<MarketApp> ApkListWithoutCurrentApk(Context context) {
        ArrayList<MarketApp> apklist = new ArrayList<MarketApp>();

        if (packageNames != null) {
            for (int i = 0; i < MarketAppDetailParser.packageNames.size(); i++) {
                String packageNamefromApk = packageNames.get(i);
                MarketApp ma = MarketAppDetailParser.APKs
                        .get(packageNamefromApk);

                Logger.d("apk name", ma.getName() + "");
                String packagename = "";
                try {
                    PackageInfo pInfo = context.getPackageManager().getPackageInfo(
                            context.getPackageName(), 0);
                    packagename = pInfo.packageName;
                } catch (NameNotFoundException e) {
                    e.printStackTrace();
                }
                if (!ma.getVisibility() || packageNames.get(i).equalsIgnoreCase(packagename))
                    continue;
                else
                    apklist.add(ma);
                // MarketAppDetailParser.packageNames.get(i);
            }
        }
        return apklist;
    }

    public static void openApk(Context context, String packageName) {
        Logger.d("inside", "open apk");
        if (PackageUtils.isPackageInstalled(context, packageName)) {
            Intent openApk = context.getPackageManager()
                    .getLaunchIntentForPackage(packageName);
            context.startActivity(openApk);
        } else {
            try {
                Logger.d("package to install", packageName);
                Logger.d("app to install", APKs.get(packageName).getName());
                MarketApp ma = APKs.get(packageName);
                new ApkDownloader(context, APKs.get(packageName)
                        .getName()).execute(APKs.get(packageName)
                                                           .getAppDownloadLink());


            } catch (Exception e2) {
                e2.printStackTrace();
                CustomDialogManager apkNotFound = new CustomDialogManager(
                        context, CustomDialogManager.ALERT);
                apkNotFound.build();
                apkNotFound
                        .setMessage("App Not Found! \n Contact your Service Provider!!!");

                apkNotFound
                        .addDissmissButtonToDialog();
                try {
                    apkNotFound.show();
                } catch (Exception e) {
                    Toast.makeText(context, "App Not Found! Contact your Service Provider!", Toast.LENGTH_SHORT).show();
                }

            }

        }

    }

    public void parse() throws JSONException {

        JSONObject jObj = new JSONObject(jsonstr);
        JSONArray dataobj = jObj.getJSONArray("data");
        for (int i = 0; i < dataobj.length(); i++) {
            JSONObject apkObj = dataobj.getJSONObject(i);
            MarketApp marketApp = new MarketApp();

            marketApp.setName(apkObj.getString("name"));
            marketApp.setAppDownloadLink(apkObj.getString("download"));
            marketApp.setAppImageLink(apkObj.getString("select_image"));
            marketApp.setVisibility(apkObj.getBoolean("visibility"));
            marketApp.setVersion(apkObj.getString("version"));
            marketApp.setAppPackageName(apkObj.getString("link"));
            marketApp.setVersionCode(apkObj.getInt("versionCode"));
            Logger.d("CheckingVersionCode",apkObj.getInt("versionCode") + "");
            packageNames.add(marketApp.getAppPackageName());
            APKs.put(marketApp.getAppPackageName(), marketApp);

        }


    }

}
