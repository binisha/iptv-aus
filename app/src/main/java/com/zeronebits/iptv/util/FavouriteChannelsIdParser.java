package com.zeronebits.iptv.util;

import android.content.Context;

import com.zeronebits.iptv.util.common.logger.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FavouriteChannelsIdParser {
	Context context;
	String jsonString;

	public FavouriteChannelsIdParser(Context context, String jsonString) {
		this.context = context;
		this.jsonString = jsonString;
	}

	public void parse() {
		try {
			JSONObject str = new JSONObject(jsonString);
			Boolean status = str.getBoolean("status");
			if (status) {
				JSONArray channel_list = str.getJSONArray("channel_list");
				for (int i = 0; i < channel_list.length(); i++) {
					JSONObject jo = channel_list.getJSONObject(i);
					try {
						int favChannelId = jo.getInt("channel_id");
						Logger.d("favourite channel id", favChannelId + "");
						AllParser.getChannelFromId(favChannelId)
								.setFavourite(1);
					} catch (JSONException je) {
						je.printStackTrace();
					} catch (NullPointerException ne) {
						ne.printStackTrace();
						continue;

					}
				}
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
