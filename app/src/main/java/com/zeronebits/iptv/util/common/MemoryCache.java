package com.zeronebits.iptv.util.common;

import android.graphics.Bitmap;

import java.lang.ref.SoftReference;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MemoryCache {
	private Map<String, SoftReference<Bitmap>> cache = Collections
			.synchronizedMap(new HashMap<String, SoftReference<Bitmap>>());

	public Bitmap get(String id) {
		if (!cache.containsKey(id))
			return null;
		SoftReference<Bitmap> ref = cache.get(id);
		return ref.get();
	}

	public void put(String id, Bitmap bitmap) {
		cache.put(id, new SoftReference<Bitmap>(bitmap));
	}

	public void clear() {
		cache.clear();
	}
	
	public int memoryCacheSize() {
		return cache.size();
	}
	
	public String getKeySets() {
		String keys = "";
		Set<String> keySet = cache.keySet();
		
		for( String s : keySet ) {
			keys += s + ",";
		}
		
		return keys;
	}
}
