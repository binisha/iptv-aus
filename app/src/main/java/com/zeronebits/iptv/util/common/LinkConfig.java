package com.zeronebits.iptv.util.common;

import android.content.Context;

import com.zeronebits.iptv.EntryPoint;
import com.zeronebits.iptv.R;
import com.zeronebits.iptv.asyn.GroupDataParser;
import com.zeronebits.iptv.util.common.logger.Logger;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class LinkConfig {





	public static final String CHECK_IF_SERVER_RECHABLE = "46.101.255.243";
	public static final String LINK_SEVER_APKs = "http://46.101.255.243/iptv/mybuilt/MarketAppLauncher.php";
	// URLs used in first page i.e entrypoint and login and register
	public static final int MAC_EXISTS = R.string.mac_exists_url;
	public static final int LOGIN_BUTTON_CLICK = R.string.login_btn_click_url;
	public static final int CHECK_VALIDITY_ACTIVATION_APPROVAL = R.string.check_validity_activation_approval_url;
	public static final int GET_SESSION = R.string.get_session_url;
	public static final int CATEGORY_CHANNEL_URL = R.string.category_channel_url;

	public static final int VERSION_CHECK = R.string.version_check_url;
	// URLS used in LiveTV
	public static final int CATEGORY_URL = R.string.category_channels_url;
	public static final int FAV_CHANNELS_ID_URL = R.string.favourite_channels_idurl;
	public static final int PARENTAL_LOCK_GET_SET_URL = R.string.parental_lock_get_set_url;
	public static final int BANNER_URL = R.string.banner_url;
	public static final int CHANNEL_LINK_URL = R.string.channel_link_url;
	public static final int AD_URL = R.string.ad_url;
	public static final int FAV_UNFAV_URL = R.string.fav_unfav_url;
	public static final int NEWS_URL = R.string.news_url;
	public static final int BUY_URL = R.string.buy_url;
	public static final int UPGRADE_URL = R.string.upgrade_url;
	public static final int EPG_URL = R.string.epg_url;
	public static final int DVR_URL = R.string.dvr_url;
	public static final int DVR_LIST_URL = R.string.dvr_list_url;
	public static final int DVR_VIDEO_URL = R.string.dvr_video_url;
	// URLs used in MovieParentCategories page
	public static final int GROUP_DATA = R.string.group_data_url;
	public static final int MULTI_LOGIN = R.string.multilogin_url;
//	public static final int MOVIE_PARENT_CATEGfORY = R.string.movie_parent_category_ddurl;
	// URLs used is Movies page
	public static final int MOVIE_CATEGORY_NAME_LIST = R.string.movie_category_name_url;
	public static final int MOVIES_LIST = R.string.movie_list_url;
	// URLs used is MovieDetails page
	public static final int MOVIE_DETAILS = R.string.movie_details_url;
	// URL used to play movie
	public static final int MOVIE_PLAY_LINK = R.string.movie_play_url;
	public static final int ALLOW_COUNTRY = R.string.allow_country;
	public static String BASE_URL = "http://46.101.255.243/iptv/";
	public static final String GET_UTC = BASE_URL+"json/other/get_utc.php";
	public static final String SCREEN_SAVER_LINK = "http://46.101.255.243/iptv/json/other/screen_saver_banners.php";

	public static final int GROUP_LIST = R.string.group_list_url;
	public static final int PACKAGE_DATA =R.string.package_data_url;
	public static final int COUNTRY_DATA =R.string.country_data_url;
	public static final int REGISTER_BUTTON_CLICK = R.string.register_btn_click_url;

	public static String getHashCode(String utc) {
		String sessionId = null;
		try {
//			if (LoginFileUtils.readFromFile(EntryPoint.macAddress)) {
				sessionId = EntryPoint.sessionId;
				Logger.e("session id in hash", sessionId);
				System.out.println(sessionId +"hash");
				if (sessionId != null || GroupDataParser.groupData.getUserId()+""!= null) {

					String SecretKey = "123456789";
					Logger.d("utc time", utc + "");
					String stringToMD5 = SecretKey + sessionId
							+ GroupDataParser.groupData.getUserId()+"" + utc;
					String hexString = md5(stringToMD5);
					return "utc=" + utc.trim() + "&userId=" + GroupDataParser.groupData.getUserId()+""
							+ "&hash=" + hexString.toString()+ Utils.getAppInfo();
				} else {
					return null;
				}
//			} else {
//				return null;
//			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private static final String md5(final String s) {
		try {
			// Create MD5 Hash
			MessageDigest digest = MessageDigest
					.getInstance("MD5");
			digest.update(s.getBytes());
			byte messageDigest[] = digest.digest();

			// Create Hex String
			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < messageDigest.length; i++) {
				String h = Integer.toHexString(0xFF & messageDigest[i]);
				while (h.length() < 2)
					h = "0" + h;
				hexString.append(h);
			}
			return hexString.toString();

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return "";
	}

	/*public static void loadChannelLink(Context context, Channel channel,
			String currentCategory, boolean flag_to_end_activity) {
		final String channelLinkUrl = LinkConfig.getString(context,
				LinkConfig.CHANNEL_LINK_URL) + "?";
		new ChannelLinkLoader(context, channel, currentCategory + "",
				flag_to_end_activity).execute(channelLinkUrl + "channelId="
				+ channel.getId() + "&ip_address="
				+ UtilsIpMac.getIpAddress() + "&");

	}*/

	public static String getString(Context context, int resId) {
		return BASE_URL + context.getResources().getString(resId);


	}

}
