package com.zeronebits.iptv.util;

import android.content.Context;

import com.zeronebits.iptv.R;

/**
 * Created by sadip_000 on 12/09/2016.
 */
public class ChannelErrorUtils {
    public static String getErrorMessageFromCode(Context context, int code){
        switch (code){
            case R.string.err_code_json_exception:
                return context.getResources().getString(R.string.err_json_exception);
            case R.string.err_code_server_unreachable:
                return context.getResources().getString(R.string.err_server_unreachable);
            case R.string.war_code_buy_before_watch:
                return context.getResources().getString(R.string.war_buy_before_watch);
            case R.string.war_code_upgrade_before_watch:
                return context.getResources().getString(R.string.war_upgrade_before_watch);
            case R.string.war_code_no_channels_in_db:
                return context.getResources().getString(R.string.war_no_channels_in_db);
            case R.string.err_code_epg_not_found:
                return context.getString(R.string.err_epg_not_found);
            case R.string.err_code_dvr_not_found:
                return context.getString(R.string.err_dvr_not_found);
        }
        return null;
    }
    public static String getMediaErrorMessage(Context context, String code){
        if(code.contains(context.getString(R.string.err_code_media_error))){
            return context.getString(R.string.err_media_error);
        }else return null;
    }


}
