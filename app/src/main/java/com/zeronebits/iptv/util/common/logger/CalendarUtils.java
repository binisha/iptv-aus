package com.zeronebits.iptv.util.common.logger;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class CalendarUtils {
	public synchronized static String getFullMonth(int month) {
		switch(month) {
		case 0:
			return "January";
		case 1:
			return "February";
		case 2:
			return "March";
		case 3:
			return "April";
		case 4:
			return "May";
		case 5:
			return "June";
		case 6:
			return "July";
		case 7:
			return "August";
		case 8:
			return "September";
		case 9:
			return "October";
		case 10:
			return "November";
		case 11:
			return "December";
		}
		return "Invalid month";
	}
	
	public synchronized static String getShortMonth(int month) {
		switch(month) {
		case 0:
			return "Jan";
		case 1:
			return "Feb";
		case 2:
			return "Mar";
		case 3:
			return "Apr";
		case 4:
			return "May";
		case 5:
			return "Jun";
		case 6:
			return "Jul";
		case 7:
			return "Aug";
		case 8:
			return "Sep";
		case 9:
			return "Oct";
		case 10:
			return "Nov";
		case 11:
			return "Dec";
		}
		return "Invalid month";
	}
	
	public static String pad(int num) {
		if(num<10)
			return new String("0"+num);
		else 
			return new String(""+num);
	}
	
	public synchronized static String getCalString(long millis) {
		String timestamp="";
		Calendar cal=new GregorianCalendar();
		cal.setTimeInMillis(millis);		
		timestamp=getShortMonth(cal.get(Calendar.MONTH))+" "+cal.get(Calendar.DAY_OF_MONTH)+", "+cal.get(Calendar.YEAR)+" "+
					pad(cal.get(Calendar.HOUR_OF_DAY))+":"+pad(cal.get(Calendar.MINUTE))+":"+pad(cal.get(Calendar.SECOND));		
		return timestamp;
	}
}
