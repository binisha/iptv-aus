package com.zeronebits.iptv.util;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.zeronebits.iptv.entity.Category;
import com.zeronebits.iptv.entity.Channel;
import com.zeronebits.iptv.util.common.logger.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Set;

public class AllParser implements Comparator<Channel> {
	private static final String TAG = "AppParser" +
			"";
	//	public static String TYPE_PURCHASE_DEFAULT = "default";
//	public static String TYPE_PURCHASE_BUY = "buy";
//	public static String TYPE_PURCHASE_BOUGHT = "bought";
	public static ArrayList<String> categorynames;
	public static HashMap<String, ArrayList<Channel>> category_channel;
	public static ArrayList<Category> allcategories;

	private String jsonString;
	private ArrayList<Channel> channellist;
	private Context context;

	public AllParser(Context context, String jsonString) {
		this.context = context;
		this.jsonString = jsonString;
		category_channel = new HashMap<String, ArrayList<Channel>>();
		categorynames = new ArrayList<String>();
        allcategories = new ArrayList<Category>();
        categorynames.add("All");
		categorynames.add("Favourite");
		allcategories.add(new Category("All", 100));
		allcategories.add(new Category("Favourite", 101));

	}

	public static Channel getChannelFromId(int channel_id) {

		for (Channel channel : AllParser.getAllChannels()) {
			if (channel.getId() == channel_id)
				return channel;
		}
		return null;
	}

	public static Channel getNextChannel(int currentChannel,
										 String categoryName) {
		Channel nextChannel = null;
		int nextChannelPosition;
		ArrayList<Channel> channelsUnderCategories;
//		if(categoryName.equalsIgnoreCase("My Purchase")){
//			channelsUnderCategories = AllParser.myPurhcasedChannels();
//		}else
		if (categoryName.equalsIgnoreCase("all")) {
			channelsUnderCategories = AllParser.getAllChannels();
		} else if (categoryName.equalsIgnoreCase("Favourite")) {
			channelsUnderCategories = AllParser.favoriteCategoryChannels();
		} else {
			channelsUnderCategories = AllParser.category_channel
					.get(categoryName);
		}
		for (int i = 0; i < channelsUnderCategories.size(); i++) {
			if (currentChannel == channelsUnderCategories.get(i)
					.getId()) {
				try {
					nextChannel = channelsUnderCategories.get(i + 1);
					nextChannelPosition = i + 1;
				} catch (IndexOutOfBoundsException ide) {
					nextChannel = channelsUnderCategories.get(0);
					nextChannelPosition = 0;
				}
				break;
			}
		}
		return nextChannel;

	}

	public static ArrayList<Channel> favoriteCategoryChannels() {
		ArrayList<Channel> favchannels = new ArrayList<Channel>();
		for (int i = 0; i < getAllChannels().size(); i++) {
			Channel ch = getAllChannels().get(i);
			if (ch.getFavourite() == 1)
				favchannels.add(ch);
			else
				continue;
		}

		return favchannels;

	}

	public static ArrayList<Channel> getAllChannels() {
		ArrayList<Channel> allChannels = new ArrayList<>();
		if(category_channel!=null) {


			Set<String> keySet = category_channel.keySet();
			for (String category : keySet) {
//				Log.d(TAG, "getAllChannels: "+category);
//				Log.d(TAG, "getAllChannels: "+category_channel);
//				Log.d(TAG, "getAllChannels: "+category_channel.get(category));
				ArrayList<Channel> channelsInCategory = category_channel.get(category);
				Log.d(TAG, "getAllChannels: "+channelsInCategory.size());
				if(channelsInCategory!=null)
				allChannels.addAll(category_channel.get(category));
			}
			Collections.sort(allChannels, new Comparator<Channel>() {
				@Override
				public int compare(Channel lhs, Channel rhs) {
					return lhs.getChannelPriority() - rhs.getChannelPriority();
				}
			});
		}

		return allChannels;
	}

//	public static ArrayList<Channel> myPurhcasedChannels() {
//		ArrayList<Channel> myChannel = new ArrayList<Channel>();
//		for (Channel channel:allChannels){
//			if (channel.getChannelPurchaseStatus()!=1){
//				myChannel.add(channel);
//			}
//		}
//		return myChannel;
//	}


	public static Channel getPrevChannel(int currentChannel,
										 String categoryName) {
		Channel prevChannel = null;
		int prevChannelPosition;
		ArrayList<Channel> channelsUnderCategories;
//		if(categoryName.equalsIgnoreCase("My Purchase")){
//			channelsUnderCategories = AllParser.myPurhcasedChannels();
//		}else
		if (categoryName.equalsIgnoreCase("all")) {
			channelsUnderCategories = AllParser.getAllChannels();
		} else if (categoryName.equalsIgnoreCase("Favourite")) {
			channelsUnderCategories = AllParser.favoriteCategoryChannels();
		} else {
			channelsUnderCategories = AllParser.category_channel
					.get(categoryName);
		}
		for (int i = 0; i < channelsUnderCategories.size(); i++) {
			if (currentChannel == channelsUnderCategories.get(i)
					.getId()) {
				try {
					prevChannel = channelsUnderCategories.get(i - 1);
					prevChannelPosition = i - 1;
				} catch (IndexOutOfBoundsException ide) {
					prevChannel = channelsUnderCategories
							.get(channelsUnderCategories.size() - 1);
					prevChannelPosition = channelsUnderCategories.size() - 1;
				}
				break;
			}
		}
		return prevChannel;

	}

	public static int getPositionOfChannelInCategory(Channel channel,
													 String categoryName) {
		ArrayList<Channel> channelsUnderCategories;
//		if(categoryName.equalsIgnoreCase("My Purchase")){
//			channelsUnderCategories = AllParser.myPurhcasedChannels();
//		}else
		if (categoryName.equalsIgnoreCase("all")) {
			channelsUnderCategories = AllParser.getAllChannels();
		} else if (categoryName.equalsIgnoreCase("Favourite")) {
			channelsUnderCategories = AllParser.favoriteCategoryChannels();
		} else {
			channelsUnderCategories = AllParser.category_channel
					.get(categoryName);
		}
		for (int positionOfChannel = 0; positionOfChannel < channelsUnderCategories
				.size(); positionOfChannel++) {
			if (channel.getId() == channelsUnderCategories.get(
					positionOfChannel).getId())
				return positionOfChannel;
		}
		return 0;

	}

	public void parse() throws JSONException {
		Logger.d("JSON STRING", jsonString);
		JSONObject root = new JSONObject(jsonString);

		JSONArray categories =root.getJSONArray("categories");
		Logger.d("length of json array of categories", categories.length() + "");
		if (categories.length() == 0) {
			Toast.makeText(context, "No channels available for your package",
					Toast.LENGTH_LONG).show();
		} else {
			for (int i = 0; i < categories.length(); i++) {
                // parsing of categories
//                int categoryId = categoryJson.getInt("category_id");
				String categoryName = (String) categories.get(i);

                categorynames.add(categoryName);
                Category category = new Category(categoryName, i);
                allcategories.add(category);
				// parsing of channels inside the categories
				try {
					JSONObject items = root.getJSONObject("items");
					for (int j = 0; j < categorynames.size(); j++) {
						if ("all".equalsIgnoreCase(categorynames.get(j))||"favourite".equalsIgnoreCase(categorynames.get(j)))
							continue;
						JSONArray channelsInCategories = items
								.getJSONArray(categorynames.get(j));
						ArrayList<Channel> channelListUnderCategories = new ArrayList<Channel>();
						for (int totalChannels = 0; totalChannels < channelsInCategories
								.length(); totalChannels++) {

							JSONObject channelsJson = channelsInCategories
									.getJSONObject(totalChannels);

							Channel channel = new Channel();
							channel.setExpiryDate(null);
							channel.setId(channelsJson.getInt("channel_id"));
							channel.setName(channelsJson
									.getString("channel_name"));
							channel.setChannelPriority(channelsJson
									.getInt("channel_priority"));
							try {
								channel.setChannelLanguage(channelsJson
										.getString("channel_language"));
								channel.setChannelCountry(channelsJson
										.getString("channel_country"));
								channel.setChannelDescription(channelsJson
										.getString("channel_desc"));
							} catch (JSONException je) {
								channel.setChannelLanguage("N/A");
								channel.setChannelCountry("N/A");
								channel.setChannelDescription("N/A");
								je.printStackTrace();
							}

							try{
								if(channelsJson.getString("favorite").equals("1")) {
									channel.setFavourite(1);
								}else{
									channel.setFavourite(0);
								}
							}catch (Exception e){

							}

							channel.setImgLink("http://"+channelsJson.getString("channel_logo"));
//							channel.setImgLink("http://mntvbox.com/channel_logo/168.png");


							channelListUnderCategories.add(channel);


						}
						category_channel.put(categoryName,
								channelListUnderCategories);
					}


				} catch (JSONException je) {
					je.printStackTrace();
					continue;
				}

			}
			Collections.sort(getAllChannels(), this);
			Logger.d("ALL Channels at all parser",getAllChannels().size()+"");

		}

	}


    public static Integer getCategoryPosition(int categoryId){
        for(int i = 0; i < allcategories.size();i++){
            Category category = allcategories.get(i);
            Logger.d("CheckingCategoryPositionOKSDSA",category.getCategoryId()+"");

            if(category.getCategoryId() == categoryId){
				return i;
            }
        }
        return  null;
    }
	@Override
	public int compare(Channel lhs, Channel rhs) {
		return lhs.getChannelPriority() - rhs.getChannelPriority();
	}
}
