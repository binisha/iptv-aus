package com.zeronebits.iptv.util.common;

import android.content.Context;

import java.util.ArrayList;
import java.util.HashMap;

public class ArrayListBean {

	Context mContext;
	
	
	
	private ArrayList<HashMap<String , Object>> arrayList;

	public ArrayList<HashMap<String, Object>> getArrayList() {
		return arrayList;
	}

	

	public ArrayListBean(Context mContext,
			ArrayList<HashMap<String, Object>> arrayList) {
		super();
		this.mContext = mContext;
		this.arrayList = arrayList;
	}



	public ArrayListBean(Context mContext) {
		super();
		this.mContext = mContext;
	}
	
	
	
	
}
