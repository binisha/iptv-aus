package com.zeronebits.iptv.util.common;

import android.os.Environment;

import com.zeronebits.iptv.util.SecurityUtils;
import com.zeronebits.iptv.util.common.logger.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class LoginFileUtils {
	public static final String APP_CONFIG_FILE_NAME = "total_login";
	private static String userEmail;
	private static String userToken;
	private static String sessionId;

	public static String getUserEmail() {
		return userEmail;
	}

	public static String getUserToken() {
		return userToken;
	}

	public static String getSessionId() {
		return sessionId;
	}

	public static boolean reWriteLoginDetailsToFile(String macAddress,
			String userEmai, String userPassword, String sessionId) {

		Logger.d("CheckingTokenRewrite",userPassword);
		if (Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED)) {
			SecurityUtils securityUtils = new SecurityUtils();
			String st = securityUtils.getEncryptedToken(macAddress) + "\n" + securityUtils.getEncryptedToken(userEmai) + "\n"	+ securityUtils.getEncryptedToken(userPassword) + "\n" + securityUtils.getEncryptedToken(sessionId);
			
			Logger.i("STRING_TO_WRITE", st);
			
			File externalStorageDir = Environment.getExternalStorageDirectory();
			File myFile = new File(externalStorageDir, APP_CONFIG_FILE_NAME);

			try {
				FileOutputStream fOut1 = new FileOutputStream(myFile);
				OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut1);
				myOutWriter.append(st);
				myOutWriter.close();
				fOut1.close();
			} catch (Exception e) {
				e.printStackTrace();
			}

			return true;
		} else {
			return false;
		}

	}

	public static boolean readFromFile(String macAddress) {
		SecurityUtils securityUtils = new SecurityUtils();

		if (Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED)) {
			File externalStorageDir = Environment.getExternalStorageDirectory();
			File myFile = new File(externalStorageDir, APP_CONFIG_FILE_NAME);
			if (myFile.exists()) {

				try {
					BufferedReader br = new BufferedReader(
							new InputStreamReader(new FileInputStream(myFile)));
					
					String data;
					StringBuffer sb = new StringBuffer();
					while ((data = br.readLine()) != null) {
						sb.append(data + ",");
					}
					Logger.d("FILE CONTAINS", sb.toString());
					String str = sb.toString().replace(",,", "," );
					String[] both = str.split(",");
					if (securityUtils.getDecryptedToken(both[0].toString()).equals(macAddress)) {
						userEmail = securityUtils.getDecryptedToken(both[1].toString());
						Logger.d("CheckingEmailDecrpted",userEmail);
						userToken = securityUtils.getDecryptedToken(both[2].toString());
						sessionId = securityUtils.getDecryptedToken(both[3].toString());
						return true;
					}
					// Toast.makeText(MainActivity.this,both[0].toString(),
					// Toast.LENGTH_SHORT).show();
				} catch (FileNotFoundException e) {
					e.printStackTrace();
					return false;
				} catch (IOException e) {
					e.printStackTrace();
					return false;
				}
			}
			return false;

		} else
			return false;
	}
}
