package com.zeronebits.iptv.util.common;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.zeronebits.iptv.util.common.logger.Logger;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.List;


public class DownloadUtil {

	public static String NotOnline = "1";
	public static String ServerUnrechable = "0";
	private static final String TAG = DownloadUtil.class.getSimpleName();

	private String link;
	private Context context;
	int links;
	private String encoding = "utf-8";

	public DownloadUtil(String link, Context context) {
		this.link = link;
		this.context = context;
	}



	public String downloadStringContent() {
		if (isOnline()) {
			if (isServerReachable(LinkConfig.CHECK_IF_SERVER_RECHABLE)) {

				StringBuilder result = new StringBuilder();

				String responseString = "";
				try {
					URL url = new URL(link);
					HttpURLConnection connection = (HttpURLConnection) url.openConnection();
					connection.connect();
					InputStream stream = connection.getInputStream();
					BufferedReader reader = new BufferedReader(new InputStreamReader(stream));

					String line;
					while ((line = reader.readLine()) != null) {
						result.append(line);
					}

				} catch (MalformedURLException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				} catch (Exception e) {
					e.printStackTrace();
				}
				Log.d("the json data is ", result.toString());
				return result.toString();

			} else
				return ServerUnrechable;
		} else
			return NotOnline;

	}


	public String postDataAndGetStringResponse( List<NameValuePair> nameValuePairs ) {
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(link );

		try {

			post.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			HttpResponse response = client.execute(post);

			InputStream is = response.getEntity().getContent();

			String result = "";

			if (is != null) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(is));

				String l = "";
				while ((l = reader.readLine()) != null) {
					result += l;
				}

				reader.close();
			}

			is.close();

			return result;
		} catch (Exception e) {
			e.printStackTrace();
			return ServerUnrechable;
		}


	}

	private boolean isServerReachable(String value)
	// To check if server is reachable
	{

		try {
			InetAddress.getByName(value).isReachable(3000);
			Logger.i("ip",InetAddress.getAllByName(value).toString());
			// //Replace with your name
			Logger.w(TAG, "connection established");
			return true;
		} catch (UnknownHostException e) {
			Logger.e(TAG, e.getMessage());
			return false;
		} catch (IOException e) {
			Logger.e(TAG, e.getMessage());
			return false;
		}
	}

	private boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

}