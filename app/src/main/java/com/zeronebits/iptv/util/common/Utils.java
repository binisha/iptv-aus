package com.zeronebits.iptv.util.common;

import android.os.Build;

import com.zeronebits.iptv.BuildConfig;

import java.io.InputStream;
import java.io.OutputStream;

public class Utils {
    public static void CopyStream(InputStream is, OutputStream os)
    {
        final int buffer_size=1024;
        try
        {
            byte[] bytes=new byte[buffer_size];
            for(;;)
            {
              int count=is.read(bytes, 0, buffer_size);
              if(count==-1)
                  break;
              os.write(bytes, 0, count);
            }
        }
        catch(Exception ex){}
    }


    public static String getAppInfo() {

        try {
            String versionName = BuildConfig.VERSION_NAME;
            int versionCode = BuildConfig.VERSION_CODE;
            String appName = "Live_tv";
            String buildNo = Build.DEVICE;
            String buildFinger = Build.FINGERPRINT;
            String androidVersion = Build.VERSION.RELEASE;

            return "&version_name=" + versionName + "&version_code=" + versionCode + "&app_name=" + appName + "&android_version=" +androidVersion;
        } catch (Exception e) {
            return "&no_value=null";
        }
    }


    public static String getAppInfoForMarkeApp() {

        try {
            String versionName = BuildConfig.VERSION_NAME;
            int versionCode = BuildConfig.VERSION_CODE;
            String appName = "Live_tv";
            String buildNo = Build.DEVICE;
            String buildFinger = Build.FINGERPRINT;
            String androidVersion = Build.VERSION.RELEASE;

            return "?version_name=" + versionName + "&version_code=" + versionCode + "&app_name=" + appName + "&android_version=" +androidVersion;
        } catch (Exception e) {
            return "?no_value=null";
        }
    }


}