package com.zeronebits.iptv.util;


import com.zeronebits.iptv.entity.EPG;
import com.zeronebits.iptv.util.common.DateUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class EPGParser {

	private String jsonString;

	private ArrayList<EPG> EPGList;
	private HashMap<String, ArrayList<EPG>> EPGHash;
	String[] days = { "Sunday", "Monday", "Tuesday", "Wednesday",
			"Thursday", "Friday", "Saturday" };

	public EPGParser(String jsonString) {
		this.jsonString = jsonString;
		EPGHash = new HashMap<String, ArrayList<EPG>>();
		EPGList = new ArrayList<EPG>();
	}


	public void parseEPG() throws JSONException{
		JSONObject jo = new JSONObject(jsonString);
		for(int i = 0 ; i < 7; i++){
			JSONArray dayArray = jo.getJSONArray(days[i]);
			EPGList = new ArrayList<EPG>();
			for(int programs = 0 ; programs < dayArray.length() ; programs++){
				EPG content = new EPG(/*channelId*/);

				String programName = dayArray.getJSONObject(programs).getString("program_name");
				content.setProgram_name(programName);
				String start_time = dayArray.getJSONObject(programs).getString("start_time");
				content.setStart_time(start_time);
				String end_time = dayArray.getJSONObject(programs).getString("end_time");
				content.setEnd_time(end_time);
				String prgmDay = dayArray.getJSONObject(programs).getString("day");
				content.setDay(prgmDay);
				EPGList.add(content);

			}
			EPGHash.put(days[i], EPGList);
		}
	}

	public HashMap<String, ArrayList<EPG>> getHashmapDays() {
		return this.EPGHash;
	}
	public static StringBuilder dataOfDayToString(String day, HashMap<String, ArrayList<EPG>> hashMap){
		ArrayList<EPG> list   = hashMap.get(day);
		StringBuilder sb = new StringBuilder();
		sb.append(list.get(0).getDay());
		for(EPG epg: list){
			sb.append(epg.toString());
		}
		return sb;
	}
	public static EPG getNextEpg(Calendar calendar,ArrayList<EPG> epgList) throws ParseException {
		Date currentTime = DateUtils._24HrsTimeFormat.parse(calendar.get(Calendar.HOUR)+":"+calendar.get(Calendar.MINUTE));
		for(int i = 0; i < epgList.size();i++){
			Date epgStartTime = DateUtils._24HrsTimeFormat.parse(epgList.get(i).getStart_time());
			if(currentTime.before(epgStartTime)){
				return epgList.get(i);
			}
		}
		return null;
	}
	public static EPG getPrevEpg(Calendar calendar,ArrayList<EPG> epgList) throws ParseException {
		Date currentTime = DateUtils._24HrsTimeFormat.parse(calendar.get(Calendar.HOUR)+":"+calendar.get(Calendar.MINUTE));
		for(int i = 0; i < epgList.size();i++){
			Date epgStartTime = DateUtils._24HrsTimeFormat.parse(epgList.get(i).getEnd_time());
			if(currentTime.before(epgStartTime)){
				return epgList.get(i);
			}
		}
		return null;
	}

	public ArrayList<EPG> getEPGList(){
		return this.EPGList;
	}
}
