package com.zeronebits.iptv.util.EPGUtils;


import com.zeronebits.iptv.entity.Channel;
import com.zeronebits.iptv.entity.EPG;
import com.zeronebits.iptv.util.common.DateUtils;
import com.zeronebits.iptv.util.common.logger.Logger;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;


/**
 * Created by sadip_000 on 28/10/2016.
 */

public class EpgUtils {

    public static ArrayList<EPGMini> get12hrEpgForDvr(Channel channel) throws ParseException {
        String today = DateUtils.fullDayFormat.format(Calendar.getInstance().getTime());

        Calendar startCal = Calendar.getInstance();
        startCal.add(Calendar.HOUR, -12);
        String startDayForEpg = DateUtils.fullDayFormat.format(startCal.getTime());

        ArrayList<EPGMini> epgMiniforDVR = new ArrayList<>();
        //if day of start cal and today are different first add epgs of prev day
        //add epg of today any how
        if(!today.equalsIgnoreCase(startDayForEpg)){
            epgMiniforDVR.addAll(getMiniEpgForDVR(channel,startCal));
//            Logger.d(epgMiniforDVR.get(0).toString(),epgMiniforDVR.get(epgMiniforDVR.size()-1).toString());
        }
        int size = epgMiniforDVR.size()-1;
        if(size<0) size = 0;
        epgMiniforDVR.addAll(getMiniEpgForDVR(channel,Calendar.getInstance()));

//        Logger.d(epgMiniforDVR.get(size).toString(),epgMiniforDVR.get(epgMiniforDVR.size()-1).toString());
        return epgMiniforDVR;
    }
    private static ArrayList<EPGMini>getMiniEpgForDVR(Channel channel , Calendar calendar) throws ParseException {
        String day = DateUtils.fullDayFormat.format(calendar.getTime());
        ArrayList<EPG> epgListOfPrevDay = channel.getEpgHash().get(day);

        String dateOfStartCal = DateUtils.dateFormat.format(calendar.getTime());

        ArrayList<EPGMini> epgMiniForDvr = new ArrayList<>();
        if(day.equalsIgnoreCase(DateUtils.fullDayFormat.format(Calendar.getInstance().getTime()))) {
            //if current cal is sent
            calendar.add(Calendar.HOUR, -12);
        }

            for(EPG epg: epgListOfPrevDay){
                Calendar epgCalWrtEndTime = DateUtils.createCalendar(dateOfStartCal,epg.getEnd_time());
                //after passedcalendar to currentcalendar add all
                Logger.d("prev cal",DateUtils.dateAndTime.format(calendar.getTime()));
                Logger.e("epg cal",DateUtils.dateAndTime.format(epgCalWrtEndTime.getTime()));
                Logger.i("Curr cal",DateUtils.dateAndTime.format(Calendar.getInstance().getTime()));

                if(calendar.before(epgCalWrtEndTime)&& epgCalWrtEndTime.before(Calendar.getInstance())){
                    Logger.d("getMiniEpgForDVR"+DateUtils.dateAndTime.format(epgCalWrtEndTime.getTime()),DateUtils.dateAndTime.format(calendar.getTime()));
                    EPGMini epgMini= new EPGMini(dateOfStartCal,epg);
                    epgMiniForDvr.add(epgMini);
                }else{
                    continue;
                }
            }





        return epgMiniForDvr;
    }

    public static ArrayList<EPGMini> get12hrEpg(Channel channel) throws ParseException {
        String today = DateUtils.fullDayFormat.format(Calendar.getInstance().getTime());

        Calendar endCal = Calendar.getInstance();
        endCal.add(Calendar.HOUR, 12);
        String startDayForEpg = DateUtils.fullDayFormat.format(endCal.getTime());

        ArrayList<EPGMini> epgMiniforEpg = new ArrayList<>();
        //add epg of today any how
        //if day of start cal and today are different add epgs of next day also
        epgMiniforEpg.addAll(getMiniEpgForEPG(channel,Calendar.getInstance()));
        if(!today.equalsIgnoreCase(startDayForEpg)){
            epgMiniforEpg.addAll(getMiniEpgForEPG(channel,endCal));
        }

        return epgMiniforEpg;
    }
    private static ArrayList<EPGMini>getMiniEpgForEPG(Channel channel , Calendar calendar) throws ParseException {
        String day = DateUtils.fullDayFormat.format(calendar.getTime());
        ArrayList<EPG> epgList= channel.getEpgHash().get(day);
        String date= DateUtils.dateFormat.format(calendar.getTime());
        ArrayList<EPGMini> epgMiniForDvr = new ArrayList<>();
        if(day.equalsIgnoreCase(DateUtils.fullDayFormat.format(Calendar.getInstance().getTime()))) {
            //if current cal is sent
            calendar.add(Calendar.HOUR, 12);
        }

        for(EPG epg: epgList){
            //this calendar must be only 12 hrs after current time i.e calendar must be before calendarWrtStartTime
            Calendar epgCalWrtEndTime = DateUtils.createCalendar(date,epg.getEnd_time());

            if(calendar.after(epgCalWrtEndTime)&& epgCalWrtEndTime.after(Calendar.getInstance())

                    //onAir

                    ){
                EPGMini epgMini= new EPGMini(date,epg);
                epgMiniForDvr.add(epgMini);
            }else{
                continue;
            }
        }
        return epgMiniForDvr;
    }
    public static ArrayList<EPGMini> getMiniEpgOfCalendarForDvr(Channel channel,Calendar dateOfCalendar){
        Calendar startCal = channel.getDvrStartCalendar();
        String day = DateUtils.fullDayFormat.format(dateOfCalendar.getTime());
        String dtPassedDate = DateUtils.dateFormat.format(dateOfCalendar.getTime());

        ArrayList<EPG> epgArrayList= channel.getEpgHash().get(day);
        ArrayList<EPGMini> epgMiniArrayList = new ArrayList<>();

        for(EPG epg: epgArrayList){
            EPGMini epgMini = new EPGMini(dtPassedDate,epg);
            if(epgMini.getStartCalendar().before(Calendar.getInstance())
                    &&epgMini.getStartCalendar().after(startCal))
                epgMiniArrayList.add(epgMini);
        }
        return epgMiniArrayList;
    }






}
