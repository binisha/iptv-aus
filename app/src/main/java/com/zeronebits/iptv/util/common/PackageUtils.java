package com.zeronebits.iptv.util.common;

import android.content.Context;
import android.content.Intent;

public class PackageUtils {
	public static boolean isPackageInstalled(Context context,String packageName) {
		Intent intent=context.getPackageManager().getLaunchIntentForPackage(packageName);
        return intent != null;
	}
}
