package com.zeronebits.iptv.util.common;

import android.content.Context;
import android.os.AsyncTask;

import com.zeronebits.iptv.R;
import com.zeronebits.iptv.asyn.GroupDataParser;
import com.zeronebits.iptv.util.AllParser;
import com.zeronebits.iptv.util.common.logger.Logger;

import org.json.JSONException;


public class CategoryAndChannelsLoader extends AsyncTask<Void, Void, String> {

    private static final String TAG = CategoryAndChannelsLoader.class.getSimpleName();
    private Context context;
    private ErrorSuccess errorSuccess;

    public CategoryAndChannelsLoader(Context context, ErrorSuccess errorSuccess) {
        this.context = context;
        this.errorSuccess = errorSuccess;
    }

    @Override
    protected String doInBackground(Void... params) {
        DownloadUtil getUtc = new DownloadUtil(LinkConfig.GET_UTC,
                                               context);
        String utc = getUtc.downloadStringContent();
        if (!utc.equals(DownloadUtil.NotOnline) && !utc.equals(DownloadUtil.ServerUnrechable)) {
            String categoryUrl = LinkConfig.getString(context, LinkConfig.CATEGORY_URL);
            categoryUrl += "?groupId=" + GroupDataParser.groupData.getGroupId() + "&" + LinkConfig.getHashCode(utc);


            DownloadUtil dUtil = new DownloadUtil(categoryUrl, context);
            Logger.d(TAG, categoryUrl + LinkConfig.getHashCode(utc));
            return dUtil.downloadStringContent();
        } else
            return utc;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (result.equalsIgnoreCase(DownloadUtil.NotOnline) || result.equalsIgnoreCase(DownloadUtil.ServerUnrechable)) {
            errorSuccess.onError(R.string.err_code_server_unreachable);
        }else{
            AllParser parser = new AllParser(context, result);
            try {
                parser.parse();
                if(AllParser.getAllChannels()==null|| AllParser.getAllChannels().size()<=0){
                    errorSuccess.onError(R.string.err_no_channels);
                }else{
                    errorSuccess.onSuccess();
                }
            } catch (JSONException e) {
                errorSuccess.onError(R.string.err_code_json_exception);
                e.printStackTrace();
            }
        }


    }

}
