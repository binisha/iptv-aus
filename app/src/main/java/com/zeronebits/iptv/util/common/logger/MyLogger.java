package com.zeronebits.iptv.util.common.logger;

import android.os.Environment;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class MyLogger {
	private static MyLogger instance=null;
	private String filePath;
	private String dirPath;
	private Calendar cal;
	
	private MyLogger() {}
	
	public static MyLogger getInstance() {
		if(instance==null)
			instance=new MyLogger();
		return instance;
	}
	
	public static void dispose() {
		instance=null;
	}
	
	private synchronized String getFilePath() {
		if(filePath==null) {
			cal=getCal();
			File d=Environment.getExternalStorageDirectory();
			File f=new File(d,"wodLogs");
			return f.getAbsolutePath();
			
		}
		return filePath;
	}
	
	private synchronized Calendar getCal() {
		if(cal==null)
			cal=new GregorianCalendar();
		return cal;
	}
	
	private synchronized File getFile() throws IOException {
		String f=getFilePath();
		//File d=new File(dirPath);
		//if(!d.exists())
		//	d.mkdirs();
		File fi=new File(f);
		if(!fi.exists()) {
			fi.createNewFile();
		}
		return fi;
	}
	
	public synchronized boolean log(String msg) {
		try {
			Calendar c=getCal();
			File f=getFile();			
			FileWriter fw=new FileWriter(f);
			fw.append(msg+"\t"+CalendarUtils.getCalString(c.getTimeInMillis())+"\n");
			fw.flush();
			fw.close();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}
}
