package com.zeronebits.iptv.util.common;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.util.HashMap;

public class SessionManager {

	SharedPreferences pref, pref1;
	Editor editor;
	Editor editor1;
	Context context;

	int MODE = 0;
	private static final String PREF_NAME = "name";

	private static final String IS_LOGIN = "IsLoggedIn";
	public static final String KEY_NAME = "name";
	public static final String KEY_EMAIL = "email";
	public static final String KEY_USERID = "user_id";

	public SessionManager(Context context) {

		this.context = context;
		pref = context.getSharedPreferences(PREF_NAME, MODE);
		editor = pref.edit();

	}

	public void createLoginSession(String name, String email, String userId) {
		editor.putBoolean(IS_LOGIN, true);
		editor.putString(KEY_NAME, name);
		editor.putString(KEY_EMAIL, email);
		editor.putString(KEY_USERID, userId);
		editor.commit();
	}

	public Boolean checkLogin() {
        return !this.isLoggedIn();
    }

	public HashMap<String, String> getUserDetails() {

		HashMap<String, String> user = new HashMap<String, String>();
		user.put(KEY_NAME, pref.getString(KEY_NAME, null));
		user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));
		user.put(KEY_USERID, pref.getString(KEY_USERID, null));
		return user;
	}

	/*
	 * public void logOutUser() { editor.clear(); editor.commit(); Intent i= new
	 * Intent(context, LoginActivity.class);
	 * i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	 * i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); context.startActivity(i); }
	 */

	public boolean isLoggedIn() {
		return pref.getBoolean(IS_LOGIN, false);
	}

}
