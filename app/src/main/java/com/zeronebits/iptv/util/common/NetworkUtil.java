package com.zeronebits.iptv.util.common;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;


/*
 * 
 * This class helps to check the network connectivity
 */
public class NetworkUtil {
     
    public static int TYPE_WIFI = 1;
    //public static int TYPE_EN = 2;
    public static int TYPE_ETHERNET = 2;
    public static int TYPE_NOT_CONNECTED = 0;
   public static int TYPE_MOBILE = 3;
    public static final int NETWORK_STATUS_NOT_CONNECTED=0,NETWORK_STAUS_WIFI=1,NETWORK_STATUS_ETHERNET=2;

    public static int getConnectivityStatus(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
 
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
        	if(activeNetwork.getType()==ConnectivityManager.TYPE_ETHERNET)
        		return TYPE_ETHERNET;
            if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;
            
            if( AppConfig.isDevelopment() ) {
            	if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
            		return TYPE_MOBILE;
            }
        } 
        return TYPE_NOT_CONNECTED;
    }
     
    public static String getConnectivityStatusString(Context context) {
        int conn = NetworkUtil.getConnectivityStatus(context);
        String status = null;
        if (conn == NetworkUtil.TYPE_WIFI) {
            status = "internet";
        } else if (conn == NetworkUtil.TYPE_ETHERNET) {
            status = "internet";
        } else if (conn == NetworkUtil.TYPE_MOBILE) {
            status = "internet";
        } else if (conn == NetworkUtil.TYPE_NOT_CONNECTED) {
            status = "no_internet";
        }
        return status;
    }


    public static int getConnectivityStatusInt(Context context) {
        int conn = NetworkUtil.getConnectivityStatus(context);
        int status = 0;
        if (conn == NetworkUtil.TYPE_WIFI) {
            status = NETWORK_STAUS_WIFI;
        } else if (conn == NetworkUtil.TYPE_MOBILE) {
            status = NETWORK_STATUS_ETHERNET;
        } else if (conn == NetworkUtil.TYPE_NOT_CONNECTED) {
            status = NETWORK_STATUS_NOT_CONNECTED;
        }
        return status;
    }
}