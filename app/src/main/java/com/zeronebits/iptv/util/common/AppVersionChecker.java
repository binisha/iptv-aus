package com.zeronebits.iptv.util.common;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

public class AppVersionChecker {
	
	private String packageName;
	private Activity activity;
	
	private int versionCode;
	private String versionName;
	private boolean isPackageAvailable;
	
	public AppVersionChecker( Activity activity ) {
		packageName = "com.google.android.youtube";
		this.activity = activity;
		
		initPackageMetadata();
	}
	
	public AppVersionChecker( String packageName, Activity activity ) {
		this.packageName = packageName;
		this.activity = activity;
		
		initPackageMetadata();
	}
	
	
	public int getPackageVersionNumber() {
		return this.versionCode;
	}
	
	public String getPackageVersionCode() {		
		return this.versionName;
	}
	
	public boolean isPackageAvaliable() {
		return this.isPackageAvailable;
	}
	
	public boolean isYoutubeUpdateAvailable( int reqVersionCode ) {
        return reqVersionCode > this.versionCode;
	}
	
	private void initPackageMetadata() {
		PackageManager pkmanager = activity.getPackageManager();
		
		try {
			PackageInfo pInfo = pkmanager.getPackageInfo(packageName, PackageManager.GET_META_DATA );

			this.versionCode = pInfo.versionCode;
			this.versionName = pInfo.versionName;
			this.isPackageAvailable = true;
		}catch( Exception e ) {
			this.versionCode = -1;
			this.versionName = "NA";
			this.isPackageAvailable = false;
		}
	}
}
