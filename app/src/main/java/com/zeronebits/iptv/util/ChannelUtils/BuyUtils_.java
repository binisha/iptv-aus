package com.zeronebits.iptv.util.ChannelUtils;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.zeronebits.iptv.R;
import com.zeronebits.iptv.asyn.GroupDataParser;
import com.zeronebits.iptv.entity.BUYSpinnerItem;
import com.zeronebits.iptv.entity.Channel;
import com.zeronebits.iptv.util.MarketAppDetailParser;
import com.zeronebits.iptv.util.common.AppConfig;
import com.zeronebits.iptv.util.common.CustomDialogManager;
import com.zeronebits.iptv.util.common.DownloadUtil;
import com.zeronebits.iptv.util.common.LinkConfig;
import com.zeronebits.iptv.util.common.LoginFileUtils;
import com.zeronebits.iptv.util.common.logger.Logger;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by sadip_000 on 15/04/2016.
 */
public class BuyUtils_ {
    //    TextView txtName, txtHeading;
    ImageView imgObject;
    TextView txtTotalBalance, /*txtRateMovies,*/
            txtRate;
    TextView txtTotalAmount;
    Button btnConfirm, btnCancel, btnTopUp;
    private Context context;
    private Channel channel;
    private Dialog buyDialog;
    private Spinner spinnerMonthSelect;

    private Double totalPrice = 0.0;
    private String durationNumber = "1";


    public BuyUtils_(Context context, Channel channel) {
        this.channel = channel;
        this.context = context;
    }

    public static CustomDialogManager showBuyWarning(final Context context, final Channel channel) {
        final CustomDialogManager buyWarning = new CustomDialogManager(context, CustomDialogManager.WARNING);
        buyWarning.build();
        buyWarning.setTitle(channel.getName());
        buyWarning.setMessage(context.getString(R.string.war_buy_before_watch));
        buyWarning.addDissmissButtonToDialog();
        buyWarning.dismissDialogOnBackPressed();
        buyWarning.setNeutralButton(context.getString(R.string.btn_buy), new OnClickListener() {
            @Override
            public void onClick(View v) {
                buyWarning.dismiss();
                new BuyUtils_(context, channel).showBuyDetailsDialog();
            }
        });
        buyWarning.show();
        return buyWarning;


    }

    public void showBuyDetailsDialog() {
        buyDialog = new Dialog(context);
        buildDialog();
        buyDialog.show();
    }

    private void buildDialog() {

        buyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        buyDialog.setContentView(R.layout.buy_details_dialog);

        /******** Everything for BUY dialog defined below **********/
//        txtHeading = (TextView) buyDialog.findViewById(R.id.dialog_heading);
//        txtName = (TextView) buyDialog.findViewById(R.id.txt_name);

        txtTotalBalance = (TextView) buyDialog
                .findViewById(R.id.txt_total_balance);
        txtRate = (TextView) buyDialog
                .findViewById(R.id.txt_rate);


        spinnerMonthSelect = (Spinner) buyDialog.findViewById(R.id.spinner_month_select);

        txtTotalAmount = (TextView) buyDialog
                .findViewById(R.id.txt_total_amount);

        btnConfirm = (Button) buyDialog
                .findViewById(R.id.btn_ok);
        btnCancel = (Button) buyDialog.findViewById(R.id.btn_cancel);
        btnTopUp = (Button) buyDialog
                .findViewById(R.id.btn_topup_balance);

        imgObject = (ImageView) buyDialog
                .findViewById(R.id.img_object_buy);

        setvalues();

    }

    private void setvalues() {

        if (channel.getChannelPurchaseStatus() == Channel.PURCHASE_BOUGHT) {
            btnConfirm.setText(context.getString(R.string.btn_upgrade));
//            txtHeading.setText(context.getString(R.string.btn_upgrade));

        } else {
            btnConfirm.setText(context.getString(R.string.btn_buy));
        }
//        txtName.setText(channel.getName());
        Logger.d("the user balance", "" + GroupDataParser.groupData.getUserBalance());
        txtTotalBalance.setText("NRs:\t" + GroupDataParser.groupData.getUserBalance());
        txtRate.setText("NRs:\t" + channel.getchannelPrice());
        String Months[] = {"1 month", "2 months", "3 months", "4 months",
                "5 months", "6 months", "7 months", "8 months ", "9 months",
                "10 months", "11 months", "12 months"};
        spinnerMonthSelect.setVisibility(View.VISIBLE);
//            txtRateMovies.setVisibility(View.GONE);

        final ArrayList<BUYSpinnerItem> durationList = new ArrayList<BUYSpinnerItem>();
        for (int i = 0; i < Months.length; i++) {

            BUYSpinnerItem item = new BUYSpinnerItem();
            item.setDuration(Months[i]);
            durationList.add(item);

        }

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
                                                                R.layout.spinner_item_default, Months);
        adapter.setDropDownViewResource(R.layout.spinner_row_new);
        spinnerMonthSelect.setAdapter(adapter);


        spinnerMonthSelect.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                durationNumber = durationList.get(position).getDuration()
                        .replaceAll("[^0-9?!\\.]", "");

                try {
                    totalPrice = channel.getchannelPrice()
                            * Double.parseDouble(durationNumber);
                    txtTotalAmount.setText("NRs:\t" + totalPrice);

                } catch (NumberFormatException e) {
                    totalPrice = 0.0;
                    // your default value
                    txtTotalAmount.setText(context
                                                   .getString(R.string.txt_not_available));
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // nothing
            }
        });


        btnConfirm.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                buyDialog.dismiss();
                try {

                    if (GroupDataParser.groupData.getUserBalance() >= totalPrice) {
                        new BuyCheck(context, channel, Integer.parseInt(durationNumber))
                                .execute();
//                        showBuyPasswordDialog(channel, Integer.parseInt(durationNumber));
                    } else {
                        final CustomDialogManager lowBalance = new CustomDialogManager(
                                context, context
                                .getString(R.string.title_low_balance),
                                context.getString(R.string.err_low_balance),
                                CustomDialogManager.ALERT);
                        lowBalance.build();
                        lowBalance.show();
                        lowBalance.addDissmissButtonToDialog();
                        lowBalance.dismissDialogOnBackPressed();
                        lowBalance.setNeutralButton(context.getString(R.string.btn_topup), new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                MarketAppDetailParser.openApk(context, MarketAppDetailParser.MyAccountAppPackage);
                            }
                        });
                    }
                } catch (NumberFormatException e) {
                    final CustomDialogManager unexpectedError = new CustomDialogManager(
                            context,
                            context.getString(R.string.err_unexpected),
                            CustomDialogManager.ALERT);
                    unexpectedError.build();
                    unexpectedError.show();
                    unexpectedError.setNeutralButton("Dismiss",
                                                     new OnClickListener() {

                                                         @Override
                                                         public void onClick(View v) {
                                                             unexpectedError.dismiss();

                                                         }
                                                     });
                }
            }
        });


        btnCancel.setText(context.getString(R.string.btn_dismiss));
        btnCancel.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                buyDialog.dismiss();
            }
        });


        btnTopUp.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                MarketAppDetailParser.openApk(context, MarketAppDetailParser.MyAccountAppPackage);

            }
        });


        UrlImageViewHelper.setUrlDrawable(imgObject,
                                          channel.getImgLink(), R.drawable.placeholder_logo,
                                          AppConfig.CACHE_HOLD_DURATION);


    }

    private void showBuyPasswordDialog(final Channel channel, final int durationNumber) {
        final Dialog passwordDialog = new Dialog(context);
        passwordDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        passwordDialog.setContentView(R.layout.dialog_enter_password);

        passwordDialog.show();

        passwordDialog.findViewById(R.id.btn_dismiss)
                .setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        passwordDialog.dismiss();

                    }
                });


        Button btnConfirm = (Button) passwordDialog
                .findViewById(R.id.btn_confirm);

        TextView title = (TextView) passwordDialog
                .findViewById(R.id.dialog_heading);
        title.setText(context.getString(R.string.title_password_buy));

        final EditText etParentalPassword = (EditText) passwordDialog
                .findViewById(R.id.et_parental_passowrd);

        final EditText etBuyPassword = (EditText) passwordDialog
                .findViewById(R.id.et_buy_passowrd);

        etBuyPassword.setVisibility(View.VISIBLE);
        etParentalPassword.setVisibility(View.GONE);

        btnConfirm.setText(context.getString(R.string.btn_confirm));

        btnConfirm.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (LoginFileUtils.getUserToken()
                        .equals(etBuyPassword.getText().toString().trim())) {


                    new BuyCheck(context, channel, durationNumber)
                            .execute();
                    passwordDialog.dismiss();

                } else {

                    Toast.makeText(context, "Password MisMatch",
                                   Toast.LENGTH_LONG).show();
                }

            }
        });

    }

    private class BuyCheck extends AsyncTask<String, Void, String> {
        private final String TAG = BuyCheck.class.getSimpleName();
        Channel objectDetails;
        CustomDialogManager progress;
        Context context;
        int durationToBuy;
        private String buyUpgradeUrl;

        public BuyCheck(Context context, Channel channel, int durationToBuy) {
            this.objectDetails = channel;
            this.context = context;
            this.durationToBuy = durationToBuy;


            if (channel.getChannelPurchaseStatus() == Channel.PURCHASE_BOUGHT) {
                buyUpgradeUrl = LinkConfig.getString(context,
                                                     LinkConfig.UPGRADE_URL);
            } else/* if (channel.getPurchaseStatus() == Channel.PURCHASE_BUY) */ {
                buyUpgradeUrl = LinkConfig.getString(context,
                                                     LinkConfig.BUY_URL);

            }
            Logger.d(TAG, buyUpgradeUrl);

            buyUpgradeUrl += "?type=" + "singleChannel"
                    + "&duration=" + durationNumber
                    + "&product_id=" + channel.getId();
            Logger.d(TAG, buyUpgradeUrl);

        }

        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub

            DownloadUtil getUtc = new DownloadUtil(LinkConfig.GET_UTC, context);
            String utc = getUtc.downloadStringContent();
            if (!utc.equals(DownloadUtil.NotOnline)
                    && !utc.equals(DownloadUtil.ServerUnrechable)) {

                buyUpgradeUrl += "&userEmail="
                        + LoginFileUtils.getUserEmail() + "&"
                        + LinkConfig.getHashCode(utc);

                DownloadUtil dUtil = new DownloadUtil(buyUpgradeUrl, context);

                Logger.d(TAG, buyUpgradeUrl);

                return dUtil.downloadStringContent();
            } else
                return utc;

            /************ the following is another process to hit the url *******************/
        /*
         * List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		 * nameValuePairs.add(new BasicNameValuePair("type", "singleMovie"));
		 * nameValuePairs.add(new BasicNameValuePair("duration", "1"));
		 * nameValuePairs.add(new BasicNameValuePair("product_id", "1"));
		 * Logger.d("name value pairs", nameValuePairs + ""); return
		 * dUtil.postDataAndGetStringResponse(nameValuePairs);
		 */

        }

        @Override
        protected void onPreExecute() {
            progress = new CustomDialogManager(context,
                                               CustomDialogManager.LOADING);
            progress.build();
            progress.setMessage("Please wait...");
            progress.show();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (progress.isShowing())
                progress.dismiss();

            Logger.d("BUY RESULT ", result);
            if (result.equalsIgnoreCase(DownloadUtil.NotOnline) || result.equalsIgnoreCase(DownloadUtil.ServerUnrechable)) {
                final CustomDialogManager noInternet = CustomDialogManager.ReUsedCustomDialogs.noInternet(context);
                noInternet.setExtraButton("Dismiss", new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        noInternet.dismiss();

                    }
                });


            } else {

                try {
                    JSONObject jobj = new JSONObject(result);
                    String message = "";
                    try {
                        // String successCode = jobj.getString("success_code");
                        message = jobj.getString("success_message");
                        objectDetails.setChannelPurchaseStatus(Channel.PURCHASE_BOUGHT);
                        GroupDataParser.groupData.setUserBalance(GroupDataParser.groupData.getUserBalance()-(objectDetails.getchannelPrice() * durationToBuy));


                        /*if (context.getClass().getName().equals(LiveTV.class.getName())) {
                            ((TextView) ((LiveTV) context).findViewById(R.id.userBalance)).setText("NRs\t" + objectDetails.getchannelPrice() * durationToBuy + "");
                        }*/
                        Logger.d(TAG + "balance after bought", objectDetails.getchannelPrice() * durationToBuy + "");

                    } catch (JSONException je) {
                        je.printStackTrace();

                        message = jobj.getString("error_message");
                    }

                    final CustomDialogManager bought_dialog = new CustomDialogManager(
                            context, objectDetails.getName(), message,
                            CustomDialogManager.MESSAGE);
                    bought_dialog.build();
                    bought_dialog.show();

                    bought_dialog.setNegativeButton("OK", new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            bought_dialog.dismiss();
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                    CustomDialogManager.ReUsedCustomDialogs.showDataNotFetchedAlert(context);
                }

            }
        }
    }
}
