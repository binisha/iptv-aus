package com.zeronebits.iptv.util.common;

/**
 * Mostly used as follows:
 * Dismiss Button --> extra
 * Next Button -----> negative
 * Prev Button -----> positive
 * Settings--------->neutral
 */

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zeronebits.iptv.EntryPoint;
import com.zeronebits.iptv.R;
import com.zeronebits.iptv.VideoPlayer.TVPlayCustomController_;
import com.zeronebits.iptv.util.MarketAppDetailParser;

import org.json.JSONException;
import org.json.JSONObject;

public class CustomDialogManager {

    public static final int DEFAULT = 0;
    public static final int LOADING = 1;
    public static final int PROGRESS = 2;
    public static final int ALERT = 3;
    public static final int MESSAGE = 4;
    public static final int WARNING = 5;

    private Context context = null;
    private String version = "", macAddress = "", title = "", message = "";

    private Dialog d;
    private TextView alertTitle, MacTextView, versionTextView, messageTextView;
    private RelativeLayout macAndVersion;
    private LinearLayout buttonLayout;
    private Button positive, neutral, negative, extra;
    private ImageView error_image;
    private ProgressBar progressBar;
    private View viewBelowMac, viewAboveButtons;

    private int type = DEFAULT;

    /**
     * mostly loading dialog
     *
     * @param context
     * @param type
     */
    public CustomDialogManager(Context context, int type) {
        this.context = context;
        this.type = type;
        this.title = context.getString(R.string.app_name);
        this.message = context.getString(R.string.err_unexpected);
    }

    /**
     * Dialog with app name as title
     *
     * @param context
     * @param message
     * @param type
     */
    public CustomDialogManager(Context context, String message, int type) {
        this.context = context;
        this.type = type;
        this.message = message;
        this.title = context.getString(R.string.app_name);
    }

    /**
     * @param context
     * @param title
     * @param message
     * @param type
     */
    public CustomDialogManager(Context context, String title, String message,
                               int type) {
        this.context = context;
        this.type = type;
        this.title = title;
        this.message = message;
    }

    public void build() {
        d = new Dialog(context);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.getWindow().setDimAmount(0.8f);
        d.setContentView(R.layout.custom_dialog);
        d.setCancelable(true);
        findViewbyId();

        hidePriorDialogUI();


        setDialogTypeSetting(type);


    }

    private void findViewbyId() {

        alertTitle = (TextView) d.findViewById(R.id.dialog_heading);
        alertTitle.setText(title);
        macAndVersion = (RelativeLayout) d.findViewById(R.id.mac_version);
        MacTextView = (TextView) d.findViewById(R.id.macaddress_variable);
        versionTextView = (TextView) d.findViewById(R.id.app_version_variable);

        viewBelowMac = d.findViewById(R.id.view_below_mac);
        viewAboveButtons = d.findViewById(R.id.view_above_button);

        progressBar = (ProgressBar) d.findViewById(R.id.progressBar);
        error_image = (ImageView) d.findViewById(R.id.error_image);
        messageTextView = (TextView) d.findViewById(R.id.message);

        buttonLayout = (LinearLayout) d.findViewById(R.id.button_layout);
        positive = (Button) d.findViewById(R.id.positive);
        neutral = (Button) d.findViewById(R.id.neutral);
        extra = (Button) d.findViewById(R.id.extra);
        negative = (Button) d.findViewById(R.id.negative);
    }

    private void hidePriorDialogUI() {
        macAndVersion.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        error_image.setVisibility(View.GONE);
        viewAboveButtons.setVisibility(View.GONE);
        viewBelowMac.setVisibility(View.GONE);
        buttonLayout.setVisibility(View.GONE);
        positive.setVisibility(View.GONE);
        neutral.setVisibility(View.GONE);
        extra.setVisibility(View.GONE);
        negative.setVisibility(View.GONE);
    }

    private void setDialogTypeSetting(int type) {
        if (type == LOADING) {
            alertTitle.setVisibility(View.GONE);
            error_image.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            viewBelowMac.setVisibility(View.GONE);
            this.message = context.getString(R.string.txt_loading);
            messageTextView.setText(this.message + "");
        } else if (type == PROGRESS) {
            alertTitle.setVisibility(View.VISIBLE);
            macAndVersion.setVisibility(View.GONE);
            viewAboveButtons.setVisibility(View.GONE);
            error_image.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
            messageTextView.setText(this.message + "");
        } else if (type == ALERT) {
            alertTitle.setVisibility(View.VISIBLE);
            error_image.setImageResource(R.drawable.error);
            error_image.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            messageTextView.setText(this.message + "");
        } else if (type == WARNING) {
            alertTitle.setVisibility(View.VISIBLE);
            error_image.setImageResource(R.drawable.warning);
            error_image.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            messageTextView.setText(this.message + "");
        } else if (type == MESSAGE) {
            alertTitle.setVisibility(View.VISIBLE);
            macAndVersion.setVisibility(View.GONE);
            error_image.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            messageTextView.setText(this.message + "");
        }
    }

    public void dismissDialogOnBackPressed() {
        d.setOnKeyListener(new DialogInterface.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface dialog, int keyCode,
                                 KeyEvent event) {
                switch (keyCode) {
                    case KeyEvent.KEYCODE_BACK:
                        d.dismiss();
                        return true;

                    default:
                        return false;
                }
            }
        });
    }

    public Dialog getInnerObject() {
        return d;
    }

    public void finishActivityOnBackPressed(final Context context) {
        d.setOnKeyListener(new DialogInterface.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface dialog, int keyCode,
                                 KeyEvent event) {
                switch (keyCode) {
                    case KeyEvent.KEYCODE_BACK:
                        d.dismiss();
                        ((Activity) context).finish();
                        return true;
                    default:
                        return false;
                }
            }
        });
    }

    public Button setPositiveButton(String btn_text,
                                    View.OnClickListener onClickListener) {
        viewAboveButtons.setVisibility(View.VISIBLE);
        buttonLayout.setVisibility(View.VISIBLE);
        positive.setText(btn_text);
        positive.setVisibility(View.VISIBLE);
        positive.setOnClickListener(onClickListener);
        return positive;

    }

    public Button setNeutralButton(String btn_text,
                                   View.OnClickListener onClickListener) {
        viewAboveButtons.setVisibility(View.VISIBLE);
        buttonLayout.setVisibility(View.VISIBLE);
        neutral.setText(btn_text);
        neutral.setVisibility(View.VISIBLE);
        neutral.setOnClickListener(onClickListener);
        return neutral;

    }

    public Button setNegativeButton(String btn_text,
                                    View.OnClickListener onClickListener) {
        viewAboveButtons.setVisibility(View.VISIBLE);
        buttonLayout.setVisibility(View.VISIBLE);
        negative.setText(btn_text);
        negative.setVisibility(View.VISIBLE);
        negative.setOnClickListener(onClickListener);
        return negative;

    }

    public String getTitle() {
        return alertTitle.getText() + "";
    }

    public void setTitle(String title) {
        this.title = title;
        /**
         * if set title is done before build it should be found again
         */
        alertTitle = (TextView) d.findViewById(R.id.dialog_heading);
        alertTitle.setText(title);
        alertTitle.setVisibility(View.VISIBLE);
    }

    public void setMessage(String message) {
        this.message = message;
        /**
         * if set message is done before build it should be found again
         */
        messageTextView = (TextView) d.findViewById(R.id.message);
        messageTextView.setText(message);
    }

    public void showMacAndVersion() {
        if (AppConfig.isDevelopment()) {
            macAddress = AppConfig.getMacAddress();
        } else {
            macAddress = GetMac.getMac(context);
        }
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), 0);
            version = pInfo.versionName;
        } catch (NameNotFoundException e) {
            version = "N/A";
            e.printStackTrace();
        }
        MacTextView.setText(macAddress);
        versionTextView.setText(version);

        macAndVersion.setVisibility(View.VISIBLE);
        viewBelowMac.setVisibility(View.VISIBLE);
    }

    public void show() {
        d.show();
    }

    public void hide() {
        d.hide();
    }

    public void dismiss() {
        d.dismiss();
    }

    public boolean isShowing() {
        /**
         * if build is not done then handle exception for dialog
         */
        try {
            return d.isShowing();
        } catch (Exception e) {
            return false;
        }
    }

    public Button getNeutralButton() {
        return neutral;
    }

    public Button getExtraButton() {
        return extra;
    }

    public void finishActivityonDismissPressed(final Context context) {

        setExtraButton(
                context.getString(R.string.btn_dismiss),
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        d.dismiss();
                        ((Activity) context).finish();

                    }
                });
    }

    // end of re used custom Dialog

    public Button setExtraButton(String btn_text,
                                 View.OnClickListener onClickListener) {
        viewAboveButtons.setVisibility(View.VISIBLE);
        buttonLayout.setVisibility(View.VISIBLE);
        extra.setText(btn_text);
        extra.setVisibility(View.VISIBLE);
        extra.setOnClickListener(onClickListener);
        return extra;

    }


    public void addDissmissButtonToDialog() {
        setExtraButton(
                context.getString(R.string.btn_dismiss),
                new View.OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        d.dismiss();
                        if (context.getClass().getName()
                                .equals(
                                        EntryPoint.class.getName())
                                )

                            ((Activity) context).finish();
                    }
                });
    }

    public static class ReUsedCustomDialogs {
        // Dialog to show when required data not available
        public static CustomDialogManager showDataNotFetchedAlert(final Context context) {
            final CustomDialogManager error = new CustomDialogManager(context,
                                                                      CustomDialogManager.ALERT);
            error.build();
            error.showMacAndVersion();
            error.setMessage(context.getString(R.string.err_json_exception));
            error.addDissmissButtonToDialog();
            if (context
                    .getClass()
                    .getName()
                    .equals(EntryPoint.class.getName())
                    ) {
                error.setNegativeButton(context.getString(R.string.btn_relaunch), new View.OnClickListener() {

                    @Override
                    public void onClick(View arg0) {
                        error.dismiss();
                        Intent i = ((ContextWrapper) context)
                                .getBaseContext()
                                .getPackageManager()
                                .getLaunchIntentForPackage(
                                        ((ContextWrapper) context).getBaseContext()
                                                .getPackageName());
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        context.startActivity(i);

                        ((Activity) context).finish();

                    }
                });
            }
            error.show();
            return error;
        }




        public static CustomDialogManager noInternet(final Context context) {
            final CustomDialogManager noInternet = new CustomDialogManager(context, CustomDialogManager.WARNING);
            noInternet.build();
            noInternet.getInnerObject().setCancelable(true);
            noInternet.setTitle(context.getString(R.string.app_name));
            noInternet.setMessage(context.getString(R.string.err_server_unreachable));
            noInternet.show();

            noInternet.addDissmissButtonToDialog();

            noInternet.setNeutralButton(context.getString(R.string.btn_settings), new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    noInternet.dismiss();
                    MarketAppDetailParser.openApk(context, MarketAppDetailParser.MyNITVSettings);

                    /*Intent intent = new Intent(
                            android.provider.Settings.ACTION_SETTINGS);
                    context.startActivity(intent);*/
                }
            });

//Relaunch Settings Dismiss .....
            if (context.getClass().getName().equals(EntryPoint.class.getName())) {

                noInternet.finishActivityonDismissPressed(context);
                noInternet.finishActivityOnBackPressed(context);

                noInternet.setPositiveButton(context.getString(R.string.btn_relaunch), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        noInternet.dismiss();
                        context.startActivity(i);
                        ((Activity) context).finish();

                    }
                });

                noInternet.getInnerObject().setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {

                        ((Activity) context).finish();
                    }

                });
            } else if (context.getClass().getName().equals(TVPlayCustomController_.class.getName())) {
                noInternet.finishActivityonDismissPressed(context);
                noInternet.finishActivityOnBackPressed(context);

                noInternet.getInnerObject().setOnCancelListener(new OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {

                        ((Activity) context).finish();
                    }

                });
            }
            return noInternet;
        }

        public static CustomDialogManager featureNotAvailable(Context context) {
            CustomDialogManager featureNotAvailable = new CustomDialogManager(context, CustomDialogManager.MESSAGE);
            featureNotAvailable.build();
            featureNotAvailable.showMacAndVersion();
            featureNotAvailable.setMessage(context.getString(R.string.msg_feature_not_available));
            featureNotAvailable.addDissmissButtonToDialog();
            featureNotAvailable.dismissDialogOnBackPressed();
            featureNotAvailable.getInnerObject().setCancelable(true);
            featureNotAvailable.show();
            return featureNotAvailable;
        }
        public static CustomDialogManager parseAndShowErrorMessage(String result, final Context context) throws JSONException {
            JSONObject jo = new JSONObject(result);
            StringBuilder sb = new StringBuilder();
            sb.append("Error Code:\t\t")
                    .append(jo.getString("error_code"))
                    .append("\n").append(jo.getString("error_message"));
            final CustomDialogManager parseError = new CustomDialogManager(context, CustomDialogManager.MESSAGE);
            parseError.build();
            parseError.setMessage(sb.toString());
            parseError.addDissmissButtonToDialog();
            parseError.dismissDialogOnBackPressed();
            parseError.getInnerObject().setCancelable(true);

            parseError.show();
            parseError.getInnerObject().setOnCancelListener(new OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    Intent i = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    parseError.dismiss();
                    context.startActivity(i);
                    ((Activity) context).finish();
                }
            });
            return parseError;


        }

    }


    public static void dataNotFetched(final Context context){
        final CustomDialogManager error = new CustomDialogManager(context, CustomDialogManager.ALERT);
        error.build();
        error.showMacAndVersion();
        error.setMessage(context.getString(R.string.err_json_exception));
        error.setNegativeButton("Dismiss", new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                error.dismiss();
                ((Activity)context).finish();

            }
        });
        error.show();
    }

}
