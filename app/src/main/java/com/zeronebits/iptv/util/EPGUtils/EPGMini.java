package com.zeronebits.iptv.util.EPGUtils;


import com.zeronebits.iptv.entity.EPG;
import com.zeronebits.iptv.util.common.DateUtils;

import java.text.ParseException;
import java.util.Calendar;

/**
 * Created by sadip_000 on 28/10/2016.
 */

public class EPGMini {

    public Calendar getStartCalendar() {
        return startCalendar;
    }

    public void setStartCalendar(Calendar startCalendar) {
        this.startCalendar = startCalendar;
    }

    public Calendar getEndCalendar() {
        return endCalendar;
    }

    public void setEndCalendar(Calendar endCalendar) {
        this.endCalendar = endCalendar;
    }

    private Calendar startCalendar;
    private Calendar endCalendar;
    private String prgmName;

    public EPGMini(String date, EPG epg) {
        setPrgmName(epg.getProgram_name());

        try {
            setEndCalendar(DateUtils.createCalendar(date,epg.getEnd_time()));
        } catch (ParseException e) {
            e.printStackTrace();
            setEndCalendar(null);
        }

        try {
            setStartCalendar(DateUtils.createCalendar(date,epg.getStart_time()));
        } catch (ParseException e) {
            e.printStackTrace();
            setStartCalendar(null);
        }
    }


    public String getPrgmName() {
        return prgmName;
    }

    public void setPrgmName(String prgmName) {
        this.prgmName = prgmName;
    }
    @Override
    public String toString(){
        return new StringBuilder().append(DateUtils.dateAndTime.format(getStartCalendar().getTime()))
                .append("\t").append(getPrgmName())
                .append("\t").append(DateUtils.dateAndTime.format(getEndCalendar().getTime())).toString();
    }
}
