package com.zeronebits.iptv.util.ChannelUtils;

import android.content.Context;

import com.zeronebits.iptv.util.common.logger.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;


public class EpgInFile {
    public static final String EPG_DIRECTORY_NAME = "Epg";

    public static boolean writeToFile(Context context, String channelId, String data) {
        String dirPath = context.getCacheDir().getPath() + File.separator + EpgInFile.EPG_DIRECTORY_NAME;
        File dir = new File(dirPath);

        // If file does not exists, then create it
        if (!dir.mkdirs()){
            System.err.println("Could not create parent directories ");
        }
        File file = new File(dir, channelId);
        if(file.exists()){
            file.delete();
        }
        try {
            file.createNewFile();
            FileOutputStream outputStream = new FileOutputStream(file);
            outputStream.write(data.getBytes());
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static boolean epgExistInFile(Context context, String channelId) {

        StringBuilder sb = new StringBuilder();

        sb.append( context.getCacheDir().getAbsolutePath() ).append( File.separator ).append( EpgInFile.EPG_DIRECTORY_NAME );
        //dirPath = sb.toString();
        Logger.i( "File Read Here", sb.toString() );
        File dir = new File(sb.toString());
        File[] listFiles = dir.listFiles();
        if(listFiles==null) return false;

        for (File file : listFiles) {
            if (file.getName().equalsIgnoreCase(channelId.trim()))
                return true;
        }
        return false;
    }

    public static String readFromFile(Context context, String channelId) throws IOException {
        String dirPath = context.getCacheDir().getPath() + File.separator + EpgInFile.EPG_DIRECTORY_NAME;
        File dir = new File(dirPath);
        File file = new File(dir, channelId);
        Logger.d("File Read from", file.getPath());
        StringBuilder sb = new StringBuilder();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
            String inputString;
            while ((inputString = bufferedReader.readLine()) != null) {
                sb.append(inputString);
            }
            bufferedReader.close();
        return sb.toString();
    }

}
