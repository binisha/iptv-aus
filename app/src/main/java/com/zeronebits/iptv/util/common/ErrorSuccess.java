package com.zeronebits.iptv.util.common;

/**
 * Created by sadip_000 on 16/09/2016.
 */
public interface ErrorSuccess {
    void onSuccess();
    void onError(int errorCode);
}
