package com.zeronebits.iptv.util.common;

import android.net.Uri;

import com.zeronebits.iptv.BuildConfig;

public class AppConfig {
	public static final int CACHE_HOLD_DURATION = 60 * 1000; // 1 minutes
	public static final int randomdialogshowrange = 2 * 60 * 60 * 1000;
	public static final String provider = "content://com.zeronebits.iptv";
	public static final Uri CONTENT_PROVIDER = Uri.parse(provider);

	/**
	 * must be set false in production phase.
	 */
	private static final boolean isInDevelopment = BuildConfig.DEBUG;
	public static boolean featureParentalLock = true;

	public static boolean isDevelopment() {
		return isInDevelopment;
	}

	/**
	 * @return static macaddress of box during development phase
	 */
	public static String getMacAddress() {
		return "784476f5cff8";
//		return "0A0121314298";
//		return "acdbda259dcf";
	}

	public static String getPrefsName() {
		return "TOTALCABLE_PREFS";
	}
}
