package com.zeronebits.iptv.util.EPGUtils;

import android.content.Context;
import android.os.AsyncTask;

import com.zeronebits.iptv.R;
import com.zeronebits.iptv.entity.Channel;
import com.zeronebits.iptv.entity.EPG;
import com.zeronebits.iptv.util.ChannelUtils.EpgInFile;
import com.zeronebits.iptv.util.EPGParser;
import com.zeronebits.iptv.util.common.DownloadUtil;
import com.zeronebits.iptv.util.common.LinkConfig;
import com.zeronebits.iptv.util.common.logger.Logger;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by binisha on 17/10/2016.
 */

public abstract class EpgLoadOption extends AsyncTask<Void,Void,String>{
    private Context context;
    private String epgLink;
    private Channel channel;

    public EpgLoadOption(Context context, Channel channel){
        this.context = context;
        epgLink = LinkConfig.getString(context, LinkConfig.EPG_URL);
//        epgLink="http://103.213.31.25/nettv_isp_test/app/json_v5/epg_json.php?channelId=343";
        this.channel = channel;
    }
    public abstract void onSuccess(HashMap<String, ArrayList<EPG>> hashmapDays);
    public abstract void onError(Object errorCode);

    public void getEpgHash() {
        /*if (channel.getEpgHash() != null) {
            onSuccess(channel.getEpgHash());
        } else if (EpgInFile.epgExistInFile(context, channel.getId() + "")) {
            //parse epg from file
            try {
                String result = EpgInFile.readFromFile(context, channel.getId() + "");
                //parsing from file complete so continue to post execute similar to parsing from api
                if (result.equalsIgnoreCase("[]")) {
                    execute();
                }else{
                    onPostExecute(result);
                }

            } catch (IOException e) {
                //parse epg from api
                execute();
            }
        } else {*/
            //parse epg from api
            execute();
//        }
    }


    @Override
    protected String doInBackground(Void... params) {
//        DownloadUtil dUtil = new DownloadUtil(epgLink, context);
        Logger.d("DVRLink", epgLink);
//        return dUtil.downloadStringContent();
        DownloadUtil getUtc = new DownloadUtil(LinkConfig.GET_UTC,                               context);
        String utc = getUtc.downloadStringContent();

        if (!utc.equals(DownloadUtil.NotOnline)
                && !utc.equals(DownloadUtil.ServerUnrechable)) {
            epgLink += "?" + LinkConfig.getHashCode(utc) + "&" + "channelId=" + channel.getId();

            DownloadUtil dUtil = new DownloadUtil(epgLink, context);
            Logger.d("EPGLink", epgLink);
            return dUtil.downloadStringContent();

        } else
            return utc;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        Logger.d("CheckingDVRResult",result);
        if (result.equalsIgnoreCase(DownloadUtil.NotOnline) || result.equalsIgnoreCase(DownloadUtil.ServerUnrechable)) {
            onError(R.string.err_code_server_unreachable);
        } else {
            EPGParser parser = new EPGParser(result);
            try {
                parser.parseEPG();
                channel.setEpgHash(parser.getHashmapDays());
                onSuccess(parser.getHashmapDays());
                EpgInFile.writeToFile(context,channel.getId()+"",result);
            } catch (JSONException e) {
                e.printStackTrace();
                onError(R.string.err_code_epg_not_found);
            }

        }
    }


}

