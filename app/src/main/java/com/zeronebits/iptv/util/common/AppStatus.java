package com.zeronebits.iptv.util.common;


import android.R;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.zeronebits.iptv.util.common.logger.Logger;

public class AppStatus {

    private static AppStatus instance = new AppStatus();
    static Context context;
    ConnectivityManager connectivityManager;
    NetworkInfo wifiInfo, mobileInfo;
    boolean connected = false;

    public static AppStatus getInstance(Context ctx) {
        context = ctx;
        return instance;
    }

    public boolean isOnline(Context con) {
        try {
            connectivityManager = (ConnectivityManager) con
                        .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        connected = networkInfo != null && networkInfo.isAvailable() &&
                networkInfo.isConnected();
        return connected;


        } catch (Exception e) {
            System.out.println("CheckConnectivity Exception: " + e.getMessage());
            Logger.v("connectivity", e.toString());
        }
        return connected;
    }
    
    @SuppressWarnings("deprecation")
	public void showAlertDialog(Context context, String title, String message, Boolean status) {
    	  AlertDialog alertDialog = new AlertDialog.Builder(context).create();
    
    	  alertDialog.setTitle(title);
    	 
    	  alertDialog.setMessage(message);
    	 
    	  
    	  alertDialog.setIcon((status) ? R.drawable.presence_online : R.drawable.presence_offline);
    	 
    	  alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
    	   @Override
		public void onClick(DialogInterface dialog, int which) {
    		   
    	   }
    	  });
    	  
    	  alertDialog.show();
    	 }
    	
}