package com.zeronebits.iptv;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningAppProcessInfo;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.zeronebits.iptv.util.common.AppConfig;
import com.zeronebits.iptv.util.common.DownloadUtil;
import com.zeronebits.iptv.util.common.GetMac;
import com.zeronebits.iptv.util.common.LinkConfig;
import com.zeronebits.iptv.util.common.logger.Logger;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;
import java.util.Random;


/**
 * Created by sadip_000 on 16/03/2016.
 */
public class ScreenSaverActivity extends Activity {
    ImageView imgScreenSaver;
    FrameLayout screenSaverLayout;
    Handler screenSaverHandler;
    Runnable screenSaverRunnable;
    TextView txtTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_saver);
        getApp().setScreenSaverStarted(true);
        screenSaverLayout = (FrameLayout) findViewById(R.id.layout_screen_saver);
        imgScreenSaver = (ImageView) findViewById(R.id.img_screenSaver);
        txtTime = (TextView) findViewById(R.id.text_time);
        txtTime.setVisibility(View.GONE);
        Bundle bundle = getIntent().getExtras();
        imgScreenSaver.setImageResource(R.drawable.screensaver);
        Logger.d("screensaver image list size", TotalCable.screenSaverImageList.size() + "");
        if (TotalCable.screenSaverImageList.size() <= 1)
            new LoadScreenSaverImageLinks().execute();
//        changePositionForTextView();
        repeatScreenSaver(bundle.getInt("delay"));
//        repeatScreenSaver("",bundle.getInt("delay"));
        killBackgroundApps();
    }

    public TotalCable getApp() {
        return (TotalCable) this.getApplication();
    }

    private void repeatScreenSaver(final int milliseconds) {
        final Random random = new Random();
        Thread changeScreenSaverImageThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (getApp().getScreenSaverStarted()) {

                    try {
                        Thread.sleep(milliseconds);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (TotalCable.screenSaverImageList.size() > 1) {

                                /*Ion.with(ScreenSaverActivity.this)
                                        .load("http:\\"+TotalCable.screenSaverImageList.get(
                                                      random.nextInt(TotalCable.screenSaverImageList.size()
                                                      )
                                              )
                                        )
                                        .withBitmap()
                                        .error(R.drawable.screensaver)
                                        .intoImageView(imgScreenSaver);*/
                                UrlImageViewHelper.setUrlDrawable(imgScreenSaver,
                                                                  TotalCable.screenSaverImageList.get(random.nextInt(
                                                                          TotalCable.screenSaverImageList.size()
                                                                                                 )
                                                                  )
                                        , R.drawable.screensaver, AppConfig.CACHE_HOLD_DURATION);
                            } else {
                                imgScreenSaver.setImageResource(R.drawable.screensaver);
                            }

                        }
                    });

                }
            }
        });
        changeScreenSaverImageThread.start();
    }

    private void killBackgroundApps() {
        ActivityManager am = (ActivityManager) this.getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningAppProcessInfo> tasks = am.getRunningAppProcesses();
        //for loop to kill all process except currentApp
        for (RunningAppProcessInfo process : tasks) {
            if (process.pid != tasks.get(0).pid) {
                android.os.Process.killProcess(process.pid);
            }
        }
        for (ApplicationInfo packageInfo : getPackageManager().getInstalledApplications(0)) {
            if (packageInfo.packageName != getApplicationContext().getPackageName()) {
                am.killBackgroundProcesses(packageInfo.packageName);
            }

        }
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        getApp().active();
        getApp().setScreenSaverStarted(false);
        finish();

    }

    private void changePositionForTextView() {
        final FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(txtTime.getLayoutParams());
        final Random random = new Random();
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                while (getApp().getScreenSaverStarted()) {
                    try {
                        Thread.sleep(8000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    params.setMargins(random.nextInt(500), random.nextInt(500),
                                      random.nextInt(200), random.nextInt(200));
                    runOnUiThread(new Thread(new Runnable() {
                        @Override
                        public void run() {
                            txtTime.setLayoutParams(params);
                            Logger.d(".ScreenSaverActivity", "layoutParams new set");
                        }
                    }));


                }

            }
        });

        t.start();

    }

    private class LoadScreenSaverImageLinks extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... params) {
            DownloadUtil downloadUtil = new DownloadUtil(LinkConfig.SCREEN_SAVER_LINK+"?mac="+ GetMac.getMac(ScreenSaverActivity.this), getApplicationContext());
            Logger.d("Screen saver Link", LinkConfig.SCREEN_SAVER_LINK);
            return downloadUtil.downloadStringContent();
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Logger.d("Screen saver json response", s);
            try {
                JSONArray jsonArray = new JSONArray(s);
                int len = jsonArray.length();
                for (int i = 0; i < len; i++) {
                    String link = jsonArray.getJSONObject(i).getString("link");
                    Logger.d("Screen saver link " + i, link);

                    TotalCable.screenSaverImageList.add(link);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}
