package com.zeronebits.iptv.VideoPlayer;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.zeronebits.iptv.R;


public class DoubleLoginActivity extends Activity{
	Button relogin;
	TextView exit_doublelogin;
	BroadcastReceiver receiver;

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doublelogin);

        exit_doublelogin= (TextView)findViewById(R.id.exit_doublelogin_text);
        exit_doublelogin.setText(getString(R.string.app_name) +" "+ getString(R.string.err_doublelogin));
        relogin =((Button)findViewById(R.id.relogin));
        relogin.requestFocus();
        relogin.setOnClickListener(
        		new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
//						Intent i = new Intent(DoubleLoginActivity.this,EntryPoint.class);
//						Intent i = getBaseContext().getPackageManager()
//					             .getLaunchIntentForPackage( getBaseContext().getPackageName() );
//						i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//						startActivity(i);
						System.exit(0);
						
					}
				});
	}

	@Override
	public void onUserInteraction() {
		super.onUserInteraction();

	}
}
