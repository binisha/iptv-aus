package com.zeronebits.iptv.VideoPlayer;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.zeronebits.iptv.R;
import com.zeronebits.iptv.util.ChannelErrorUtils;
import com.zeronebits.iptv.util.MarketAppDetailParser;
import com.zeronebits.iptv.util.common.CategoryAndChannelsLoader;
import com.zeronebits.iptv.util.common.ErrorSuccess;
import com.zeronebits.iptv.util.common.logger.Logger;


/**
 * Created by NITV on 08/09/2016.
 */
public class FragmentError extends Fragment {


    TextView txtErrorMessage, txtErrorCode;
    Button btnPositive, btnNegative;
    String strErrorCode, strErrorMessage;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_error, container, false);
        findViewByIds(view);


        final Bundle bundle = getArguments();
        setErrorOnUI(bundle);

        return view;
    }

    private void setErrorOnUI(Bundle bundle) {
        try {
            int errCode = bundle.getInt(String.valueOf(TVPlayCustomController_.TAG_ERROR_CODE.toString()));
            boolean dvr = bundle.getBoolean("DVR");

            if(errCode == R.string.err_code_epg_not_found){
                if(dvr){
                    strErrorCode = getString(R.string.err_code_dvr_not_found);
                    strErrorMessage = ChannelErrorUtils.getErrorMessageFromCode(getActivity(), R.string.err_code_dvr_not_found);
                }else{
                    strErrorCode = getString(errCode);
                    strErrorMessage = ChannelErrorUtils.getErrorMessageFromCode(getActivity(), errCode);
                }
            }else {
                strErrorCode = getString(errCode);
                strErrorMessage = ChannelErrorUtils.getErrorMessageFromCode(getActivity(), errCode);
                Logger.i("TEST", 1 + "");
            }

        } catch (Exception e) {
            try {
                Logger.i("TEST", 2 + "");
                strErrorCode = bundle.getString(TVPlayCustomController_.TAG_ERROR_CODE);
                if (strErrorCode.contains("MEDIA_ERROR")) {
                    strErrorMessage = ChannelErrorUtils.getMediaErrorMessage(getActivity(), strErrorCode);
                } else {
                    //e.g   e.getMessage();
                    strErrorMessage = strErrorCode;
                    strErrorCode = null;
                }
            } catch (Exception e1) {
                Logger.i("TEST", 3 + "");
                e1.printStackTrace();
                strErrorCode = null;
                strErrorMessage = "N/A";
            }

        } finally {
            if (strErrorCode != null) {
                txtErrorCode.setVisibility(View.VISIBLE);
                txtErrorCode.setText("Error Code: " + strErrorCode);
                txtErrorMessage.setText(strErrorMessage);

                if (strErrorCode.equalsIgnoreCase(getString(R.string.err_code_server_unreachable))) {
                    btnPositive.setVisibility(View.VISIBLE);
                    btnPositive.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //open settings
                            MarketAppDetailParser.openApk(getActivity(), MarketAppDetailParser.MyNITVSettings);
                        }
                    });
                    btnPositive.requestFocus();

                } else if (strErrorCode.equalsIgnoreCase(getString(R.string.err_code_json_exception))) {
                    btnPositive.setVisibility(View.VISIBLE);
                    btnPositive.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            new CategoryAndChannelsLoader(getActivity(), new ErrorSuccess() {
                                @Override
                                public void onSuccess() {
                                    //update fragment main
                                    //try to open last played channel

                                }

                                @Override
                                public void onError(int errorCode) {
                                    //set error
                                    Bundle bundle = new Bundle();
                                    bundle.putInt(TVPlayCustomController_.TAG_ERROR_CODE, errorCode);
                                    setErrorOnUI(bundle);
                                }
                            }).execute();
                        }
                    });
                    btnPositive.requestFocus();
                }
            }
            txtErrorMessage.setText(strErrorMessage);
        }

        TVPlayCustomController_ tvPlayCustomController = (TVPlayCustomController_) getActivity();
        tvPlayCustomController.stopHandleToCloseMenu();

    }

    private void findViewByIds(View view) {
        Typeface medium = Typeface.createFromAsset(getActivity().getAssets(), "font/Exo2-Medium.otf");
        Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), "font/Exo2-Bold.otf");
        Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "font/Exo2-Regular_0.otf");
        Typeface semiBold = Typeface.createFromAsset(getActivity().getAssets(), "font/Exo2-SemiBold.otf");
        Typeface light = Typeface.createFromAsset(getActivity().getAssets(), "font/Exo2-Light.otf");
        btnNegative = (Button) view.findViewById(R.id.btn_negative);
        btnPositive = (Button) view.findViewById(R.id.btn_positive);
        txtErrorMessage = (TextView) view.findViewById(R.id.txt_error_msg);
        txtErrorCode = (TextView) view.findViewById(R.id.txt_error_code);
        TextView error = (TextView) view.findViewById(R.id.error);
        error.setTypeface(bold);
        txtErrorCode.setTypeface(regular);
        txtErrorMessage.setTypeface(regular);
        btnPositive.setFocusable(true);
        btnPositive.requestFocus();
        txtErrorCode.setVisibility(View.GONE);
        btnPositive.setVisibility(View.GONE);
        btnNegative.setVisibility(View.GONE);


    }
}
