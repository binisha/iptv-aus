package com.zeronebits.iptv.VideoPlayer;

import android.content.Intent;
import android.media.MediaPlayer.OnVideoSizeChangedListener;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.widget.Toast;


import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.ErrorReason;
import com.google.android.youtube.player.YouTubePlayer.PlaybackEventListener;
import com.google.android.youtube.player.YouTubePlayer.PlayerStateChangeListener;
import com.google.android.youtube.player.YouTubePlayer.Provider;
import com.google.android.youtube.player.YouTubePlayerView;
import com.zeronebits.iptv.R;


public class youtubeMobilePlayActivity extends YouTubeBaseActivity implements
		YouTubePlayer.OnInitializedListener {

	private static String YT_KEY = "AIzaSyBwKJO2DOKXlT25syR4qfAV9InyzQ6tmaM";
	public static final String API_KEY = "AIzaSyBf-m75nFwQLZogdn00AfdxRp17nzkUh1A";
	// public static String video_id;
	public String playlist_id;
	public int position;

	private YouTubePlayer youTubePlayer;
	private YouTubePlayerView youTubePlayerView;
	 private MyPlayerStateChangeListener myPlayerStateChangeListener;
	 private MyPlaybackEventListener myPlaybackEventListener;
	 String log = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_videoplay);

		playlist_id = getIntent().getStringExtra("playlist_id");
		position = getIntent().getIntExtra("position", 0);
		System.out.println(playlist_id+ position);
youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtubeplayerview);
		youTubePlayerView.initialize(YT_KEY, this);
		myPlayerStateChangeListener = new MyPlayerStateChangeListener();
        myPlaybackEventListener = new MyPlaybackEventListener();

		
	}

	@Override
	public void onInitializationFailure(Provider provider,
			YouTubeInitializationResult result) {
		
		Toast.makeText(getApplicationContext(),
				"YouTubePlayer.onInitializationFailure()", Toast.LENGTH_LONG)
				.show();
		Log.d("error", result + "");
	}

	@Override
	public void onInitializationSuccess(Provider provider,
			YouTubePlayer player, boolean wasRestored) {
		
//		player.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
//		player.setShowFullscreenButton(true);
		  player.setPlayerStateChangeListener(myPlayerStateChangeListener);
		  player.setPlaybackEventListener(myPlaybackEventListener);
		
		if (!wasRestored) {
			player.cueVideo(playlist_id);
			
			player.play();
		}
	}
	
	 private final class MyPlayerStateChangeListener implements PlayerStateChangeListener {

		@Override
		public void onAdStarted() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onError(ErrorReason arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onLoaded(String arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onLoading() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void onVideoEnded() {
			// TODO Auto-generated method stub
		
			finish();
			
		}

		@Override
		public void onVideoStarted() {
			// TODO Auto-generated method stub
			//youTubePlayer.play();
			
		}
		 
	 }
	 
	 private final class MyPlaybackEventListener implements PlaybackEventListener {
		  
		  private void updateLog(String prompt){
		   log +=  "MyPlaybackEventListener" + "\n-" + 
		     prompt + "\n\n=====";
		  // textVideoLog.setText(log);
		  }

		 @Override
		  public void onBuffering(boolean arg0) {
		   updateLog("onBuffering(): " + String.valueOf(arg0));
		  }

		  @Override
		  public void onPaused() {
		   updateLog("onPaused()");
		  }

		  @Override
		  public void onPlaying() {
		   updateLog("onPlaying()");
		  }

		  @Override
		  public void onSeekTo(int arg0) {
		   updateLog("onSeekTo(): " + String.valueOf(arg0));
		  }

		  @Override
		  public void onStopped() {
		   updateLog("onStopped()");
		  }
		  
		 }


	public void onBackPressed()
	{
//		FragmentMain fragment = new FragmentMain();
//		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//		transaction.replace(R.id.container_fragment_main, fragment);
//		transaction.commit();

	}
	@Override
	protected void onStop()
	{
//		unregisterReceiver(this);
		super.onStop();
	}
}
