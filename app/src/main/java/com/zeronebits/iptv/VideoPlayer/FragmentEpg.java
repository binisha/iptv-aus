package com.zeronebits.iptv.VideoPlayer;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.zeronebits.iptv.R;
import com.zeronebits.iptv.adpater.DateAdapter;
import com.zeronebits.iptv.adpater.EPGAdapter_;
import com.zeronebits.iptv.adpater.EPGChannelListAdapter;
import com.zeronebits.iptv.entity.Channel;
import com.zeronebits.iptv.entity.EPG;
import com.zeronebits.iptv.util.AllParser;
import com.zeronebits.iptv.util.ChannelErrorUtils;
import com.zeronebits.iptv.util.EPGUtils.EpgLoadOption;
import com.zeronebits.iptv.util.common.DateUtils;
import com.zeronebits.iptv.util.common.logger.Logger;

import org.lucasr.twowayview.TwoWayView;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import static com.zeronebits.iptv.util.AllParser.getPositionOfChannelInCategory;

/**
 * Created by binisha on 11/7/16.
 */
public class FragmentEpg extends Fragment {

    private TwoWayView gvChannelList;
    private LinearLayout layoutDateEpg;
    private GridView gvDate, gvEpgDvr;
    private TextView txtOnAir, txtDayView, txtDateView, txtPrgmTime, txtPrgmName;
    private TVPlayCustomController_ tvPlayCustomController;
    private TextView txtChannelName;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_epg_dvr, container, false);
        tvPlayCustomController = (TVPlayCustomController_) getActivity();
        findViewByIds(view);


        String currentDay = DateUtils.fullDayFormat.format(Calendar.getInstance().getTime());
        String currentDate = DateUtils.dateFormat.format(Calendar.getInstance().getTime());
        txtDayView.setText(currentDay);
        txtDateView.setText(currentDate);



        int channelId = getArguments().getInt(TVPlayCustomController_.TAG_CURRENT_CHANNEL_ID, 0);
        Channel channel = AllParser.getChannelFromId(channelId);

        final EPGChannelListAdapter channelListAdapter = new EPGChannelListAdapter(tvPlayCustomController, R.layout.epg_channel_list_row, AllParser.getAllChannels());
        gvChannelList.setAdapter(channelListAdapter);
        gvChannelList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                EPGChannelListAdapter channelListAdapter = (EPGChannelListAdapter) gvChannelList.getAdapter();
                channelListAdapter.setPosition(position);
                channelListAdapter.notifyDataSetChanged();
                gvChannelList.setSelection(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        gvChannelList.setOnItemClickListener(new ChannelOnItemClickListener());

        int positionOfChannel = getPositionOfChannelInCategory(channel, AllParser.allcategories.get(0).getCategoryName());
//        int positionOfChannel = 0;
        gvChannelList.setSelection(positionOfChannel);

        channelListAdapter.setPosition(positionOfChannel);
        channelListAdapter.notifyDataSetChanged();

        new ChannelOnItemClickListener().onItemClick(null, null, positionOfChannel, 0);
        if (getArguments().containsKey(TVPlayCustomController_.TAG_ERROR_CODE)) {
            showError(getArguments().get(TVPlayCustomController_.TAG_ERROR_CODE));
        }

        return view;
    }

    private void findViewByIds(View view) {
        gvChannelList = (TwoWayView) view.findViewById(R.id.channel_list);
        gvEpgDvr = (GridView) view.findViewById(R.id.gv_prgms);
        txtPrgmTime = (TextView) view.findViewById(R.id.txt_on_air_prgm_time);
        txtPrgmName = (TextView) view.findViewById(R.id.txt_on_air_prgm_name);
        gvDate = (GridView) view.findViewById(R.id.gv_date);
        layoutDateEpg = (LinearLayout) view.findViewById(R.id.layout_date_epg);
        txtChannelName = (TextView) view.findViewById(R.id.txt_channel_name);


        txtOnAir = (TextView) view.findViewById(R.id.txt_on_air);
        txtDayView = (TextView) view.findViewById(R.id.txt_day);
        txtDateView = (TextView) view.findViewById(R.id.txt_date);

        Typeface medium = Typeface.createFromAsset(getActivity().getAssets(), "font/Exo2-Medium.otf");
        Typeface light = Typeface.createFromAsset(getActivity().getAssets(), "font/Exo2-Light.otf");

        txtOnAir.setTypeface(light);

        txtChannelName.setTypeface(light);
        txtDateView.setTypeface(light);
        txtDayView.setTypeface(light);

        txtPrgmName.setTypeface(light);
        txtPrgmTime.setTypeface(light);



    }
    private ArrayList<Calendar> getCalendarList(Channel channel) {
        final ArrayList<Calendar> calendarList = new ArrayList<>();
        /*if(channel.getDvrStartCalendar()!=null){
            //add dvr dates except today
            int diffDay =  Calendar.getInstance().get(Calendar.DAY_OF_YEAR) - channel.getDvrStartCalendar().get(Calendar.DAY_OF_YEAR);
            for(int i = diffDay ; i > 0 ;i--){
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_WEEK,-i);
                calendarList.add(calendar);
            }

            Logger.d("total no of dates with dvr",calendarList.size()+"");
        }*/
        //add today
        calendarList.add(Calendar.getInstance());
        //add epg dates

        for (int i = 1; i <= 6; i++) {
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_WEEK, i);
            calendarList.add(calendar);
        }

        return calendarList;
    }

    private void showError(Object errorCode) {
        if (errorCode instanceof String) {
//            String msg = ChannelErrorUtils.getErrorMessageFromCode(tvPlayCustomController, (String) errorCode);
            if(((String)errorCode).contains("MEDIA_ERROR"))
                Toast.makeText(tvPlayCustomController, "Couldnt play the requested video.", Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(tvPlayCustomController, (String)errorCode, Toast.LENGTH_SHORT).show();
        } else if (errorCode instanceof Integer) {
            int intErrorCode = (int) errorCode;
            switch (intErrorCode) {
                case R.string.err_code_epg_not_found:
                case R.string.war_code_buy_before_watch:
                case R.string.war_code_upgrade_before_watch:
                case R.string.err_code_server_unreachable:
                    //open fragment
                    layoutDateEpg.setVisibility(View.GONE);
                    layoutDateEpg.setVisibility(View.GONE);
                    Logger.e("channel category load", "error");
                    Bundle bundle = new Bundle();
                    bundle.putInt(TVPlayCustomController_.TAG_ERROR_CODE, intErrorCode);
                    Fragment fragment = new FragmentError();
                    fragment.setArguments(bundle);
                    try {
                        getChildFragmentManager().beginTransaction()
                                .replace(R.id.container_epg_dvr, fragment, "error")
                                .commit();
                    }catch (Exception e ){e.printStackTrace();}

                    break;
                default:
                    String msg = ChannelErrorUtils.getErrorMessageFromCode(tvPlayCustomController, (int) errorCode);
                    Toast.makeText(tvPlayCustomController, msg, Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    }

    private class ChannelOnItemClickListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final Channel channel = AllParser.getAllChannels().get(position);
            txtChannelName.setText(channel.getName());
            txtDateView.setText("");
            txtDayView.setText("");
            txtPrgmName.setText("");
            txtPrgmTime.setText("");

            gvDate.setAdapter(null);
            gvEpgDvr.setAdapter(null);
            Fragment child =getChildFragmentManager().findFragmentByTag("error");
            if(child!=null)
                getChildFragmentManager().beginTransaction().remove(child).commit();

            layoutDateEpg.setVisibility(View.VISIBLE);
            layoutDateEpg.setVisibility(View.VISIBLE);
//            tvPlayCustomController.progressBar.setVisibility(View.VISIBLE);
//            tvPlayCustomController.progressBar.bringToFront();

            new EpgLoadOption(tvPlayCustomController, channel) {
                @Override
                public void onSuccess(final HashMap<String, ArrayList<EPG>> epgHash) {
                    //if dvr exists then calender of dvr
                    //else calendat of epg
//                    tvPlayCustomController.progressBar.setVisibility(View.INVISIBLE);

                    Logger.d("Hoooooo","Epg load success");
                    ArrayList<Calendar> calendarList = getCalendarList(channel);
                    gvDate.setAdapter(new DateAdapter(tvPlayCustomController, R.layout.item_single_text, calendarList));
                    gvDate.setOnItemClickListener(new DateOnItemClickListener(calendarList, channel, epgHash));
                    gvDate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            DateAdapter adapter = (DateAdapter) gvDate.getAdapter();
                            adapter.setSelectedPosition(position);
                            adapter.notifyDataSetChanged();
                            gvDate.smoothScrollToPositionFromTop(position, gvDate.getHeight() / 2, 50);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

                    //auto click on todays date
                    String today = DateUtils.dateFormat.format(Calendar.getInstance().getTime());
                    for(int i = 0 ; i< calendarList.size();i++){
                        String calDate = DateUtils.dateFormat.format(calendarList.get(i).getTime());
                        if(calDate.equalsIgnoreCase(today)){
                            new DateOnItemClickListener(calendarList,channel,epgHash).onItemClick(null,null,i,0);
                            break;
                        }
                    }
                }

                @Override
                public void onError(Object errorCode) {
//                    tvPlayCustomController.progressBar.setVisibility(View.INVISIBLE);

                    Logger.e("TEST SADIP",errorCode.toString());
                    showError(errorCode);
                    /*String errMsg;
                    if(errorCode instanceof Integer)
                        errMsg = ChannelErrorUtils.getErrorMessageFromCode(getContext(),(Integer) errorCode);
                    else
                        errMsg = (String) errorCode;
                    Toast.makeText(getContext(),errMsg,Toast.LENGTH_SHORT).show();*/
                }
            }.getEpgHash();
        }
    }

    private class DateOnItemClickListener implements AdapterView.OnItemClickListener {
        private Channel channel;
        private HashMap<String, ArrayList<EPG>> epgHash;
        private ArrayList<Calendar> calendarList;
        private ArrayList<EPG> epgOfDay;


        public DateOnItemClickListener(ArrayList<Calendar> calendarList, Channel channel, HashMap<String, ArrayList<EPG>> epgHash) {
            this.calendarList = calendarList;
            this.channel = channel;
            this.epgHash = epgHash;
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            gvEpgDvr.setAdapter(null);
            DateAdapter adapter =(DateAdapter)gvDate.getAdapter();
            adapter.notifyDataSetChanged();
            gvDate.setSelection(position);
            gvDate.smoothScrollToPosition(position,50);
            final Calendar calendarOnDayClick = calendarList.get(position);
            String day = DateUtils.fullDayFormat.format(calendarOnDayClick.getTime());
            setEpgOfDay(epgHash.get(day));
            if(position == 0)
                epgOfDay = getFilteredEpgList(epgOfDay,calendarOnDayClick);

            gvEpgDvr.setAdapter(new EPGAdapter_(tvPlayCustomController, R.layout.item_text_img_hor, epgOfDay, calendarOnDayClick));
            gvEpgDvr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    EPGAdapter_ adapter = (EPGAdapter_) gvEpgDvr.getAdapter();
                    adapter.setPosition(position);
                    adapter.notifyDataSetChanged();
                    gvEpgDvr.smoothScrollToPositionFromTop(position, gvEpgDvr.getHeight() / 2, 50);


                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            populateOnAirProgAndMakeSelection(epgOfDay, calendarOnDayClick);
        }
        private ArrayList<EPG>getFilteredEpgList(ArrayList<EPG> epgOfDay,Calendar calendarOnDayClick){
            String strTimeInCalendar = DateUtils._24HrsTimeFormat.format(calendarOnDayClick.getTime());
            try {
                Date dtTimeInCalendar = DateUtils._24HrsTimeFormat.parse(strTimeInCalendar);
                for (Iterator<EPG> it = epgOfDay.iterator(); it.hasNext();) {
                    EPG epg = it.next();
                    Date prgmEndTime = DateUtils._24HrsTimeFormat.parse(epg.getEnd_time());
                    if(prgmEndTime.before(dtTimeInCalendar)){
                        it.remove();
                    }/*else break;*/

                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return epgOfDay;

        }

        private void populateOnAirProgAndMakeSelection(ArrayList<EPG> epgArrayListDay, Calendar calendar) {
            String inDate = DateUtils.dateFormat.format(calendar.getTime());
            String curDate = DateUtils.dateFormat.format(Calendar.getInstance().getTime());
            if (inDate.equalsIgnoreCase(curDate)) {
                txtOnAir.setText("ON AIR");
                for (int i = 0; i < epgArrayListDay.size(); i++) {
                    EPG epg = epgArrayListDay.get(i);
                    try {
                        String currentTimeInString = DateUtils._24HrsTimeFormat.format(Calendar.getInstance().getTime());
                        Date currentTime = DateUtils._24HrsTimeFormat.parse(currentTimeInString);
                        Date prgmStartTime = DateUtils._24HrsTimeFormat.parse(epg.getStart_time());
                        Date prgmEndTime = DateUtils._24HrsTimeFormat.parse(epg.getEnd_time());
                        if (prgmStartTime.before(currentTime) && prgmEndTime.after(currentTime) ||
                                prgmStartTime == currentTime ||
                                prgmEndTime == currentTime) {
                            txtPrgmTime.setText(epg.getStart_time() + " : " + epg.getEnd_time());
                            txtPrgmName.setText(epg.getProgram_name());
                            EPGAdapter_ adapter_ = (EPGAdapter_) gvEpgDvr.getAdapter();
                            adapter_.setPosition(i);
                            adapter_.notifyDataSetChanged();
                            gvEpgDvr.setSelection(i);
                            txtPrgmTime.setVisibility(View.VISIBLE);
                            txtPrgmName.setVisibility(View.VISIBLE);
                            break;
                        }
                    } catch (ParseException pe) {
                        pe.printStackTrace();
                    }
                }
            } else {
                if (Calendar.getInstance().getTime().before(calendar.getTime())) {
                    txtOnAir.setText("EPG");
                } else txtOnAir.setText("DVR");
                txtPrgmTime.setVisibility(View.INVISIBLE);
                txtPrgmName.setVisibility(View.INVISIBLE);
            }
            txtDayView.setText(DateUtils.fullDayFormat.format(calendar.getTime()));
            txtDateView.setText(inDate);
        }

        public ArrayList<EPG> getEpgList() {
            return epgOfDay;
        }
        private void setEpgOfDay(ArrayList<EPG>epgOfDay){
            this.epgOfDay = new ArrayList<>();
            this.epgOfDay.addAll(epgOfDay);

        }
    }

}
