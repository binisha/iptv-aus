package com.zeronebits.iptv.VideoPlayer;

import android.content.ComponentName;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubeIntents;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.google.android.youtube.player.YouTubeStandalonePlayer;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.wang.avi.AVLoadingIndicatorView;
import com.zeronebits.iptv.R;
import com.zeronebits.iptv.TotalCable;
import com.zeronebits.iptv.database.Database;
import com.zeronebits.iptv.entity.Channel;
import com.zeronebits.iptv.util.AllParser;
import com.zeronebits.iptv.util.EPGUtils.EPGMini;
import com.zeronebits.iptv.util.common.AppConfig;
import com.zeronebits.iptv.util.common.DateUtils;
import com.zeronebits.iptv.util.common.DownloadUtil;
import com.zeronebits.iptv.util.common.GetMac;
import com.zeronebits.iptv.util.common.LinkConfig;
import com.zeronebits.iptv.util.common.Mcrypt;
import com.zeronebits.iptv.util.common.logger.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Thread.sleep;


public class TVPlayCustomController_ extends AppCompatActivity implements
        SurfaceHolder.Callback {
    public static final String TAG_ERROR_CODE = "errorCode";
    public static final String PREVIEW_LINK = "previewLink";
    protected static final String TAG_CALENDAR = "calendar";
    protected static final String TAG_CURRENT_CHANNEL_ID = "channelId";
    protected static final String TAG_CURRENT_CATEGORY = "categoryName";
    protected static final int PLAYBACK_IDLE_CHECK_TIME = 5 * 60 * 60 * 1000; // 5 hour
    private static final int TIMER_EXIT_APP = 5 * 60 * 1000; // 5 minutes
    private static final String TAG = TVPlayCustomController_.class.getName();
    private static final int RECOVERY_DIALOG_REQUEST = 1;
    public Handler handlerToCloseMenu = new Handler();
    public int currentimageindex = 0;
    public MediaPlayer player;
    public int currentChannelId = 0;
    protected Database database;
    protected AVLoadingIndicatorView progressBar;
    protected Handler closeActivityHandler = new Handler();
    protected Handler idleTimerHandler = new Handler();
    protected VideoControllerView controller = null;
    ImageView videoStatus;
    LinearLayout backgroundTransparent;
    ImageView scrollimg;
    boolean flagAd = true;
    YouTubeFragment fragment;
    // public static final String YT_KEY= "AIzaSyC-x4wqzs_Bzbwyxk17LfeAE0U6V87MaVQ";

    private static String YT_KEY = "AIzaSyBwKJO2DOKXlT25syR4qfAV9InyzQ6tmaM";
    private int STANDALONE_PLAYER_REQUEST_CODE = 1;
    ImageView mainImage;
    protected Runnable idleTimeRunnable = new Runnable() {

        @Override
        public void run() {
            openErrorInFragment(R.string.war_code_playback_idle, currentChannelId);
            closeActivityHandler.postDelayed(closeActivityRunnable, TIMER_EXIT_APP);

        }

    };
    static int currentCategoryPosition;
    FrameLayout errorFrame;
    ArrayList<String> adImage;
    private TextView txtChangeChannelFromPriority;
    private TextView txtRandomDisplayBoxId;
    private SurfaceView videoSurface;
    private String strChangeChannelFromPriority = "";
    private Handler handlerChangeChannelFromNumbers = new Handler();
    private Runnable runnableChangeChannelFromNumbers = new Runnable() {
        @Override
        public void run() {
            changeChannelFromPriority();

        }
    };
    parsedImage parsedImage = null;

    String playlistID, isYoutube;
    private Runnable runnableToHideMac, runnableToShowMac;
    private Runnable runnableToCloseMenu = new Runnable() {
        @Override
        public void run() {
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container_movie_player);
            if (fragment != null) {
                if (!(fragment instanceof FragmentError)) {
                    if (player.isPlaying()) {
                        getSupportFragmentManager().beginTransaction().remove(fragment).commit();
                        //                        getSupportFragmentManager().getFragments().remove(fragment);
                        stopHandleToCloseMenu();
                        System.out.println("I am here another thread d");

                        flagAd = true;
                    }
                    Logger.d("CheckingonResumeorPause", "Close");
                }
            }
        }
    };

    private Thread closeFragmentThread = new Thread(new Runnable() {
        @Override
        public void run() {
            while (true) {
                final Fragment parent = getSupportFragmentManager().findFragmentByTag("parent");
                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (parent != null) {
                    try {

                        Fragment child = parent.getChildFragmentManager().findFragmentByTag("error");
                        try {
                            if (child == null && player.isPlaying()) {
                                if (parent instanceof FragmentDVR || parent instanceof FragmentEpg) {
                                    if (System.currentTimeMillis() - ((TotalCable) getApplication()).getLastUsed() > 10 * 1000) {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                getSupportFragmentManager().beginTransaction().remove(parent).commit();
                                                backgroundTransparent.setVisibility(View.GONE);
                                                System.out.println("I am here another thread");
                                                flagAd = true;

                                            }
                                        });

                                    }
                                } else {
                                    if (System.currentTimeMillis() - ((TotalCable) getApplication()).getLastUsed() > 5 * 1000) {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                getSupportFragmentManager().beginTransaction().remove(parent).commit();
                                                backgroundTransparent.setVisibility(View.GONE);
                                                System.out.println("I am here another thread");
                                                flagAd = true;

                                            }
                                        });

                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } catch (Exception e) {

                    }
                }
            }
        }
    });

    private Thread playingConditionAD = new Thread(new Runnable() {
        @Override
        public void run() {
            while (true) {

                final Fragment parent = getSupportFragmentManager().findFragmentByTag("parent");
                try {
                    sleep(6 * 1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                try {
                    if (parent == null && player.isPlaying()) {

                        System.out.println("I am here pLAYING");
                        try {
                            if (scrollimg.getVisibility() == View.VISIBLE) {
                                System.out.println("I am here VISIBLE");

                            } else {

                                if (parent == null && flagAd) {
                                    if (parsedImage == null) {
                                        parsedImage = new parsedImage(currentChannelId);
                                        parsedImage.execute();
                                        Logger.d("CheckingAdCondition", "LoadURL");

                                    }

                                }
                            }
                        } catch (Exception e) {
//                        scrollimg.setVisibility(View.VISIBLE);
                            Logger.d("CheckingAdCondition", "LoadURLCatch");

                            new parsedImage(currentChannelId).execute();
                            System.out.println("I am here catch");


                        }


                    } else {
                        playingConditionAD.stop();
                        try {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    System.out.println("here thread");
                                    scrollimg.setVisibility(View.GONE);

                                }
                            });
//                        if (scrollimg.getVisibility() == View.VISIBLE) {

//                        }
                        } catch (Exception e) {
                            e.printStackTrace();
                            scrollimg.setVisibility(View.GONE);
                        }
                    }
                } catch (Exception e) {

                }
            }
        }
    });
    protected Runnable closeActivityRunnable = new Runnable() {
        @Override
        public void run() {
            finish();
            closeFragmentThread.interrupt();
            flagAd = false;
        }
    };
    private Handler handlerToHideMac, handlerToShowMac;
    private Calendar calendar = null;
    ImageView recordedStatus;
    public static ImageView playPauseStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_player);
        findViewByIds();
        videoSurface = (SurfaceView) findViewById(R.id.videoSurface);

        SurfaceHolder videoHolder = videoSurface.getHolder();
        videoHolder.addCallback(this);
        player = new MediaPlayer();


        database = new Database(this);
        try {
            String id = database.getChannelId();
            currentChannelId = Integer.parseInt(id);

        } catch (Exception e) {
            Logger.d("CheckingError", "I am here");

            currentChannelId = 0;
        }
        randomDisplayMacAddress();
    }


    private void findViewByIds() {
//        youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_player_view);
        playPauseStatus = (ImageView) findViewById(R.id.img_play_pause);
        recordedStatus = (ImageView) findViewById(R.id.videoStatus);
        progressBar = (AVLoadingIndicatorView) findViewById(R.id.progressBar1);
        txtChangeChannelFromPriority = (TextView) findViewById(R.id.tv_setchannelfrompriority);
        txtRandomDisplayBoxId = (TextView) findViewById(R.id.tvboxID);
        txtRandomDisplayBoxId.setText(AppConfig.isDevelopment() ? AppConfig.getMacAddress() : GetMac.getMac(this));
        mainImage = (ImageView) findViewById(R.id.mainImage);
        backgroundTransparent = (LinearLayout) findViewById(R.id.transparentLoadingBackground);
        errorFrame = (FrameLayout) findViewById(R.id.container_movie_player);
        scrollimg = (ImageView) findViewById(R.id.scrollViewImage);
//        videoStatus = (ImageView) findViewById(R.id.videoStatus);
    }

    private void randomDisplayMacAddress() {
        final Random random = new Random();
        handlerToShowMac = new Handler();
        handlerToHideMac = new Handler();


        final FrameLayout.LayoutParams params =
                (FrameLayout.LayoutParams) txtRandomDisplayBoxId.getLayoutParams();

        runnableToHideMac = new Runnable() {

            @Override
            public void run() {
                params.setMargins(500, random.nextInt(500),
                        random.nextInt(200), random.nextInt(200));
                txtRandomDisplayBoxId.setLayoutParams(params);
                txtRandomDisplayBoxId.setVisibility(View.INVISIBLE);
                System.out.println("box is invisible");

                handlerToShowMac.postDelayed(runnableToShowMac, 5 * 1000 * 60);
            }
        };
        runnableToShowMac = new Runnable() {

            @Override
            public void run() {
                params.setMargins(500, random.nextInt(500),
                        random.nextInt(200), random.nextInt(200));

                txtRandomDisplayBoxId.bringToFront();
                txtRandomDisplayBoxId.setLayoutParams(params);
                txtRandomDisplayBoxId.setVisibility(View.VISIBLE);
                System.out.println("box is shown");

                handlerToHideMac.postDelayed(runnableToHideMac, 1000 * 7);
            }
        };

        handlerToShowMac.postDelayed(runnableToShowMac, 7 * 1000 * 60);

    }

    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {
        System.out.println("I am here is back");

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container_movie_player);

        if (fragment != null && fragment instanceof FragmentMain) {
            player.release();
            // finish();
            //setResult(0);

            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }

            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 4000);

//            if (player.isPlaying()) {
//                getSupportFragmentManager().beginTransaction().remove(fragment).commit();
//                getSupportFragmentManager().getFragments().remove(fragment);
//            } else {
//
//            }

        } else {
            flagAd = false;
//            playingConditionAD.interrupt();
            closeFragmentThread.interrupt();
            if (scrollimg.getVisibility() == View.VISIBLE) {
                System.out.println("I am here");
                scrollimg.setVisibility(View.GONE);
            }
            openFragmentMain(currentChannelId);

        }
    }


    private void startHandleToCloseMenu() {
        Logger.d("CheckingonResumeorPause", "Start");

        handlerToCloseMenu.postDelayed(runnableToCloseMenu, 60000);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        stopHandleToCloseMenu();
        Fragment fragment = getSupportFragmentManager().findFragmentByTag("parent");
        switch (keyCode) {

            case KeyEvent.KEYCODE_DPAD_CENTER:
                Fragment fragment1 = getSupportFragmentManager().findFragmentById(R.id.container_movie_player);

                if (fragment1 != null && fragment1 instanceof FragmentMain) {
                    player.release();
                    finish();

//            if (player.isPlaying()) {
//                getSupportFragmentManager().beginTransaction().remove(fragment).commit();
//                getSupportFragmentManager().getFragments().remove(fragment);
//            } else {
//
//            }

                } else {
                    flagAd = false;
//            playingConditionAD.interrupt();
                    closeFragmentThread.interrupt();
                    if (scrollimg.getVisibility() == View.VISIBLE) {
                        System.out.println("I am here");
                        scrollimg.setVisibility(View.GONE);
                    }
                    openFragmentMain(currentChannelId);

                }
            case 83:
            case 63:
            case KeyEvent.KEYCODE_MENU:
            case KeyEvent.KEYCODE_NUMPAD_ENTER:
            case KeyEvent.KEYCODE_INFO:
                if (fragment == null) {
                    flagAd = false;
//                    playingConditionAD.interrupt();
                    openFragment(false, null, currentChannelId);
                } else if (player.isPlaying() && fragment != null) {
                    getSupportFragmentManager().beginTransaction().remove(fragment).commit();

                } else {
                    return super.onKeyDown(keyCode, event);
                }
                return true;
            case KeyEvent.KEYCODE_MEDIA_STOP:
                if (player.isPlaying()) {
                    player.stop();
                    if (controller == null) {
                        openFragment(false, null, currentChannelId);
                    } else {
                        openFragment(true, null, currentChannelId);
                    }
                }

            case KeyEvent.KEYCODE_DPAD_DOWN:
                if (controller != null && fragment == null) {
                    controller.show();
                    controller.videoInfo.setVisibility(View.GONE);

                    return true;
                } else if (fragment == null && (controller == null)) {
                    try {
                        new ChannelLinkLoader(AllParser.getPrevChannel(currentChannelId, AllParser.categorynames.get(currentCategoryPosition)).getId()).execute();
                    } catch (NullPointerException npe) {
                        Toast.makeText(this, "Prev channel not found", Toast.LENGTH_SHORT).show();
                    }
                }
                return super.onKeyDown(keyCode, event);
            case KeyEvent.KEYCODE_DPAD_UP:
                if (fragment == null && controller == null) {
                    Logger.d("CheckingUpKeyClick", "if");

                    try {
                        new ChannelLinkLoader(AllParser.getNextChannel(currentChannelId, AllParser.categorynames.get(currentCategoryPosition)).getId()).execute();
                        return true;
                    } catch (NullPointerException e) {
                        Toast.makeText(this, "Next channel not found", Toast.LENGTH_SHORT).show();
                    }
                }
                return super.onKeyDown(keyCode, event);

            case 87:// media next or ch+
            case KeyEvent.KEYCODE_PAGE_UP:
            case KeyEvent.KEYCODE_CHANNEL_UP:
                try {
                    new ChannelLinkLoader(AllParser.getNextChannel(currentChannelId, AllParser.categorynames.get(currentCategoryPosition)).getId()).execute();
                    return true;
                } catch (NullPointerException npe) {
                    Toast.makeText(this, "Next channel not found", Toast.LENGTH_SHORT).show();
                }
                return super.onKeyDown(keyCode, event);
            case 88:// media prev or ch-
            case KeyEvent.KEYCODE_PAGE_DOWN:
            case KeyEvent.KEYCODE_CHANNEL_DOWN:
                try {
                    new ChannelLinkLoader(AllParser.getPrevChannel(currentChannelId, AllParser.categorynames.get(currentCategoryPosition)).getId()).execute();
                } catch (NullPointerException npe) {
                    Toast.makeText(this, "Prev channel not found", Toast.LENGTH_SHORT).show();
                }
                return true;
            /**
             * number keycode events are handled seperately
             */


            case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
                if (player.isPlaying()) {
                    player.pause();
                    playPauseStatus.setVisibility(View.VISIBLE);
                    playPauseStatus.bringToFront();
                    return true;

                } else
                    try {
                        player.start();
                        playPauseStatus.setVisibility(View.GONE);
                        return true;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                return super.onKeyDown(keyCode, event);

            case KeyEvent.KEYCODE_MEDIA_REWIND:
            case KeyEvent.KEYCODE_DPAD_LEFT:
                if (controller != null && fragment == null && !controller.isShowing()) {
                    player.seekTo(player.getCurrentPosition() - 15000);
                    return true;
                }
                return super.onKeyDown(keyCode, event);

            case KeyEvent.KEYCODE_MEDIA_FAST_FORWARD:
            case KeyEvent.KEYCODE_DPAD_RIGHT:
                if (controller != null && fragment == null && !controller.isShowing()) {
                    player.seekTo(player.getCurrentPosition() + 15000);
                    return true;
                }
                return super.onKeyDown(keyCode, event);
            case KeyEvent.KEYCODE_MEDIA_PLAY:
            case 44:
                if (!player.isPlaying()) {
                    try {
                        player.start();
                        playPauseStatus.setVisibility(View.GONE);
                        return true;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                return super.onKeyDown(keyCode, event);
            case KeyEvent.KEYCODE_MEDIA_PAUSE:
            case 7:
                if (player.isPlaying()) {
                    player.pause();
                    playPauseStatus.setVisibility(View.VISIBLE);
                    playPauseStatus.bringToFront();
                    return true;
                }
                return super.onKeyDown(keyCode, event);

            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
                return numberKeyCodeEvent(keyCode);
            default:
                return super.onKeyDown(keyCode, event);
        }


    }

    @Override
    protected void onPause() {
        player.release();
//        closeFragmentThread.interrupt();
        flagAd = false;
//        playingConditionAD.interrupt();

        finish();
        try {
            scrollimg.setVisibility(View.GONE);
//            unregisterReceiver(MultipleUsedStuffs.broadcastDoubleLoginCheck);
        } catch (Exception e) {

        }
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        flagAd = true;
//        MultipleUsedStuffs.SetupMultiLogin(TVPlayCustomController_.this);

        ((TotalCable) this.getApplication()).setVideoPlaying(true);
        try {
            new ChannelLinkLoader(Integer.parseInt(database.getChannelId())).execute();
        } catch (Exception e) {
            openErrorInFragment(R.string.war_code_no_channels_in_db, 0);
        }
        closeFragmentThread.start();
        startHandleToCloseMenu();


    }

    public void openErrorInFragment(Object error, int channelId) {

        if (player != null) {
            if (!player.isPlaying()) mainImage.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        }
        Bundle bundle = new Bundle();
        bundle.putInt(TAG_CURRENT_CHANNEL_ID, channelId);

        if (error instanceof String) {
            bundle.putString(TAG_ERROR_CODE, error.toString());
        } else if (error instanceof Integer) {
            bundle.putInt(TAG_ERROR_CODE, (int) error);
        }

        Fragment nextFrag;

        nextFrag = new FragmentMain();
        nextFrag.setArguments(bundle);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container_movie_player, nextFrag)
                .addToBackStack("parent")
                .commit();
        stopHandleToCloseMenu();
    }

    public void stopHandleToCloseMenu() {
        Logger.d("CheckingonResumeorPause", "Stop");
        handlerToCloseMenu.removeCallbacks(runnableToCloseMenu);
    }

    private boolean numberKeyCodeEvent(int keyCode) {

        stopHandlerChangeChannelFromNumbers();
        switch (keyCode) {
            case 7:
                txtChangeChannelFromPriority.setVisibility(View.VISIBLE);
                txtChangeChannelFromPriority.bringToFront();
                if (txtChangeChannelFromPriority.getText().length() < 4) {
                    txtChangeChannelFromPriority.setText(txtChangeChannelFromPriority
                            .getText() + "0");
                    strChangeChannelFromPriority = txtChangeChannelFromPriority
                            .getText() + "";
                    startHandlerChangeChannelFromNumbers();
                    // condnsetchannelfrompriority();
                }

                return true;

            case 8:
                txtChangeChannelFromPriority.setVisibility(View.VISIBLE);
                txtChangeChannelFromPriority.bringToFront();
                if (txtChangeChannelFromPriority.getText().length() < 4) {
                    txtChangeChannelFromPriority.setText(txtChangeChannelFromPriority
                            .getText() + "1");
                    strChangeChannelFromPriority = txtChangeChannelFromPriority
                            .getText() + "";
                    startHandlerChangeChannelFromNumbers();
                    // condnsetchannelfrompriority();
                }
                return true;
            case 9:
                txtChangeChannelFromPriority.setVisibility(View.VISIBLE);
                txtChangeChannelFromPriority.bringToFront();
                if (txtChangeChannelFromPriority.getText().length() < 4) {
                    txtChangeChannelFromPriority.setText(txtChangeChannelFromPriority
                            .getText() + "2");
                    strChangeChannelFromPriority = txtChangeChannelFromPriority
                            .getText() + "";
                    startHandlerChangeChannelFromNumbers();
                    // condnsetchannelfrompriority();
                }
                return true;
            case 10:
                txtChangeChannelFromPriority.setVisibility(View.VISIBLE);
                txtChangeChannelFromPriority.bringToFront();
                if (txtChangeChannelFromPriority.getText().length() < 4) {
                    txtChangeChannelFromPriority.setText(txtChangeChannelFromPriority
                            .getText() + "3");
                    strChangeChannelFromPriority = txtChangeChannelFromPriority
                            .getText() + "";
                    startHandlerChangeChannelFromNumbers();
                    // condnsetchannelfrompriority();
                }
                return true;
            case 11:
                txtChangeChannelFromPriority.setVisibility(View.VISIBLE);
                txtChangeChannelFromPriority.bringToFront();
                if (txtChangeChannelFromPriority.getText().length() < 4) {
                    txtChangeChannelFromPriority.setText(txtChangeChannelFromPriority
                            .getText() + "4");
                    strChangeChannelFromPriority = txtChangeChannelFromPriority
                            .getText() + "";
                    startHandlerChangeChannelFromNumbers();
                    // condnsetchannelfrompriority();
                }
                return true;
            case 12:
                txtChangeChannelFromPriority.setVisibility(View.VISIBLE);
                txtChangeChannelFromPriority.bringToFront();
                if (txtChangeChannelFromPriority.getText().length() < 4) {
                    txtChangeChannelFromPriority.setText(txtChangeChannelFromPriority
                            .getText() + "5");
                    strChangeChannelFromPriority = txtChangeChannelFromPriority
                            .getText() + "";
                    startHandlerChangeChannelFromNumbers();
                    // condnsetchannelfrompriority();
                }
                return true;
            case 13:
                txtChangeChannelFromPriority.setVisibility(View.VISIBLE);
                txtChangeChannelFromPriority.bringToFront();
                if (txtChangeChannelFromPriority.getText().length() < 4) {
                    txtChangeChannelFromPriority.setText(txtChangeChannelFromPriority
                            .getText() + "6");
                    strChangeChannelFromPriority = txtChangeChannelFromPriority
                            .getText() + "";
                    startHandlerChangeChannelFromNumbers();
                    // condnsetchannelfrompriority();
                }
                return true;
            case 14:
                txtChangeChannelFromPriority.setVisibility(View.VISIBLE);
                txtChangeChannelFromPriority.bringToFront();
                if (txtChangeChannelFromPriority.getText().length() < 4) {
                    txtChangeChannelFromPriority.setText(txtChangeChannelFromPriority
                            .getText() + "7");
                    strChangeChannelFromPriority = txtChangeChannelFromPriority
                            .getText() + "";
                    startHandlerChangeChannelFromNumbers();
                    // condnsetchannelfrompriority();
                }
                return true;
            case 15:
                txtChangeChannelFromPriority.setVisibility(View.VISIBLE);
                txtChangeChannelFromPriority.bringToFront();
                if (txtChangeChannelFromPriority.getText().length() < 4) {
                    txtChangeChannelFromPriority.setText(txtChangeChannelFromPriority
                            .getText() + "8");
                    strChangeChannelFromPriority = txtChangeChannelFromPriority
                            .getText() + "";
                    startHandlerChangeChannelFromNumbers();
                    // condnsetchannelfrompriority();
                }
                return true;
            case 16:
                txtChangeChannelFromPriority.setVisibility(View.VISIBLE);
                txtChangeChannelFromPriority.bringToFront();
                if (txtChangeChannelFromPriority.getText().length() < 4) {
                    txtChangeChannelFromPriority.setText(txtChangeChannelFromPriority
                            .getText() + "9");
                    strChangeChannelFromPriority = txtChangeChannelFromPriority
                            .getText() + "";
                    startHandlerChangeChannelFromNumbers();
                    // condnsetchannelfrompriority();
                }
                return true;

            default:
                return false;
        }

    }

    private void stopHandlerChangeChannelFromNumbers() {
        handlerChangeChannelFromNumbers.removeCallbacks(runnableChangeChannelFromNumbers);
    }

    private void startHandlerChangeChannelFromNumbers() {
        handlerChangeChannelFromNumbers.postDelayed(runnableChangeChannelFromNumbers, 2000);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        player.setDisplay(holder);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    private void changeChannelFromPriority() {
        txtChangeChannelFromPriority.setVisibility(View.GONE);

        int counter = 0;
        for (Channel channel : AllParser.getAllChannels()) {
            counter++;
            int priority = channel.getChannelPriority();
            if (strChangeChannelFromPriority.equals(priority + "")) {
                stopHandlerChangeChannelFromNumbers();
                txtChangeChannelFromPriority.setText("");
                new ChannelLinkLoader(channel.getId()).execute();
//                loadObjectToPlay(channel);
                break;
            }
        }
        //counter completed but no channels found whith the priority
        if (counter == AllParser.getAllChannels().size()) {
            txtChangeChannelFromPriority.setText("");
            txtChangeChannelFromPriority.setVisibility(View.GONE);
            Toast.makeText(this, "Sorry no associated channel ", Toast.LENGTH_SHORT).show();
        }


    }

    protected void playChannelUrl(final int channelId, String channelUrl) {
        recordedStatus.setVisibility(View.INVISIBLE);
        playPauseStatus.setVisibility(View.GONE);

        flagAd = true;
        Logger.d("CheckingStatusDVR", "I am heres");
        try {
            player.reset();
        } catch (Exception e) {
            Logger.d("CheckingStatusDVR", "Player not reset");

        }
        try {
            parsedImage = null;
            playingConditionAD.start();
        } catch (Exception e) {
            e.printStackTrace();

        }
        try {

            Logger.d("CheckingStatusDVR", "try to play");

            player.setAudioStreamType(AudioManager.STREAM_MUSIC);
//          player.setDataSource(this, Uri.parse("http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4"));
            player.setDataSource(this, Uri.parse(channelUrl));
            player.prepareAsync();
            player.setOnPreparedListener(new OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    progressBar.setVisibility(View.GONE);
                    backgroundTransparent.setVisibility(View.GONE);
                    closeFragmentMainOnChannelPlay();
                    videoSurface.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent service = new Intent();
                            service.setComponent(new ComponentName("com.mytv.MyTVHome",
                                    "com.mytv.utils.FullScreenVideoService"));
                            Logger.e(TAG + ": on prepared", "call full screen");
                            service.putExtra("fullscreen", true);
                            startService(service);

                        }
                    }, 1000);

                    player.start();
                    currentChannelId = channelId;
                }
            });
            player.setOnErrorListener(new OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    try {
                        player.reset();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Logger.d("CheckingStatusDVR", "Error");

                    Logger.d("WHAT: " + what, "EXTRA: " + extra);

                    try {
                        player.reset();
                    } catch (Exception e) {

                    }
                    switch (what) {
                        case -38:
                            /**
                             * start called in state 1
                             * start called before set data source
                             */
                            new ChannelLinkLoader(channelId).execute();
                            break;
            /*case 100:
                //media server died
                player.stop();
                player.start();
            */
                        default:
                            try {
                                player.reset();
                            } catch (Exception e) {
                            }
                            StringBuilder sb = new StringBuilder().append("MEDIA_ERROR:\t").append("W").append(what).append("E").append(extra);
                            openErrorInFragment(sb.toString(), channelId);

                    }

                    return true;
                }
            });

            controller = null;

        } catch (IllegalArgumentException | SecurityException | IllegalStateException e) {
            e.printStackTrace();
            openErrorInFragment(e.getMessage(), channelId);
            Logger.d("CheckingStatusDVR", "Illegal");

        } catch (IOException e) {
            e.printStackTrace();
            openErrorInFragment(e.getMessage(), channelId);
        }

    }

    public void closeFragmentMainOnChannelPlay() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container_movie_player);
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction().remove(fragment).commit();
//            getSupportFragmentManager().getFragments().remove(fragment);
            stopHandleToCloseMenu();
            Logger.d("CheckingonResumeorPause", "Close");
            flagAd = true;
        }
    }

    private long getSeekPosition(String videoStartTime, String epg_start_time) {
        long seekPos = 0;
        try {
            Date videoTime = DateUtils._24HrsTimeFormat.parse(videoStartTime);
            Calendar videoCal = Calendar.getInstance();
            videoCal.setTime(videoTime);
            Date epgTime = DateUtils._24HrsTimeFormat.parse(epg_start_time);
            Calendar epgCal = Calendar.getInstance();
            epgCal.setTime(epgTime);

            seekPos = epgCal.getTimeInMillis() - videoCal.getTimeInMillis();

            Logger.d("seekDuration: epgCal.getTimeInMillis", epgCal.getTimeInMillis() + "");
            Logger.d("seekDuration: videoCal.getTimeInMillis", videoCal.getTimeInMillis() + "");
            Logger.d("seekDuration:", seekPos + "");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return seekPos;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (controller != null) {
            controller.show();
            controller.videoInfo.setVisibility(View.GONE);
        }
        return super.onTouchEvent(event);
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        Logger.d("CheckingonResumeorPause", "UserInteratcion");
        ((TotalCable) this.getApplication()).active();
        idleTimerHandler.removeCallbacks(idleTimeRunnable);
        idleTimerHandler.postDelayed(idleTimeRunnable, PLAYBACK_IDLE_CHECK_TIME);
        stopHandleToCloseMenu();
        startHandleToCloseMenu();
    }

    public void playDvrVideo(Channel channel, final EPGMini epgMini) {
        TVPlayCustomController_.this.currentChannelId = channel.getId();
        TVPlayCustomController_.this.calendar = epgMini.getStartCalendar();
        new DVRPlay(TVPlayCustomController_.this, channel, epgMini, player) {
            @Override
            public void onError(boolean openDVR, Object err) {
                progressBar.setVisibility(View.GONE);
                backgroundTransparent.setVisibility(View.GONE);
                Toast.makeText(TVPlayCustomController_.this, err.toString(), Toast.LENGTH_SHORT).show();
//                openFragment(openDVR, err, currentChannelId);
            }

            @Override
            protected void myOnComplete(MediaPlayer mp) {
                openFragment(true, null, currentChannelId);
            }


            @Override
            protected void myOnPrepared(final int channelId, String nextVideoName) {
                currentChannelId = channelId;
                recordedStatus.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                backgroundTransparent.setVisibility(View.GONE);
                mainImage.setVisibility(View.GONE);
//                videoStatus.setVisibility(View.VISIBLE);

                videoSurface.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent service = new Intent();
                        service.setComponent(new ComponentName("com.mytv.MyTVHome",
                                "com.mytv.utils.FullScreenVideoService"));
                        service.putExtra("fullscreen", true);
                        startService(service);

                    }
                }, 1000);


                controller = new VideoControllerView(TVPlayCustomController_.this, true);
                controller.setAnchorView((FrameLayout) findViewById(R.id.videoSurfaceContainer));
                controller.setMediaPlayer(new VideoControllerView.MediaPlayerControl() {
                    @Override
                    public void start() {
                        playPauseStatus.setVisibility(View.GONE);
                        player.start();
                    }

                    @Override
                    public void pause() {
                        playPauseStatus.setVisibility(View.VISIBLE);
                        playPauseStatus.bringToFront();
                        player.pause();
                    }

                    @Override
                    public int getDuration() {
                        try {
                            return player.getDuration();
                        } catch (Exception e) {
                            e.printStackTrace();
                            return 0;
                        }
                    }

                    @Override
                    public int getCurrentPosition() {
                        return player.getCurrentPosition();
                    }

                    @Override
                    public void seekTo(int pos) {
                        player.seekTo(pos);
                    }

                    @Override
                    public boolean isPlaying() {
                        return player.isPlaying();
                    }

                    @Override
                    public int getBufferPercentage() {
                        return 0;
                    }

                    @Override
                    public boolean canPause() {
                        return true;
                    }

                    @Override
                    public boolean canSeekBackward() {
                        return true;
                    }

                    @Override
                    public boolean canSeekForward() {
                        return true;
                    }

                    @Override
                    public Channel channel() {
                        return AllParser.getChannelFromId(currentChannelId);
                    }
                }, player);
                controller.setPrevNextListeners(new View.OnClickListener() {
                    //epg played from calendar instead of position
                    //get calendar exact with date and epg end time to play next or prev epg
                    @Override
                    public void onClick(View v) {
                        //prev
                        controller.hide();
                        Bundle bundle = new Bundle();
                        bundle.putInt(TAG_CURRENT_CHANNEL_ID, channelId);
                        bundle.putSerializable(TAG_CALENDAR, calendar);

                        Fragment nextFrag = new FragmentDVR();
                        nextFrag.setArguments(bundle);
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.container_movie_player, nextFrag, "parent")
                                .commit();
                    }
                }, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //next
                        controller.hide();
                        Bundle bundle = new Bundle();
                        bundle.putInt(TAG_CURRENT_CHANNEL_ID, channelId);
                        bundle.putSerializable(TAG_CALENDAR, calendar);

                        Fragment nextFrag = new FragmentDVR();
                        nextFrag.setArguments(bundle);
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.container_movie_player, nextFrag, "parent")
                                .commit();
                    }
                });
                controller.setMenuInfoListeners(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //menu
                        controller.hide();
                        Bundle bundle = new Bundle();
                        bundle.putInt(TAG_CURRENT_CHANNEL_ID, channelId);
                        bundle.putSerializable(TAG_CALENDAR, calendar);

                        Fragment nextFrag = new FragmentDVR();
                        nextFrag.setArguments(bundle);
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.container_movie_player, nextFrag, "parent")
                                .commit();
                    }
                }, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //dvr
                        //open dvr passing calendar
                        controller.videoInfo.setText((DateUtils._24HrsTimeFormat.format(epgMini.getStartCalendar().getTime())) + " | " + epgMini.getPrgmName() + " | " + (DateUtils._24HrsTimeFormat.format(epgMini.getEndCalendar().getTime())));
                        controller.videoInfo.setVisibility(View.VISIBLE);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                Logger.d("CheckingAdCondition", "LoadImageAnimatedHide");
                                controller.videoInfo.setVisibility(View.INVISIBLE);
                            }
                        }, 10 * 1000);

                        /*controller.hide();

                        Bundle bundle = new Bundle();
                        bundle.putInt(TAG_CURRENT_CHANNEL_ID, channelId);
                        bundle.putSerializable(TAG_CALENDAR, calendar);

                        Fragment nextFrag = new FragmentDVR();
                        nextFrag.setArguments(bundle);
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.container_movie_player, nextFrag, "parent")
                                .commit();*/
                    }
                });
                player.start();
            }

            @Override
            protected void myPreExecute() {
                progressBar.setVisibility(View.VISIBLE);
                backgroundTransparent.setVisibility(View.VISIBLE);
                progressBar.bringToFront();
                backgroundTransparent.bringToFront();
                mainImage.setVisibility(View.GONE);
//                videoStatus.setVisibility(View.GONE);
            }
        }.execute();

    }

    private void openFragmentMain(int channelId) {
        progressBar.setVisibility(View.GONE);
        Fragment nextFrag = new FragmentMain();
        Bundle bundle = new Bundle();
        bundle.putInt(TAG_CURRENT_CHANNEL_ID, channelId);
        nextFrag.setArguments(bundle);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container_movie_player, nextFrag, "parent")
                .commit();
        startHandleToCloseMenu();
    }

    protected void openFragment(boolean IsDVRError, Object error, int channelId) {
        if (!player.isPlaying()) mainImage.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);

        Bundle bundle = new Bundle();
        bundle.putInt(TAG_CURRENT_CHANNEL_ID, channelId);
        playPauseStatus.setVisibility(View.GONE);

        if (error instanceof String) {
            bundle.putString(TAG_ERROR_CODE, error.toString());
        } else if (error instanceof Integer) {
            bundle.putInt(TAG_ERROR_CODE, (int) error);
        }


        Fragment nextFrag;
        if (IsDVRError) {
            nextFrag = new FragmentDVR();
            bundle.putSerializable(TAG_CALENDAR, calendar);
        } else {
            nextFrag = new FragmentMain();
        }
        nextFrag.setArguments(bundle);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container_movie_player, nextFrag, "parent")
                .commit();
    }

    public void makescroll() {
        final Handler mHandler = new Handler();

        // Create runnable for posting
        final Runnable mUpdateResults = new Runnable() {
            public void run() {
                Logger.d("CheckingAdCondition", "LoadImageAnimated");
                AnimateandSlideShow();

            }
        };
        int delay = 1 * 1000; // delay for 1 sec.

        int period = 30 * 60 * 1000; // repeat every 4 sec.

        Timer timer = new Timer();

        timer.scheduleAtFixedRate(new TimerTask() {

            public void run() {
                mHandler.post(mUpdateResults);
            }

        }, delay, period);

    }

    private void AnimateandSlideShow() {
        System.out.println("here in animatedslideshow");
//        if (flagAd) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Logger.d("CheckingAdCondition", "LoadImageAnimatedHide");
                scrollimg.setVisibility(View.INVISIBLE);
            }
        }, 10 * 1000);
        scrollimg.setVisibility(View.VISIBLE);
//        } else {
//            scrollimg.setVisibility(View.GONE);
//        }
        scrollimg.setScaleType(ImageView.ScaleType.FIT_XY);

        try {
            UrlImageViewHelper.setUrlDrawable(scrollimg, adImage.get(currentimageindex % adImage.size()));
        } catch (Exception e) {
            e.printStackTrace();
//            scrollimg.setImageDrawable(TVPlayCustomController_.this.getResources().getDrawable(R.drawable.ad));
        }


        currentimageindex++;

    }


    public class ChannelLinkLoader extends AsyncTask<Void, Void, String> {
        private int channelId;

        public ChannelLinkLoader(int channelId) {
            this.channelId = channelId;
//            currentChannel = nowChannel;
        }

        @Override
        protected String doInBackground(Void... params) {
            DownloadUtil getUtc = new DownloadUtil(LinkConfig.GET_UTC, TVPlayCustomController_.this);
            String utc = getUtc.downloadStringContent();
            if (!utc.equals(DownloadUtil.NotOnline)
                    && !utc.equals(DownloadUtil.ServerUnrechable)) {
                String link = LinkConfig.getString(TVPlayCustomController_.this,
                        LinkConfig.CHANNEL_LINK_URL)
                        + "?" + "channelID=" + channelId
                        + "&" + LinkConfig.getHashCode(utc);

                DownloadUtil dUtil = new DownloadUtil(link, TVPlayCustomController_.this);
                Logger.d(".ChannelLinkLoader", link);

                return dUtil.downloadStringContent();
            } else
                return utc;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            try{
//                unregisterReceiver(``,UsedStuffs.broadcastDoubleLoginCheck);
//                MultipleUsedStuffs.SetupMultiLogin(TVPlayCustomController_.this);
//            }catch (Exception e){
//            }
            progressBar.setVisibility(View.VISIBLE);
            backgroundTransparent.setVisibility(View.VISIBLE);
//            errorFrame.setBackgroundColor(getColor(R.color.transp));
            backgroundTransparent.bringToFront();
            progressBar.bringToFront();
            mainImage.setVisibility(View.GONE);
//            videoStatus.setImageResource(R.drawable.status_live);
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            if (result.equalsIgnoreCase(DownloadUtil.NotOnline) || result.equalsIgnoreCase(DownloadUtil.ServerUnrechable)) {
                progressBar.setVisibility(View.GONE);
                openErrorInFragment(R.string.err_code_server_unreachable, channelId);
            } else {
                if(fragment != null){
                    getSupportFragmentManager().beginTransaction().remove(fragment);
                }
                try {
                    Logger.d(TAG + "channelUrl", result);

                    JSONObject root = new JSONObject(result);
                    String channelUrl = root.getString("link");

                    String channelServerType = root.getString("server_type");

                    if (channelServerType.equals("1")) {
                        Mcrypt mCrypt = new Mcrypt();
                        try {
                            channelUrl = (new String(mCrypt.decrypt(channelUrl))).trim();
                            Logger.d(TAG + "channelUrl decrypted", channelUrl + "");
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                    try {
                        URL stringUrl = new URL(channelUrl);
                        String[] ourUrl = channelUrl.replace(".", " ").split(" ");


                        if (ourUrl[1].equals("youtube")) {

                            String pattern = "(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";

                            Pattern compiledPattern = Pattern.compile(pattern);
                            Matcher matcher = compiledPattern.matcher(channelUrl);

                            if (matcher.find()) {
                                System.out.println(matcher.group());
                                playlistID = matcher.group();
                            }
                            try {
                                player.reset();
                            } catch (Exception e) {
                            }

                            fragment = YouTubeFragment.newInstance(playlistID);
                            getSupportFragmentManager().beginTransaction().replace(R.id.container_movie_player, fragment).commit();
                          /*  if (YouTubeIntents.canResolvePlayVideoIntent(TVPlayCustomController_.this)) {
                                //Opens in the StandAlonePlayer, defaults to fullscreen
                                startActivity(YouTubeStandalonePlayer.createVideoIntent(TVPlayCustomController_.this,
                                        YT_KEY, playlistID));
                            }*/
//                            youTubePlayerView.initialize(YT_KEY, this);
//                            startActivity(YouTubeIntents.createPlayVideoIntentWithOptions(TVPlayCustomController_.this,playlistID, true, true));

                           /* Intent videoint = YouTubeStandalonePlayer.createVideoIntent(
                                    TVPlayCustomController_.this, YT_KEY, playlistID, 0,
                                    true, false);
                            startActivityForResult(videoint, STANDALONE_PLAYER_REQUEST_CODE);*/


//                            Intent i = new Intent(TVPlayCustomController_.this, youtubeMobilePlayActivity.class);
//
//                            i.putExtra("playlist_id", playlistID);
//
//                            i.putExtra("position", 0);
//
//                            startActivity(i);

                        } else {

//                    setCommonValuesToUI(channelTemp);
                            database.deleteChannel();
                            database.insertChannel(channelId + "");
                            playChannelUrl(channelId, channelUrl);

                        }


                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }


                    Logger.d("channelTemplink", channelUrl);
                } catch (JSONException je) {
                    je.printStackTrace();
                    progressBar.setVisibility(View.GONE);
                    Logger.d(TAG + ChannelLinkLoader.class.getSimpleName() + "error json", result);
                    try {
                        JSONObject Jobj = new JSONObject(result);
                        String errorCode = Jobj.getString("error_code");
                        String errorMessage = Jobj.getString("error_message");
                        if (errorCode.equals("307")) {
                            openErrorInFragment(R.string.war_code_buy_before_watch, channelId);
                        } else if (errorCode.equals("308")) {
                            openErrorInFragment(R.string.war_code_upgrade_before_watch, channelId);

                        } else {
                            openErrorInFragment(errorMessage, channelId);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        openErrorInFragment(R.string.err_code_json_exception, channelId);
                        Logger.d(TAG + ChannelLinkLoader.class.getSimpleName() + " error json", "parsing error");
                    }

                }

            }
        }
    }


    private class parsedImage extends AsyncTask<String, String, String> {
        private int channelId;

        @Override
        protected String doInBackground(String... params) {
            DownloadUtil getUtc = new DownloadUtil(LinkConfig.GET_UTC, TVPlayCustomController_.this);
            String utc = getUtc.downloadStringContent();
            if (!utc.equals(DownloadUtil.NotOnline)
                    && !utc.equals(DownloadUtil.ServerUnrechable)) {
                String link = LinkConfig.getString(TVPlayCustomController_.this,
                        LinkConfig.AD_URL)
                        + "?" + "channel_id=" + channelId
                        + "&" + LinkConfig.getHashCode(utc);

                DownloadUtil dUtil = new DownloadUtil(link, TVPlayCustomController_.this);
                Logger.d(".ChannelLinkLoad" +
                        "er", link);

                return dUtil.downloadStringContent();
            } else
                return utc;

        }

        public parsedImage(int channelId) {
            this.channelId = channelId;
//            currentChannel = nowChannel;
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {

                System.out.println(result);
                JSONArray jsonarray = new JSONArray(result);
                adImage = new ArrayList<String>();
                for (int i = 0; i < jsonarray.length(); i++) {
                    JSONObject jsonobject = jsonarray.getJSONObject(i);
                    String imageLink = jsonobject.getString("link");
                    adImage.add(imageLink);

                }
                Logger.d("CheckingAdCondition", "LoadURLMake");

                flagAd = false;
                makescroll();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


    }

    /*  @Override
      protected void onActivityResult(int requestCode, int resultCode, Intent data) {
          super.onActivityResult(requestCode, resultCode, data);


          if (requestCode == STANDALONE_PLAYER_REQUEST_CODE
                  && resultCode != RESULT_OK) {
  //            setResult(0);
            *//*  YouTubeInitializationResult errorReason = YouTubeStandalonePlayer
                    .getReturnedInitializationResult(data);
            if (errorReason.isUserRecoverableError()) {
                errorReason.getErrorDialog(this, 0).show();
            } else {


                // String errorMessage = String.format("PLAYER ERROR!!",
                // errorReason.toString());

            }*//*
        }
    }*/
    @Override
    protected void onStop() {
        super.onStop();
    }


}


