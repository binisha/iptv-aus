package com.zeronebits.iptv.VideoPlayer;

import android.content.ComponentName;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import android.widget.VideoView;

import com.zeronebits.iptv.R;


/**
 * Created by NITV on 14/09/2016.
 */
public class PreviewFragment extends Fragment {

    VideoView vidView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.preview_fragment, container, false);
        Bundle bundle = getArguments();
        String url = bundle.getString(TVPlayCustomController_.PREVIEW_LINK);
        vidView = (VideoView)view.findViewById(R.id.preview) ;
        vidView.setVideoPath(url);
        vidView.setOnPreparedListener( new MediaPlayer.OnPreparedListener(){

            @Override
            public void onPrepared(MediaPlayer mp) {
                mp.setVolume(0f, 0f);
                vidView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent service = new Intent();
                        service.setComponent(new ComponentName("com.mytv.MyTVHome",
                                "com.mytv.utils.FullScreenVideoService"));
                        service.putExtra("fullscreen", true);
                        getActivity().startService(service);

                    }
                }, 1000);
                vidView.start();
            }

        });

        vidView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                Toast.makeText(getActivity(), "Cannot Load Preview", Toast.LENGTH_SHORT).show();
                return true;
            }
        });
        vidView.setZOrderOnTop(true);
        return view;
    }


    @Override
    public void onPause() {
        super.onPause();
        vidView.pause();
    }
}
