package com.zeronebits.iptv.VideoPlayer;

import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.zeronebits.iptv.R;
import com.zeronebits.iptv.adpater.CategoryListAdapter;
import com.zeronebits.iptv.adpater.ChannelListFragmentMainAdapter;
import com.zeronebits.iptv.database.Database;
import com.zeronebits.iptv.entity.Category;
import com.zeronebits.iptv.entity.Channel;
import com.zeronebits.iptv.util.AllParser;
import com.zeronebits.iptv.util.common.DownloadUtil;
import com.zeronebits.iptv.util.common.LinkConfig;
import com.zeronebits.iptv.util.common.logger.Logger;

import org.json.JSONObject;
import org.lucasr.twowayview.TwoWayView;

import java.util.ArrayList;

/**
 * Created by sadip_000 on 23/08/2016.
 */

public class FragmentMain extends Fragment{
    private static final String TAG = FragmentMain.class.getName();
    public GridView gvChannelList;
    public Handler handlerToLoadPreview = new Handler();
    Database database;
    TVPlayCustomController_.ChannelLinkLoader channelLinkLoader = null;
    View view;
    int currentChannelPosition = 0;
    FrameLayout errorFrameLayout;
    private Channel currentChannel;
    private TextView channelName, channelDescription, amount, channelNo, currentCategoryTitleView;
    private TwoWayView categoryList;
    private ImageView btnFav, btnDvr,btnEpg;
    private TVPlayCustomController_ tvPlayCustomController;
    private ChannelListFragmentMainAdapter channelListAdapter;
    private TextView txtFavUnfav, txtBuy, txtDvr, txtEpg, txtPreview;
    private Runnable runnableToLoadPreview = new Runnable() {
        @Override
        public void run() {
            Logger.d("CheckingPreviewhandler", "started");
            stopHandleToPreview();

        }
    };
    private int currentChannelCategoryPosition = 0;

    @Override
    public void onResume() {
        super.onResume();
        gvChannelList.requestFocus();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_main, container, false);
        findViewByIds(view);
        tvPlayCustomController = (TVPlayCustomController_) getActivity();
        database = new Database(getActivity());
        if (getArguments().containsKey(TVPlayCustomController_.TAG_ERROR_CODE)) {
            Fragment fragment = new FragmentError();
            fragment.setArguments(getArguments());
            getChildFragmentManager().beginTransaction()
                    .replace(R.id.container_fragment_main, fragment,"error")
                    .commit();
        }

        tvPlayCustomController.backgroundTransparent.setVisibility(View.VISIBLE);
        tvPlayCustomController.progressBar.setVisibility(View.VISIBLE);
        tvPlayCustomController.backgroundTransparent.bringToFront();
        tvPlayCustomController.progressBar.bringToFront();

//        new CategoryAndChannelsLoader(tvPlayCustomController, this).execute();
onSuccess();
        return view;
    }

    private void findViewByIds(View view) {
        Typeface medium = Typeface.createFromAsset(getActivity().getAssets(), "font/Exo2-Medium.otf");
        Typeface bold = Typeface.createFromAsset(getActivity().getAssets(), "font/Exo2-Bold.otf");
        Typeface regular = Typeface.createFromAsset(getActivity().getAssets(), "font/Exo2-Regular_0.otf");
        Typeface semiBold = Typeface.createFromAsset(getActivity().getAssets(), "font/Exo2-SemiBold.otf");
        Typeface light = Typeface.createFromAsset(getActivity().getAssets(), "font/Exo2-Light.otf");


        ((TextView) view.findViewById(R.id.txt_fav_unfav)).setTypeface(regular);
        ((TextView) view.findViewById(R.id.txt_dvr)).setTypeface(regular);
        gvChannelList = (GridView) view.findViewById(R.id.gv_channels);
        channelName = (TextView) view.findViewById(R.id.txt_title);
        channelName.setTypeface(light);
        channelDescription = (TextView) view.findViewById(R.id.ChannelDescription);
        channelDescription.setTypeface(light);
        amount = (TextView) view.findViewById(R.id.amount);
        amount.setTypeface(semiBold);
        channelNo = (TextView) view.findViewById(R.id.channelNo);
        channelNo.setTypeface(bold);
        currentCategoryTitleView = (TextView) view.findViewById(R.id.currentcategory);
        categoryList = (TwoWayView) view.findViewById(R.id.categoryList);
        btnFav = (ImageView) view.findViewById(R.id.fav);
        txtFavUnfav = (TextView) view.findViewById(R.id.txt_fav_unfav);
        txtDvr = (TextView) view.findViewById(R.id.txt_dvr);
        btnDvr = (ImageView) view.findViewById(R.id.dvr);
        btnEpg = (ImageView) view.findViewById(R.id.epg);
        txtEpg=(TextView) view.findViewById(R.id.txt_epg);
        errorFrameLayout = (FrameLayout) view.findViewById(R.id.container_fragment_main);
        btnDvr.setNextFocusUpId(R.id.categoryList);
        btnEpg.setNextFocusUpId(R.id.categoryList);
        btnFav.setNextFocusUpId(R.id.categoryList);
        btnDvr.setNextFocusLeftId(R.id.gv_channels);
        categoryList.setNextFocusDownId(btnDvr.getId());
        gvChannelList.setNextFocusRightId(categoryList.getId());
        gvChannelList.setNextFocusDownId(btnDvr.getId());


        btnFav.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (btnFav.hasFocus()) {
                    btnFav.setScaleX(1.3f);
                    btnFav.setScaleY(1.3f);
                    txtFavUnfav.setScaleX(1.2f);
                    txtFavUnfav.setScaleY(1.2f);
                    txtFavUnfav.setTextColor(Color.parseColor("#4d9b75"));

                    /*gvChannelList.clearFocus();
                    try{channelListAdapter.setPosition(-1);
                    channelListAdapter.notifyDataSetChanged();}catch (Exception e){}*/
                } else {
                    txtFavUnfav.setTextColor(Color.parseColor("#ffffff"));

                    btnFav.setScaleX(1.0f);
                    btnFav.setScaleY(1.0f);
                    txtFavUnfav.setScaleX(1.0f);
                    txtFavUnfav.setScaleY(1.0f);
                }
            }
        });


        btnDvr.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (btnDvr.hasFocus()) {
                    btnDvr.setScaleX(1.3f);
                    btnDvr.setScaleY(1.3f);
                    txtDvr.setScaleX(1.2f);
                    txtDvr.setScaleY(1.2f);
                    txtDvr.setTextColor(Color.parseColor("#4d9b75"));
                } else {
                    txtDvr.setTextColor(Color.parseColor("#ffffff"));
                    btnDvr.setScaleX(1.0f);
                    btnDvr.setScaleY(1.0f);
                    txtDvr.setScaleX(1.0f);
                    txtDvr.setScaleY(1.0f);
                }
            }
        });
        btnEpg.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (btnEpg.hasFocus()) {
                    btnEpg.setScaleX(1.3f);
                    btnEpg.setScaleY(1.3f);
                    txtEpg.setScaleX(1.2f);
                    txtEpg.setScaleY(1.2f);
                    txtEpg.setTextColor(Color.parseColor("#4d9b75"));


                } else {
                    txtEpg.setTextColor(Color.parseColor("#ffffff"));
                    btnEpg.setScaleX(1.0f);
                    btnEpg.setScaleY(1.0f);
                    txtEpg.setScaleX(1.0f);
                    txtEpg.setScaleY(1.0f);
                }
            }
        });



    }

    @Override
    public void onPause() {
        super.onPause();
        closePreviewFragment();
    }

    public void closePreviewFragment() {
        Fragment fragment = getFragmentManager().findFragmentById(R.id.preview);
        if (fragment != null) {
            getFragmentManager().beginTransaction().remove(fragment).commit();
            getFragmentManager().getFragments().remove(fragment);
        }
    }

    protected void stopHandleToPreview() {
        Logger.d("CheckingPreviewhandler", "stophandler");

        handlerToLoadPreview.removeCallbacks(runnableToLoadPreview);
    }

    private void startHandleToPreview() {
        Logger.d("CheckingPreviewhandler", "startedHandler");

        handlerToLoadPreview.postDelayed(runnableToLoadPreview, 1000 * 5);
    }




    public void onSuccess() {
        tvPlayCustomController.progressBar.setVisibility(View.GONE);
        tvPlayCustomController.backgroundTransparent.setVisibility(View.GONE);
        final CategoryListAdapter categoryListAdapter = new CategoryListAdapter(getActivity(), AllParser.allcategories);
        Logger.d("CategoryName", AllParser.categorynames.toString());
        categoryList.setAdapter(categoryListAdapter);
        try {
            currentChannelCategoryPosition = AllParser.getCategoryPosition(Integer.parseInt(database.getChannelCategory()));
        } catch (Exception e) {
            currentChannelCategoryPosition = 0;
        }
        categoryList.setSelection(currentChannelCategoryPosition);
//        categoryList.setVisibility(View.INVISIBLE);

        categoryList.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                categoryList.setSelection(position);
                categoryListAdapter.getPosition(position);
                categoryListAdapter.notifyDataSetChanged();
                categoryList.smoothScrollToPosition(position,categoryList.getWidth()/2);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {}
        });

        loadChannelListOnCategoryClicked(AllParser.categorynames.get(currentChannelCategoryPosition), AllParser.allcategories.get(currentChannelCategoryPosition).getCategoryId());
        currentCategoryTitleView.setText(AllParser.categorynames.get(currentChannelCategoryPosition));
        final int channelId = getArguments().getInt(TVPlayCustomController_.TAG_CURRENT_CHANNEL_ID, 0);
        Channel currentPlayingChannel =AllParser.getChannelFromId(channelId);
        if(currentPlayingChannel!=null){
            setValues(currentPlayingChannel);
            gvChannelList.setSelection(AllParser.getPositionOfChannelInCategory(currentPlayingChannel, AllParser.categorynames.get(currentChannelCategoryPosition)));
        }


        categoryList.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Category category = AllParser.allcategories.get(position);
                String categoryClicked =category.getCategoryName();
                currentCategoryTitleView.setText(categoryClicked);
                loadChannelListOnCategoryClicked(categoryClicked, category.getCategoryId());
            }
        });


        Bundle bundle = getArguments();

        categoryList.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    categoryList.setSelection(0);
                }else{
                    categoryList.clearFocus();
                }
            }
        });

        gvChannelList.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    categoryList.setSelection(-1);
                    categoryList.clearFocus();
                    gvChannelList.setSelection(gvChannelList.getSelectedItemPosition());
                    ChannelListFragmentMainAdapter adapter = (ChannelListFragmentMainAdapter)gvChannelList.getAdapter();
                    adapter.setPosition(gvChannelList.getSelectedItemPosition());
                    adapter.notifyDataSetChanged();
                }else{
                    /*gvChannelList.clearFocus();
                    gvChannelList.setSelection(-1);*/
//                    try{channelListAdapter.setPosition(-1);
//                        channelListAdapter.notifyDataSetChanged();}catch (Exception e){}
                }
            }
        });
    }



    private void loadChannelListOnCategoryClicked(final String category,final int categoryId) {
        final ArrayList<Channel> channelList = getChannelList(category);
        //hide channel list and show no channels if arraylist is empty
        TVPlayCustomController_.currentCategoryPosition = AllParser.getCategoryPosition(categoryId);
        if (channelList == null || channelList.size() < 1) {
            Logger.e("currentChannel is null", "HIDE LIST");
            gvChannelList.setVisibility(View.GONE);
        } else {
            setValues(channelList.get(0));
            gvChannelList.setVisibility(View.VISIBLE);
            channelListAdapter = new ChannelListFragmentMainAdapter(tvPlayCustomController, R.layout.new_channel_list_row, channelList);
            gvChannelList.setAdapter(channelListAdapter);
            gvChannelList.requestFocus();

            gvChannelList.setOnItemSelectedListener(new OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    closePreviewFragment();
                    currentChannelPosition = position;
                    channelListAdapter.setPosition(position);
                    channelListAdapter.notifyDataSetChanged();
                    setValues(channelList.get(position));
                    currentChannel = channelList.get(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });

            gvChannelList.setOnItemClickListener(new OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (channelLinkLoader != null) {
                        channelLinkLoader.cancel(true);
                        channelLinkLoader = null;
                    }
                    errorFrameLayout.setVisibility(View.INVISIBLE);
                    Channel channelToPlay = channelList.get(position);
                    if (!(channelToPlay.getChannelPurchaseStatus() == Channel.PURCHASE_BUY)) {
                        database.deleteChannel();
                        database.deleteChannelCategory();
                        database.insertChannel(channelToPlay.getId() + "");
                        database.insertChannelCategory(categoryId + "");
                        if(tvPlayCustomController.controller!=null){
                            channelLinkLoader = tvPlayCustomController.new ChannelLinkLoader(channelList.get(position).getId());
                            channelLinkLoader.execute();
                        }else if (channelToPlay.getId() != getArguments().getInt(TVPlayCustomController_.TAG_CURRENT_CHANNEL_ID) || !tvPlayCustomController.player.isPlaying()) {
                            channelLinkLoader = tvPlayCustomController.new ChannelLinkLoader(channelList.get(position).getId());
                            channelLinkLoader.execute();
                        } else {
                            tvPlayCustomController.getSupportFragmentManager().beginTransaction().remove(FragmentMain.this).commit();
                            Toast.makeText(tvPlayCustomController, channelList.get(position).getName() + " is currently playing", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        onError(R.string.war_code_buy_before_watch);
                    }
                }
            });
        }

    }

    private void setValues(final Channel channel) {
        if (channel != null) {
            channelName.setText(channel.getName());
            try {
                channelDescription.setText((channel.getChannelDescription()).substring(0, 200) + ".....");
            } catch (Exception e) {
                channelDescription.setText((channel.getChannelDescription()));

            }
            channelNo.setText(channel.getChannelPriority() + "");
            amount.setText(channel.getchannelPrice() + "");

            if(channel.getFavourite() == 1){
                txtFavUnfav.setText("Unset Fav");
            }else{
                txtFavUnfav.setText("set Fav");
            }

            btnFav.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    String favUnfavUrl = LinkConfig.getString(getActivity(), LinkConfig.FAV_UNFAV_URL) + "?";
                    new SetFavouriteTask(channel).execute(favUnfavUrl);
                }
            });

            btnDvr.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//
                Toast.makeText(tvPlayCustomController, "DVR is not available at this time. Please check later", Toast.LENGTH_SHORT).show();

//                Fragment nextFrag = new FragmentDVR();
//                Bundle bundle = new Bundle();
//                bundle.putInt(TVPlayCustomController_.TAG_CURRENT_CHANNEL_ID, channel.getId());
//                nextFrag.setArguments(bundle);
//
//                getActivity().getSupportFragmentManager().beginTransaction()
//                        .replace(R.id.container_movie_player, nextFrag, "parent")
//                        .commit();
            }
        });
        btnEpg.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(tvPlayCustomController, "EPG is not available at this time. Please check later", Toast.LENGTH_SHORT).show();
                
                /*binisha*/
//                Fragment nextFrag = new FragmentEpg();
//                Bundle bundle = new Bundle();
//                bundle.putInt(TVPlayCustomController_.TAG_CURRENT_CHANNEL_ID, channel.getId());
//                nextFrag.setArguments(bundle);
//
//                getActivity().getSupportFragmentManager().beginTransaction()
//                        .replace(R.id.container_movie_player, nextFrag, "parent")
//                        .commit();

            }
        });

        } else {
            channelName.setText("N/A");
            channelDescription.setText("N/A");
            channelNo.setText("N/A");
            amount.setText("N/A");
        }
    }

    private ArrayList<Channel> getChannelList(String currentCategoryName) {
        ArrayList<Channel> channelList;
        if (currentCategoryName.equalsIgnoreCase("Favourite")) {
            channelList = AllParser.favoriteCategoryChannels();
        } else if (currentCategoryName.equalsIgnoreCase("All")) {
            channelList =AllParser.getAllChannels();
        }else {
            channelList = AllParser.category_channel.get(currentCategoryName);
        }
        return channelList;
    }

    public void onError(int errorCode) {
        tvPlayCustomController.progressBar.setVisibility(View.GONE);
        tvPlayCustomController.backgroundTransparent.setVisibility(View.GONE);
        errorFrameLayout.setVisibility(View.VISIBLE);

        Bundle bundle = new Bundle();
        bundle.putInt(TVPlayCustomController_.TAG_ERROR_CODE, errorCode);
        Fragment fragment = new FragmentError();
        fragment.setArguments(bundle);
        getFragmentManager().beginTransaction()
                .replace(R.id.container_fragment_main, fragment)
                .addToBackStack(null)
                .commit();

    }

    public void closeErrorFragment(){
            Fragment fragment = getFragmentManager().findFragmentById(R.id.container_fragment_main);
            if (fragment != null) {
                getFragmentManager().beginTransaction().remove(fragment).commit();
                getFragmentManager().getFragments().remove(fragment);
        }

    }


    private class SetFavouriteTask extends AsyncTask<String, Void, String> {
        Channel channel;

        public SetFavouriteTask(Channel channel) {
            this.channel = channel;
        }

        @Override
        protected String doInBackground(String... params) {

            DownloadUtil getUtc = new DownloadUtil(LinkConfig.GET_UTC, tvPlayCustomController);
            String utc = getUtc.downloadStringContent();
            if (!utc.equals(DownloadUtil.NotOnline)
                    && !utc.equals(DownloadUtil.ServerUnrechable)) {
                String link = params[0]+ "channelID=" + channel.getId() + "&" + LinkConfig.getHashCode(utc) ;

                DownloadUtil dUtil = new DownloadUtil(link, tvPlayCustomController);
                Logger.d(TAG,"Setfav link " + link + channel.getId());
                return dUtil.downloadStringContent();
            } else
                return utc;
        }

        @Override
        protected void onPostExecute(String result) {

            // offline
            if (result.equalsIgnoreCase(DownloadUtil.NotOnline) || result.equalsIgnoreCase(DownloadUtil.ServerUnrechable)) {
                onError(R.string.err_code_server_unreachable);
            } else {
                try {
                    JSONObject root = new JSONObject(result);
                    boolean rsp_status = root.getBoolean("status");

                    if (rsp_status) {
                        channel.setFavourite(1);
                        Toast.makeText(tvPlayCustomController,
                                       "Channel is set as favorite currentChannel",
                                       Toast.LENGTH_LONG).show();
                        txtFavUnfav.setText("Unset Fav");

                    } else {
                        channel.setFavourite(0);
                        txtFavUnfav.setText("Set Fav");
                        Toast.makeText(tvPlayCustomController,
                                       "Channel is unset as favorite currentChannel",
                                       Toast.LENGTH_LONG).show();

                    }
                } catch (Exception e) {
                    Toast.makeText(tvPlayCustomController,
                                   "Could not set/unset currentChannel as favorite currentChannel",
                                   Toast.LENGTH_LONG).show();
                }

                if (currentCategoryTitleView.getText().toString().equals("Favourite")) {
                    ArrayList<Channel> channelList = AllParser.favoriteCategoryChannels();
                    if (channelList.size() > 0) {
                        loadChannelListOnCategoryClicked(currentCategoryTitleView.getText().toString(), currentChannelCategoryPosition);
                    } else {
                        Logger.e("currentChannel is null", "GOT IT");
//                        categoryList.setVisibility(View.INVISIBLE);
                    }

                }
            }
        }
    }



}

