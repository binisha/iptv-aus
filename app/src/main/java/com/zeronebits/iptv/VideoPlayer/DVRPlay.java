package com.zeronebits.iptv.VideoPlayer;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.zeronebits.iptv.R;
import com.zeronebits.iptv.entity.Channel;
import com.zeronebits.iptv.util.EPGUtils.EPGMini;
import com.zeronebits.iptv.util.common.DateUtils;
import com.zeronebits.iptv.util.common.DownloadUtil;
import com.zeronebits.iptv.util.common.LinkConfig;
import com.zeronebits.iptv.util.common.logger.Logger;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import static com.zeronebits.iptv.util.common.DateUtils._24HrsTimeFormat;


/**
 * Created by sadip_000 on 30/09/2016.
 */

public abstract class DVRPlay extends AsyncTask<Void, Void, String> {
    private Context context;
    private Channel channel;
    private MediaPlayer player;
    private EPGMini epgMini;

    public DVRPlay(Context tvPlayCustomController_, Channel channel, EPGMini epgMini, MediaPlayer player) {
        this.context = tvPlayCustomController_;
        this.channel = channel;
        this.epgMini = epgMini;
        this.player = player;
    }

    @Override
    protected String doInBackground(Void... params) {
        DownloadUtil getUtc = new DownloadUtil(LinkConfig.GET_UTC, context);
        String utc = getUtc.downloadStringContent();
        String link = LinkConfig.getString(context,
                                           LinkConfig.DVR_VIDEO_URL)
                + "?" + "channelId=" + channel.getId()
                + "&" + "date=" + DateUtils.dateFormat.format(epgMini.getStartCalendar().getTime())
                + "&" + "startTime=" + _24HrsTimeFormat.format(epgMini.getStartCalendar().getTime());

        if (!utc.equals(DownloadUtil.NotOnline)
                && !utc.equals(DownloadUtil.ServerUnrechable)) {
            link += "&" + LinkConfig.getHashCode(utc);
            DownloadUtil dUtil = new DownloadUtil(link, context);
            Logger.d(".DvrLinkLoader", link);

            return dUtil.downloadStringContent();
        } else
            return utc;

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        myPreExecute();
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (result.equalsIgnoreCase(DownloadUtil.NotOnline) || result.equalsIgnoreCase(DownloadUtil.ServerUnrechable)) {
            onError(true, R.string.err_code_server_unreachable);
        } else {
            try {
                JSONObject jobj = new JSONObject(result);
                String link = jobj.getString("link");
                String videoStartTime = jobj.getString("startTime");
                String nextVideoName = jobj.getString("nextVideoName");
                playDvrUrl(epgMini, link, videoStartTime, nextVideoName);


            } catch (JSONException je) {
                try {
                    JSONObject Jobj = new JSONObject(result);
                    String errorCode = Jobj.getString("error_code");
                    String errorMessage = Jobj.getString("error_message");
                    if (errorCode.equals("307")) {
                        onError(true, R.string.war_code_buy_before_watch);
                    } else if (errorCode.equals("308")) {
                        onError(true, R.string.war_code_upgrade_before_watch);

                    } else {

                        onError(true, errorMessage);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    onError(true, R.string.err_code_json_exception);
                }

            }
        }
    }

    public abstract void onError(boolean openDVR, Object err);

    private void playDvrUrl(final EPGMini epgMini, String dvrURL, String videoStartTime, final String nextVideoName) {
        try {
            player.reset();
        } catch (Exception e) {
            e.printStackTrace();
        }

            try {
                Logger.d("link from abhinav2 : ", dvrURL);
                player.setAudioStreamType(AudioManager.STREAM_MUSIC);
//                player.setDataSource(context, Uri.parse("http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4"));
                player.setDataSource(context, Uri.parse(dvrURL));
                player.prepareAsync();
                final long seekPos = getSeekPosition(videoStartTime, DateUtils._24HrsTimeFormat.format(epgMini.getStartCalendar().getTime()));

                player.setOnPreparedListener(new OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        Fragment fragment = ((AppCompatActivity) context).getSupportFragmentManager().findFragmentByTag("parent");
                        if (fragment != null)
                            ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction().remove(fragment).commit();

                        myOnPrepared(channel.getId(), nextVideoName);
                        player.seekTo((int) seekPos);

                    }

                });
                player.setOnErrorListener(new OnErrorListener() {
                    @Override
                    public boolean onError(MediaPlayer mp, int what, int extra) {

                        //code for dvr error
                        try {player.reset();} catch (Exception e) {}
                        StringBuilder sb = new StringBuilder().append("MEDIA_ERROR:\t").append("W").append(what).append("E").append(extra);
//                        Toast.makeText(context, "Couldnt play the requested video.", Toast.LENGTH_SHORT).show();

                        DVRPlay.this.onError(true, sb.toString());
                        return false;
                    }
                });
                player.setOnCompletionListener(new OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        myOnComplete(mp);
//                    if (nextVideoName.equalsIgnoreCase("null") || nextVideoName.equalsIgnoreCase("N/A")) {}
//                        Toast.makeText(context, "PlayBack Complete", Toast.LENGTH_SHORT).show();
//                        Toast.makeText(context, "Couldnt play the requested video.", Toast.LENGTH_SHORT).show();
                    }
                });


            } catch (IllegalArgumentException | SecurityException | IllegalStateException | IOException e) {
                onError(false, e.getMessage());
                e.printStackTrace();
            }



    }

    protected abstract void myOnComplete(MediaPlayer mp);

    private long getSeekPosition(String videoStartTime, String epg_start_time) {
        long seekPos = 0;
        try {
            Date videoTime = _24HrsTimeFormat.parse(videoStartTime);
            Calendar videoCal = Calendar.getInstance();
            videoCal.setTime(videoTime);
            Date epgTime = _24HrsTimeFormat.parse(epg_start_time);
            Calendar epgCal = Calendar.getInstance();
            epgCal.setTime(epgTime);

            seekPos = epgCal.getTimeInMillis() - videoCal.getTimeInMillis();

            Logger.d("seekDuration: epgCal.getTimeInMillis", epgCal.getTimeInMillis() + "");
            Logger.d("seekDuration: videoCal.getTimeInMillis", videoCal.getTimeInMillis() + "");
            Logger.d("seekDuration:", seekPos + "");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return seekPos;
    }

    protected abstract void myOnPrepared(int channelId, String nextVideoName);

    protected abstract void myPreExecute();
}
