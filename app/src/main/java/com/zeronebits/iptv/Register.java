package com.zeronebits.iptv;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.zeronebits.iptv.util.common.AlertDialogManager;
import com.zeronebits.iptv.util.common.AppConfig;
import com.zeronebits.iptv.util.common.DownloadUtil;
import com.zeronebits.iptv.util.common.GetMac;
import com.zeronebits.iptv.util.common.LinkConfig;
import com.zeronebits.iptv.util.common.logger.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Register extends Activity {
    private static final String TAG = "com.totalcale.Register";

    EditText fName, lName, email, dName, groupValue, password, cpassword, packageValue, countryValue, phone;
    TextView mac, mismatch;
    ArrayList<HashMap<String, String>> groupList, packageList, countryList;
    JSONArray groups = null;
    String groupUrl, packageUrl, countryUrl;
    Spinner mSpinner, mSpinnerCountry;
    String[] items;
    String[] itemsPack, itemsCont;
    Button register;
    String macAddress, firstName, lastName, sEmail, sdName, spassword,
            scpassword, sPhone;
    AlertDialogManager alertDialogManager;
    String success;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }


        groupUrl = LinkConfig.getString(Register.this, LinkConfig.GROUP_LIST);
        packageUrl = LinkConfig.getString(Register.this, LinkConfig.PACKAGE_DATA);
        countryUrl = LinkConfig.getString(Register.this, LinkConfig.COUNTRY_DATA);

//		groupUrl = getString(R.string.base_url) + "groups_json.php";

        email = (EditText) findViewById(R.id.editText3Email);
        dName = (EditText) findViewById(R.id.dName);
        phone = (EditText) findViewById(R.id.etPhoneNumber);
        password = (EditText) findViewById(R.id.password);
        cpassword = (EditText) findViewById(R.id.cpassword);
        mSpinner = (Spinner) findViewById(R.id.spinner1);
//        mSpinner2 =(Spinner) findViewById(R.id.spinner2);
        mSpinnerCountry = (Spinner) findViewById(R.id.spinnerCountry);

        mac = (TextView) findViewById(R.id.textView6macAddress);
        mismatch = (TextView) findViewById(R.id.mismatch);
        mismatch.setVisibility(View.INVISIBLE);
        alertDialogManager = new AlertDialogManager();
        register = (Button) findViewById(R.id.button1register);

        if (AppConfig.isDevelopment()) {
            macAddress = AppConfig.getMacAddress();
        } else {
            macAddress = GetMac.getMac(this);
        }
        mac.setText(macAddress);

       /* getGroupData(groupUrl);
        getPackageData(packageUrl);
        getCountryData(countryUrl);*/


        new CountryData().execute();


//        mSpinner2.setAdapter(adapter2);


        // Checking the corect password typed or not
        cpassword.setOnKeyListener(new OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                // TODO Auto-generated method stub
                spassword = password.getText().toString();
                scpassword = cpassword.getText().toString();
                if (scpassword.length() > 0) {
                    if (!spassword.equals(scpassword)) {
                        mismatch.setVisibility(View.VISIBLE);
                    } else {
                        mismatch.setVisibility(View.INVISIBLE);
                    }
                } else {
                    mismatch.setVisibility(View.INVISIBLE);
                }
                return false;
            }
        });

        register.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                sEmail = email.getText().toString();
                sdName = dName.getText().toString();
                spassword = password.getText().toString();
                sPhone = phone.getText().toString();
                scpassword = cpassword.getText().toString();

                int text1 = mSpinner.getSelectedItemPosition();
                HashMap<String, String> map = groupList.get(text1);
                String grouptxt = map.get("group_id");
//                int text3 = mSpinner2.getSelectedItemPosition();
//                HashMap<String, String> map2 = packageList.get(text3);
//                String packagetxt = map2.get("package_id");
                int text4 = mSpinnerCountry.getSelectedItemPosition();
                HashMap<String, String> map3 = countryList.get(text4);
                String countrytxt = map3.get("country_id");

                if (checkEmptyField()) {
                    if (!mismatch.isShown()) {
                        if (isEmailValid(sEmail)) {

                            try {
                                String queryString = "email=" + sEmail
                                        + "&username=" + sEmail + "&password="
                                        + spassword + "&display_name="
                                        + URLEncoder.encode(sdName, "UTF-8")
                                        + "&user_group=" + grouptxt + "&phone=" + sPhone + "&country=" + countrytxt + "&macAddress="
                                        + macAddress;
                                Logger.d("queryString register", queryString);


                                String regLink = LinkConfig.getString(Register.this, LinkConfig.REGISTER_BUTTON_CLICK) + "?" + queryString;
                                Logger.e("register button onclick", regLink);
///*								String regLink = getString(R.string.base_url)
//                                        + "register_json.php?" + queryString;*/

                                new RegisterTask().execute(regLink);
                            } catch (Exception e) {
                                alertDialogManager.showAlertDialog(Register.this,
                                        "Invalid value for one or more data",
                                        "Please input values correctly",
                                        false);
                            }

                        } else {
                            alertDialogManager.showAlertDialog(Register.this,
                                    "Invalid Email..!",
                                    "Please input valid email address...",
                                    false);
                        }
                    } else {
                        alertDialogManager.showAlertDialog(Register.this,
                                "Password Mismatch..!",
                                "Please confirm the password..", false);
                    }
                } else {
                    alertDialogManager.showAlertDialog(Register.this,
                            "Register Failed..!",
                            "Please Enter the required fields..", false);
                }
            }
        });
    }
    /*public void getCountryData(String url) {
        countryList = new ArrayList<HashMap<String, String>>();

        DownloadUtil dUtil = new DownloadUtil(url, Register.this);

        String jsonString = dUtil.downloadStringContent();

        try {

            JSONObject root = new JSONObject(jsonString);

            groups = root.getJSONArray("country");

            itemsCont = new String[groups.length()];
            for (int i = 0; i < groups.length(); i++) {
                JSONObject c1 = groups.getJSONObject(i);

                String id = c1.getString("country_id");
                String name = c1.getString("country_name");

                HashMap<String, String> map3 = new HashMap<String, String>();
                itemsCont[i] = c1.getString("country_name");

                map3.put("country_id", id);
                map3.put("country_name", name);

                countryList.add(map3);
            }
        } catch (JSONException e) {
            Logger.printStackTrace(e);
        }
    }*/

    public class CountryData extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            DownloadUtil dUtil = new DownloadUtil(countryUrl, Register.this);

            String jsonString = dUtil.downloadStringContent();
            return jsonString;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {

                JSONObject root = new JSONObject(s);

                groups = root.getJSONArray("country");
                countryList = new ArrayList<HashMap<String, String>>();

                itemsCont = new String[groups.length()];
                for (int i = 0; i < groups.length(); i++) {
                    JSONObject c1 = groups.getJSONObject(i);

                    String id = c1.getString("country_id");
                    String name = c1.getString("country_name");

                    HashMap<String, String> map3 = new HashMap<String, String>();
                    itemsCont[i] = c1.getString("country_name");

                    map3.put("country_id", id);
                    map3.put("country_name", name);

                    countryList.add(map3);
                }
                ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(Register.this, android.R.layout.simple_spinner_item, itemsCont);
                adapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                mSpinnerCountry.setAdapter(adapter3);
                new GroupData().execute();
            } catch (JSONException e) {
                Logger.printStackTrace(e);
            }
        }
    }

   /* public void getPackageData(String url) {
        packageList = new ArrayList<HashMap<String, String>>();

        DownloadUtil dUtil = new DownloadUtil(url, Register.this);

        String jsonString = dUtil.downloadStringContent();

        try {

            JSONObject root = new JSONObject(jsonString);

            groups = root.getJSONArray("group");

            itemsPack = new String[groups.length()];
            for (int i = 0; i < groups.length(); i++) {
                JSONObject c1 = groups.getJSONObject(i);

                String id = c1.getString("package_id");
                String name = c1.getString("package_name");

                HashMap<String, String> map2 = new HashMap<String, String>();
                itemsPack[i] = c1.getString("package_name");

                map2.put("package_id", id);
                map2.put("package_name", name);

                packageList.add(map2);
            }
        } catch (JSONException e) {
            Logger.printStackTrace(e);
        }
    }*/

    public class PackageData extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            DownloadUtil dUtil = new DownloadUtil(packageUrl, Register.this);

            String jsonString = dUtil.downloadStringContent();
            return jsonString;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                packageList = new ArrayList<HashMap<String, String>>();

                JSONObject root = new JSONObject(s);

                groups = root.getJSONArray("group");

                itemsPack = new String[groups.length()];
                for (int i = 0; i < groups.length(); i++) {
                    JSONObject c1 = groups.getJSONObject(i);

                    String id = c1.getString("package_id");
                    String name = c1.getString("package_name");

                    HashMap<String, String> map2 = new HashMap<String, String>();
                    itemsPack[i] = c1.getString("package_name");

                    map2.put("package_id", id);
                    map2.put("package_name", name);

                    packageList.add(map2);
                }
                ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(Register.this, android.R.layout.simple_spinner_item, itemsPack);
                adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            } catch (JSONException e) {
                Logger.printStackTrace(e);
            }
        }
    }


    public class GroupData extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {
            DownloadUtil dUtil = new DownloadUtil(groupUrl, Register.this);

            String jsonString = dUtil.downloadStringContent();
            return jsonString;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                groupList = new ArrayList<HashMap<String, String>>();

                JSONObject root = new JSONObject(s);

                groups = root.getJSONArray("group");

                items = new String[groups.length()];
                for (int i = 0; i < groups.length(); i++) {
                    JSONObject c1 = groups.getJSONObject(i);

                    String id = c1.getString("group_id");
                    String name = c1.getString("group_name");

                    HashMap<String, String> map = new HashMap<String, String>();
                    items[i] = c1.getString("group_name");

                    map.put("group_id", id);
                    map.put("group_name", name);

                    groupList.add(map);
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<String>(Register.this, android.R.layout.simple_spinner_item, items);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                mSpinner.setAdapter(adapter);
                new PackageData().execute();
            } catch (JSONException e) {
                Logger.printStackTrace(e);
            }
        }
    }

    /*
    public void getGroupData(String url) {


        groupList = new ArrayList<HashMap<String, String>>();

        DownloadUtil dUtil = new DownloadUtil(url, Register.this);

        String jsonString = dUtil.downloadStringContent();

        try {

            JSONObject root = new JSONObject(jsonString);

            groups = root.getJSONArray("group");

            items = new String[groups.length()];
            for (int i = 0; i < groups.length(); i++) {
                JSONObject c1 = groups.getJSONObject(i);

                String id = c1.getString("group_id");
                String name = c1.getString("group_name");

                HashMap<String, String> map = new HashMap<String, String>();
                items[i] = c1.getString("group_name");

                map.put("group_id", id);
                map.put("group_name", name);

                groupList.add(map);
            }
        } catch (JSONException e) {
            Logger.printStackTrace(e);
        }
    }*/

    public boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public boolean checkEmptyField() {

        return sEmail.length() > 0 && sdName.length() > 0
                && spassword.length() > 0 && scpassword.length() > 0;
    }

    private class RegisterTask extends AsyncTask<String, Void, String> {


        @Override
        protected String doInBackground(String... params) {

            DownloadUtil dUtil = new DownloadUtil(params[0], Register.this);

            return dUtil.downloadStringContent();
        }

        @Override
        protected void onPostExecute(String result) {

            try {
                Logger.d(TAG, result);

                JSONObject root = new JSONObject(result);
                success = root.getString("success");

                if (success.equals("1")) {
                    Intent i = new Intent(Register.this, RegistrationSuccessful.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                    startActivity(i);
                    finish();
                } else {
                    alertDialogManager
                            .showAlertDialog(
                                    Register.this,
                                    "Failure..!",
                                    "Sorry we could not register you to the system. \nEither you are not an authorized customer or details you have provided are wrong ..",
                                    false);

                }
            } catch (JSONException e) {
                alertDialogManager
                        .showAlertDialog(
                                Register.this,
                                "Failure..!",
                                "Sorry we could not register you to the system. \nEither you are not an authorized customer or details you have provided are wrong .",
                                false);

            } catch (Exception e) {
                alertDialogManager
                        .showAlertDialog(
                                Register.this,
                                "Failure..!",
                                "Sorry we could not register you to the system. \nEither you are not an authorized customer or details you have provided are wrong ...",
                                false);

            }
        }

    }

}
