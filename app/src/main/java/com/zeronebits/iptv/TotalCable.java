package com.zeronebits.iptv;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build.VERSION_CODES;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.zeronebits.iptv.util.ChannelUtils.EpgInFile;
import com.zeronebits.iptv.util.common.AppConfig;
import com.zeronebits.iptv.util.common.LoginFileUtils;
import com.zeronebits.iptv.util.common.logger.Logger;

import java.io.File;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Stack;


/**
 * Point to change before using the sample:
 * open build:gradle(Module:app)
 * paste "  useLibrary 'org.apache.http.legacy' " inside android tag
 * paste "    compile 'com.koushikdutta.ion:ion:2.+'  " inside dependency tag
 */

public class TotalCable extends Application {
    private static final String TAG = TotalCable.class.getName();
    private static final int DELAY_TO_START_SCREENSAVER = 5 * 60 * 1000;
    private static final int DELAY_BETWEEN_CHANGE_IMAGE_IN_SCREENSAVER = 1 * 60 * 1000;
    private static final int USER_INACTIVITY_CHECK_FOR_SCREENSAVER = 5 * 60 * 1000;
    public static ArrayList<String> screenSaverImageList;

    public long getLastUsed() {
        return lastUsed;
    }

    private long lastUsed;

    private UserInteractionThread waiter;
    private boolean screenSaverStarted = false; // flag that knows if screensaver is on top or not
    private boolean videoPlaying = false;// flag that knows if video is being played in application so that screensaver should not be started


    public boolean getVideoPlaying() {
        return videoPlaying;
    }


    public void setVideoPlaying(boolean isVideoPlaying) {
        this.videoPlaying = isVideoPlaying;
    }

    public boolean getScreenSaverStarted() {
        return screenSaverStarted;
    }

    public void setScreenSaverStarted(boolean isScreenSaverOn) {
        screenSaverStarted = isScreenSaverOn;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        deleteOlderFiles();
        Logger.d(TAG, "Starting Application" + this.toString());
        waiter = new UserInteractionThread(DELAY_TO_START_SCREENSAVER);// starts checking after 5 minutes
        screenSaverImageList = new ArrayList<String>();
        screenSaverImageList.add("");
        waiter.start();


    }

    public void deleteOlderFiles() {
        String dirPath = getCacheDir().getPath() + File.separator + EpgInFile.EPG_DIRECTORY_NAME;
        File dir = new File(dirPath);

//        File dir = new File(getExternalFilesDir(null).getPath());
        Stack<File> dirlist = new Stack<File>();
        //empty stack
        dirlist.clear();
        //add dir on stack
        dirlist.push(dir);
        while (!dirlist.isEmpty()) {
            File dirCurrent = dirlist.pop();
            File[] fileList = dirCurrent.listFiles();
            if (fileList != null) {
                for (File aFileList : fileList) {
                    if (!aFileList.getName().equalsIgnoreCase(
                            LoginFileUtils.APP_CONFIG_FILE_NAME)) {
                        if (aFileList.isDirectory())
                            dirlist.push(aFileList);
                        else {
                            Calendar c = Calendar.getInstance();
                            long date = c.get(Calendar.DATE);
                            Date today = new Date(date);

                            int diffInDays = (int) ((today.getTime() - aFileList.lastModified()) / (1000 * 60 * 60 * 24));
                            Logger.e("SADIP"+aFileList.getName(),(int) (aFileList.lastModified() / (1000 * 60 * 60 * 24))+ "");
                            if (diffInDays > 1) {
                                aFileList.delete();
                            }
                        }
                    }
                }
            } else
                Logger.d(TAG, "dirlist is empty");
        }
    }

    /**
     * this method must be called from every activity in onUserInteraction() to find user inactivity
     */
    public void active() {
        waiter.active();
        setScreenSaverStarted(false);
    }

    /**
     * @return the flag that recognizes if app is on top of any other application
     */
    @TargetApi(VERSION_CODES.LOLLIPOP_MR1)
    private boolean currentAppInForeground() {
        String currentApp = "NULL";
//        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.LOLLIPOP) {
        ActivityManager am = (ActivityManager) this.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> tasks = am.getRunningAppProcesses();
        currentApp = tasks.get(0).processName;
        /*} else {
            UsageStatsManager usm = (UsageStatsManager) this.getSystemService(Context.USAGE_STATS_SERVICE);
            long time = System.currentTimeMillis();
            List<UsageStats> appList = usm.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, time - 1000 * 1000, time);
            if (appList != null && appList.size() > 0) {
                SortedMap<Long, UsageStats> mySortedMap = new TreeMap<Long, UsageStats>();
                for (UsageStats usageStats : appList) {
                    mySortedMap.put(usageStats.getLastTimeUsed(), usageStats);
                }
                if (mySortedMap != null && !mySortedMap.isEmpty()) {
                    currentApp = mySortedMap.get(mySortedMap.lastKey()).getPackageName();
                }
                for (UsageStats app : appList) {
                    if (!currentApp.equalsIgnoreCase( app.getPackageName())){
//                        ActivityManager aM =(ActivityManager)
//                                getApplicationContext().getSystemService(Catalogue.content_holder.getApplicationContext().ACTIVITY_SERVICE);
//                        aM.killBackgroundProcesses("com.android.email");
                    }
                }

            }
        }*/


        return currentApp.equalsIgnoreCase(getApplicationContext().getPackageName());


    }

    public boolean checkScreenStatus() {
        boolean flag = false;
        try {
            Cursor cursor = getContentResolver().query(AppConfig.CONTENT_PROVIDER, null, null, null, null);
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    String flg = cursor.getString(0);
                    Logger.d("CheckingvlauefoS", flg);
                    flag = flg.equals("true");
                }
                cursor.close();
            }
        } catch (Exception e) {
            Logger.printStackTrace(e);
        }

        return flag;
    }

    /**
     * it is the main thread that finds user interation whole over the app
     * it keeps running even if the app is closed pressing back, couldnt figure that why?
     * but can be stopped if within the running perion the flag stop is set to true
     * shows screen saver only if the app is in foregorund
     */
    private class UserInteractionThread extends Thread {

        private long period;
        private boolean stop;


        public UserInteractionThread(long period) {
            this.period = period;
            stop = false;
        }

        public void run() {
            long idle = 0;
            this.active();
            Looper.prepare();
            while (!stop) {
                try {
                    Thread.sleep(USER_INACTIVITY_CHECK_FOR_SCREENSAVER); //checks to show screensaver after every 1 minute
                } catch (InterruptedException e) {
                    Logger.d(TAG, "Waiter interrupted!");
                }
                idle = System.currentTimeMillis() - lastUsed;
                Logger.d(TAG, "Application is idle for " + idle + " ms");
                if (idle > period) {
                    idle = 0;

                    /**
                     * show screenSaver only
                     * if the app is running on foreground
                     * if screensaver is not already shown
                     * if video is not being played
                     */

                    if (currentAppInForeground() && !getScreenSaverStarted() && !getVideoPlaying()) {
                        startScreenSaver();
//                        onTrimMemory(TRIM_MEMORY_BACKGROUND);
                    }

                }

            }
            /**
             * if needed to stop thread running set the flag stop here to true
             */

        }

        private synchronized void active() {
            lastUsed = System.currentTimeMillis();
        }

        /**
         * it is to show the screen saver which is shown as activity
         */
        private void startScreenSaver() {
            if (checkScreenStatus()) {
                Log.d("CheckingScreenStaus", "Displaying....");
                Intent i = new Intent(getApplicationContext(), ScreenSaverActivity.class);
                i.putExtra("delay", DELAY_BETWEEN_CHANGE_IMAGE_IN_SCREENSAVER);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(i);
            } else {
                Toast.makeText(getBaseContext(), "Screen Saver is off", Toast.LENGTH_SHORT).show();
                Log.d("CheckingScreenStaus", "Not Displaying....");
            }
        }

        // for soft stopping of thread within the loop that keeps running thread on background too
        public void stopThread() {
            stop = true;
        }
    }

}
