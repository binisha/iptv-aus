package com.zeronebits.iptv.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Ashraf on 22/11/2015.
 */
public class Database extends SQLiteOpenHelper {


    public Database(Context applicationcontext) {
        super(applicationcontext, "total_cable.db", null, 02);
    }

    //Creates Table
    @Override
    public void onCreate(SQLiteDatabase database) {
        String query1,query2,query3;

        query1 = "CREATE TABLE channel_play ( channelId VARCHAR )";
        query2 = "CREATE TABLE channel_category ( channel_cateogry VARCHAR )";
        query3 = "CREATE TABLE channel_position ( channel_position VARCHAR )";

        database.execSQL(query1);
        database.execSQL(query2);
        database.execSQL(query3);

    }


    @Override
    public void onUpgrade(SQLiteDatabase database, int version_old, int current_version) {
        String query1,query2,query3;
        query1 = "DROP TABLE IF EXISTS channel_play";
        query2 = "DROP TABLE IF EXISTS channel_category";
        query3 = "DROP TABLE IF EXISTS channel_position";
        database.execSQL(query1);
        database.execSQL(query2);
        database.execSQL(query3);

        onCreate(database);
    }


    public void insertChannel(String channelId) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("channelId", channelId);
        database.insert("channel_play", null, values);
        database.close();

    }

    public void insertChannelCategory(String channelId) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("channel_cateogry", channelId);
        database.insert("channel_category", null, values);
        database.close();

    }
    public void insertChannelPosition(String position) {
        SQLiteDatabase database = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("channel_position", position);
        database.insert("channel_position", null, values);
        database.close();

    }

    public void deleteChannel() {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from channel_play");
        db.close();
    }
    public void deleteChannelCategory() {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from channel_category");
        db.close();
    }
    public void deleteChannelPosition() {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from channel_position");
        db.close();
    }


    public String getChannelId(){
        String channelId = "";
        SQLiteDatabase database = this.getWritableDatabase();
        String Query = "Select * from channel_play";
        Cursor cursor = database.rawQuery(Query, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                channelId = cursor.getString(0);
               }
        }
        database.close();
        return channelId;
    }


    public String getChannelCategory(){
        String channelId = "";
        SQLiteDatabase database = this.getWritableDatabase();
        String Query = "Select * from channel_category";
        Cursor cursor = database.rawQuery(Query, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                channelId = cursor.getString(0);
            }
        }
        database.close();
        return channelId;
    }


    public String getChannelPosition(){
        String channelId = "";
        SQLiteDatabase database = this.getWritableDatabase();
        String Query = "Select * from channel_position";
        Cursor cursor = database.rawQuery(Query, null);
        if (cursor != null) {
            while (cursor.moveToNext()) {
                channelId = cursor.getString(0);
            }
        }
        database.close();
        return channelId;
    }
}