package com.zeronebits.iptv;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.zeronebits.iptv.util.common.GetMac;


/**
 *activity that shows the registration is successful
 * in which the message is been checked through server and shows successful register
 */

public class RegistrationSuccessful extends Activity {

	private TextView messageView;
	private Button retry;
	
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.unauthorized_access);
		findViewById(R.id.username).setVisibility(View.GONE);
		retry = (Button) findViewById(R.id.retry);
		retry.setText("Re-Launch");
//		button.setText(getString(R.string.login_button_text));
//		button.setVisibility( View.VISIBLE );
		retry.requestFocus();
		retry.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent i = getBaseContext().getPackageManager()
			             .getLaunchIntentForPackage( getBaseContext().getPackageName() );
				i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(i);
				finish();
			}
		});
		
		messageView = (TextView) findViewById(R.id.unauthorized_message);
		messageView.setGravity(android.view.Gravity.LEFT);
		
		String macAddress = GetMac.getMac(this); // Getting mac addresss;
		
		StringBuilder mb = new StringBuilder();
		mb.append( "Your Box ID is " )
			.append(macAddress)
			.append( "\n\n" )
			.append( getString( R.string.register_success_message) )
			.append(  "\n\n" )
			.append( getString( R.string.thanku_message ) )
			.append( ",\n\n" )
			.append( getString( R.string.app_name) )
			.append( " " )
			.append( getString( R.string.team) );
		
		messageView.setText( mb.toString() );
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
	}
	
	
	
}
