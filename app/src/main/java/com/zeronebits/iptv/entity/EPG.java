package com.zeronebits.iptv.entity;

import com.zeronebits.iptv.util.AllParser;

/**
 * Created by sadip_000 on 15/08/2016.
 */
public class EPG extends Channel {

    String program_name, start_time, end_time, day;

public EPG(){}
    public EPG(int channelId){
        Channel channel = AllParser.getChannelFromId(channelId);
        setId(channelId);
        //Channel extensions
        setName(channel.getName());
        setPrice(channel.getchannelPrice());
        setChannelDescription(channel.getChannelDescription());
        setChannelLanguage(channel.getChannelLanguage());
        setChannelPriority(channel.getChannelPriority());
        setImgLink(channel.getImgLink());
        setFavourite(channel.getFavourite());
        setChannelCountry(channel.getChannelCountry());
        setChannelPurchaseStatus(channel.getChannelPurchaseStatus());
        setExpiryDate(channel.getExpiryDate());
        setParentalLock(channel.getParentalLock());

    }
    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public void SetNull() {
        program_name = "N/A";
        start_time = "N/A";
        end_time = "N/A";
        day = "N/A";
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getStart_time())
                .append(" - ")
                .append(getEnd_time())
                .append("\t\t")
                .append(getProgram_name())


        ;
        return sb.toString();
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public String getProgram_name() {
        return program_name;
    }

    public void setProgram_name(String program_name) {
        this.program_name = program_name;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getPrgmTime() {
        return new StringBuilder().
                append(getStart_time())
                .append(" - ")
                .append(getEnd_time()).toString();
    }
}