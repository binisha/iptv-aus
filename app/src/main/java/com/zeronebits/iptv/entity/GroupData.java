package com.zeronebits.iptv.entity;

public class GroupData {

	private int groupId;
	private String groupName;
	private String groupDescription;
	private String groupLogoLink;
	private int activationStatus;
	private int userId;
	private String userEmail;
	private String userName;
	private String displayName;
	private String boxId;
	private int isActive;
	private double userBalance;

	public void setGroupId( int groupId ) {
		this.groupId = groupId;
	}

	public void setGroupName( String groupName ) {
		this.groupName = groupName;
	}

	public void setGroupDescription( String groupDescription ) {
		this.groupDescription = groupDescription;
	}

	public void setGroupLogoLink( String groupLogoLink ) {
		this.groupLogoLink = groupLogoLink;
	}

	public void setActivationStatus( int activationStatus ) {
		this.activationStatus = activationStatus;
	}

	public void setUserId( int userId ) {
		this.userId = userId;
	}

	public void setUserEmail( String userEmail ) {
		this.userEmail = userEmail;
	}

	public void setUserName( String userName ) {
		this.userName = userName;
	}

	public void setDisplayName( String displayName ) {
		this.displayName = displayName;
	}

	public void setBoxId( String boxId ) {
		this.boxId = boxId;
	}

	public void setIsActive( int isActive ) {
		this.isActive = isActive;
	}

	public void setUserBalance( double userBalance ) {
		this.userBalance = userBalance;
	}
	
	
	public int getGroupId() {
		return groupId;
	}
	
	public String getGroupName() {
		return this.groupName;
	}
	
	public String getGroupDescription() {
		return this.groupDescription;
	}
	
	public String getGroupLogoLink() {
		return this.groupLogoLink;
	}
	
	public int getActivationStatus() {
		return this.activationStatus;
	}
	
	public int getUserId() {
		return this.userId;
	}
	
	public String getUserName() {
		return this.userName;
	}
	
	public String getUserEmail() {
		return this.userEmail;
	}
	
	public String getDisplayName() {
		return this.displayName;
	}
	
	public String getBoxId() {
		return this.boxId;
	}
	
	public int getIsActive() {
		return this.isActive;
	}
	
	public double getUserBalance() {
		return this.userBalance;
	}
	
}
