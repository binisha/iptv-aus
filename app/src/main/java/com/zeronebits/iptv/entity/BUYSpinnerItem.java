package com.zeronebits.iptv.entity;

public class BUYSpinnerItem {

	private String duration;

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

}
