package com.zeronebits.iptv.entity;

public class Category {
	private String categoryName;
	private int categoryId;

	public Category() {
	}

	public Category(String categoryName, int categoryId) {
		this.categoryName = categoryName;
		this.categoryId = categoryId;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return this.categoryName;
	}

	public int getCategoryId() {
		return this.categoryId;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("Category[").append("\n\t Category Name: ")
				.append(this.categoryName).append("\n\t Category Id: ")
				.append(this.categoryId).append("\n]");

		return sb.toString();
	}
}
