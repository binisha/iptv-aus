package com.zeronebits.iptv.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class Channel {
	public static final int PURCHASE_DEFAULT = 0;
	public static final int PURCHASE_BUY = 1;
	public static final int PURCHASE_BOUGHT = 2;
	private int id;
	private String name;
	private double price;
	private String channelDescription;
	private String channelLanguage;
	private int channelPriority;
	private String imgLink;
	private int favourite;
	private String channelCountry;
	private int purchase_status;
	private String expiryDate;
	// parental lock in 1 for true 0 for false
	private String parentalLock;
	private String dvrPath;
	private HashMap<String, ArrayList<EPG>> epgHash;
	private Calendar dvrStartCalendar;

	public Calendar getDvrStartCalendar() {
		return dvrStartCalendar;
	}

	public void setDvrStartCalendar(Calendar dvrStartCalendar) {
		this.dvrStartCalendar = dvrStartCalendar;
	}

	public HashMap<String, ArrayList<EPG>> getEpgHash() {
		return epgHash;
	}

	public void setEpgHash(HashMap<String, ArrayList<EPG>> epgHash) {
		this.epgHash = epgHash;
	}

	public Channel() {
		// TODO Auto-generated constructor stub
	}

	public String getParentalLock() {
		return parentalLock;
	}

	public void setParentalLock(String parentalLock) {
		this.parentalLock = parentalLock;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getChannelCountry() {
		return channelCountry;
	}

	public void setChannelCountry(String channelCountry) {
		this.channelCountry = channelCountry;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getchannelPrice() {
		return this.price;
	}

	public String getChannelDescription() {
		return this.channelDescription;
	}

	public void setChannelDescription(String channelDescription) {
		this.channelDescription = channelDescription;
	}

	public String getChannelLanguage() {
		return this.channelLanguage;
	}

	public void setChannelLanguage(String channelLanguage) {
		this.channelLanguage = channelLanguage;
	}

	public int getChannelPriority() {
		return this.channelPriority;
	}

	public void setChannelPriority(int channelPriority) {
		this.channelPriority = channelPriority;
	}

	public String getImgLink() {
//		return "https://" + this.imgLink;
		return this.imgLink;
	}

	public void setImgLink(String imgLink) {
		this.imgLink = imgLink;
	}

	public int getFavourite() {

		return this.favourite;
	}

	public void setFavourite(int favourite) {
		this.favourite = favourite;
	}

	public int getChannelPurchaseStatus() {
		return this.purchase_status;
	}

	public void setChannelPurchaseStatus(int purchaseStatus) {
		this.purchase_status = purchaseStatus;

	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("Channel[").append("\n\t Channel ID: ")
				.append(this.id).append("\n\t Channel Name: ")
				.append(this.name).append("\n\t Channel Description: ")
				.append(this.channelDescription)
				.append("\n\t Channel Language: ").append(this.channelLanguage)
				.append("\n\t Channel Priority: ").append(this.channelPriority)
				.append("\n\t Channel Price: ").append(this.price)
				.append("\n\t Channel Logo Link: ")
				.append(this.imgLink).append("\n\t Channel Fav: ")
				.append(this.favourite).append("\n\t Channel Country: ")
				.append(this.channelCountry)
				.append("\n\t Channel Purchase Status: ")
				.append(this.purchase_status)
				.append("\n\t Channel Expiry Date: ").append(this.expiryDate)
				.append("\n\t Channel parental Lock Status: ")
				.append(this.parentalLock).append("\n]");

		return sb.toString();
	}

	public String getDvrPath() {
		return dvrPath;
	}

	public void setDvrPath(String dvrPath) {
		this.dvrPath = dvrPath;
	}
}
