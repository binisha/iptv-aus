package com.zeronebits.iptv;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.zeronebits.iptv.asyn.CheckIfValidMacAddress;
import com.zeronebits.iptv.asyn.JSONParserValidCheck;
import com.zeronebits.iptv.asyn.PerformTask;
import com.zeronebits.iptv.common.ApkDownloader;
import com.zeronebits.iptv.entity.MarketApp;
import com.zeronebits.iptv.util.MarketAppDetailParser;
import com.zeronebits.iptv.util.SecurityUtils;
import com.zeronebits.iptv.util.common.AppConfig;
import com.zeronebits.iptv.util.common.CustomDialogManager;
import com.zeronebits.iptv.util.common.GetMac;
import com.zeronebits.iptv.util.common.LinkConfig;
import com.zeronebits.iptv.util.common.logger.Logger;


public class EntryPoint extends Activity{
    private static final String TAG = EntryPoint.class.getSimpleName();
    public static String macAddress, username, userId, email,validity;
    public static String display_name, sessionId;
    public static String error_message = "";
    public static String error_code = "";
    public static String platform = "android-stb";

    public static LinearLayout loginLayout;
    public static LinearLayout loadingLayout;
    public static TextView userMacAddressView,versiontxt;
    public static Button btn_login;
    public static EditText et_username, et_password;
    public static String encrypted_password, decrypted_password,version;
    String id,isapproved, isactive;
    private boolean isTaskCancelled = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Logger.d(TAG, "STARTED");
        UrlImageViewHelper.cleanup(getApplicationContext(), 600000);

        loginLayout = (LinearLayout) findViewById(R.id.loginLinearyLayout);
        loadingLayout = (LinearLayout) findViewById(R.id.loadingLinearyLayout);
        userMacAddressView = (TextView) findViewById(R.id.app_version);

        btn_login = (Button) findViewById(R.id.loginButtonPS);
        et_username = (EditText) findViewById(R.id.userNamePS);
        et_password = (EditText) findViewById(R.id.passWordPS);
        et_password.setTypeface(Typeface.DEFAULT);
        versiontxt = (TextView) findViewById(R.id.txt_version_loading);
        et_password.setTransformationMethod(new PasswordTransformationMethod());
        error_message = getString(R.string.err_server_unreachable);

        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(
                    getPackageName(), 0);
            version = pInfo.versionName;
            versiontxt.setText(version);
        } catch (PackageManager.NameNotFoundException e) {
            Logger.printStackTrace(e);
        }

        decrypted_password = et_password.getText().toString().trim();
        //session = new SessionManager(getApplicationContext());
        btn_login.requestFocus();

        if (AppConfig.isDevelopment()) {
            macAddress = AppConfig.getMacAddress();
        } else {
            macAddress = GetMac.getMac(this); // Getting mac addresss
        }

        String mac_check_link = LinkConfig.getString(EntryPoint.this,
                LinkConfig.MAC_EXISTS) + "?mac=" + macAddress;

        Logger.d("MAC CHECK GARNE XUTTAI PARA", mac_check_link);

        new CheckIfValidMacAddress(EntryPoint.this).execute(mac_check_link);


        btn_login.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String uname = et_username.getText().toString().trim();
                String pword = et_password.getText().toString().trim();

                if (uname.length() > 0 && pword.length() > 0) {
                    String URL = LinkConfig.getString(EntryPoint.this,
                            LinkConfig.LOGIN_BUTTON_CLICK)+ "?uname="+ uname+ "&pswd=" + pword + "&boxId=" + macAddress;
                    Logger.d("Login Activity: URL login check", URL);
                    SecurityUtils sUtils = new SecurityUtils();

                    encrypted_password=sUtils.getEncryptedToken(pword);
                    new PerformTask(EntryPoint.this).execute(URL);

                } else {
                    final CustomDialogManager failed = new CustomDialogManager(EntryPoint.this, CustomDialogManager.ALERT);
                    failed.build();
                    failed.setTitle("Login Failed");
                    failed.setMessage("\"Login Failed!\"\n" +
                            "Please Enter the Username and Password");
                    failed.setPositiveButton(getString(R.string.btn_dismiss), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            failed.dismiss();
                        }
                    });


                    failed.show();
                }

            }
        });


    }

    /**
     * this method is used to check the macaddress that is been registerd or not and if not registered call for activity to register macaddress
     */


    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        finish();
    }


    public void apkVerSionCheck() {
        int version;
        try {
            Logger.d("CheckingVersion",getPackageName());

            final MarketApp apk = MarketAppDetailParser.APKs.get(getPackageName());
            version = BuildConfig.VERSION_CODE;

            int versionFromJson = apk.getVersionCode();
            Logger.d("CheckingVersion",getPackageName()+"   "+ version + "   "+versionFromJson);
            if (versionFromJson > version) {
                final CustomDialogManager newVerSionAvailable = new CustomDialogManager(EntryPoint.this, CustomDialogManager.WARNING);
                newVerSionAvailable.build();
                newVerSionAvailable.setMessage(getString(R.string.msg_update));
                newVerSionAvailable.setPositiveButton(getString(R.string.btn_update), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        newVerSionAvailable.dismiss();
                        new ApkDownloader(EntryPoint.this, getString(R.string.app_name)).execute(apk.getAppDownloadLink());
                    }
                });
                newVerSionAvailable.setNegativeButton(getString(R.string.btn_dismiss), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        newVerSionAvailable.dismiss();
                        checkValidUser();
                    }
                });
                newVerSionAvailable.show();
                newVerSionAvailable.getInnerObject().setCancelable(true);
                newVerSionAvailable.getInnerObject().setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {

                        checkValidUser();
                    }
                });

            } else {

                checkValidUser();
            }
        } catch (Exception e) {
            Toast.makeText(EntryPoint.this, "Could not check version!!!", Toast.LENGTH_SHORT).show();

            checkValidUser();
        }

    }



    public void checkValidUser(){

        JSONParserValidCheck validCheck = new JSONParserValidCheck(EntryPoint.this);
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1) {
            validCheck.execute(LinkConfig.getString(EntryPoint.this,LinkConfig.CHECK_VALIDITY_ACTIVATION_APPROVAL)+ "?boxId=" + EntryPoint.macAddress);

        } else {
            validCheck.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, LinkConfig.getString(EntryPoint.this, LinkConfig.CHECK_VALIDITY_ACTIVATION_APPROVAL) + "?boxId=" + EntryPoint.macAddress);
        }
    }
}