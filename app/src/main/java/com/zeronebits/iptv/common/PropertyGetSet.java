package com.zeronebits.iptv.common;

import android.content.Context;
import android.widget.Toast;

import com.zeronebits.iptv.util.common.logger.Logger;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * It is depwndent upon the firmware of android.
 * Properties are set "" by default to the devices that donot support
 * Properties are set to 1 to enable and 0 to disable
 */
public class PropertyGetSet {
	/**
	 * persist.sys.parental.ctrl is for parental control
	 * persist.sys.screensaver.on is for screen-saver
	 * persist.sys.parental.pwd is for the password for parental control
	 * nitv.log.status is not yet used but is used for preparing the log
	 * nitv.video.running is to assure that video is running which always should be opposite to that of screen-saver
	 */
	public static String PARENTAL_CONTROL= "persist.sys.parental.ctrl";
	public static String SCREEN_SAVER= "persist.sys.screensaver.on";
	public static String PARENTAL_PASSWORD="persist.sys.parental.pwd";
	public static String LOG_STATUS="nitv.log.status";
	public static String VIDEO_RUNNING_USAGE="nitv.video.running";

	/**
	 *
	 * @param key may have the values defined above
	 * @param value may be set 0 or 1 in string
	 * @return boolean for success or failure
	 */
	public static boolean setProperty(String key, String value) {

		boolean success = false;

		try {

			Class SystemProperty = Class.forName("android.os.SystemProperties");

			Class[] paramTypes = new Class[2];

			paramTypes[0] = String.class;

			paramTypes[1] = String.class;

			Method set = SystemProperty.getMethod("set", paramTypes);

			Object[] params = new Object[2];

			params[0] = new String(key);

			params[1] = new String(value);

			set.invoke(SystemProperty, params);
			Logger.d("samuel---set", params[0].toString());
			Logger.d("samuel---set", params[1].toString());
			success = true;
			return success;

		} catch (ClassNotFoundException e) {

			// TODO Auto-generated catch block

			e.printStackTrace();

		} catch (NoSuchMethodException e) {

			// TODO Auto-generated catch block

			e.printStackTrace();

		} catch (IllegalAccessException e) {

			// TODO Auto-generated catch block

			e.printStackTrace();

		} catch (IllegalArgumentException e) {

			// TODO Auto-generated catch block

			e.printStackTrace();

		} catch (InvocationTargetException e) {

			// TODO Auto-generated catch block

			e.printStackTrace();

		}

		return success;

	}

	/**
	 * gets the current value of property
	 * @param key is the key of which the value is required
	 * @return the value of the key which may be 0 or 1 in string or ""
	 */
	public static String getProperty(String key) {

		String value = null;

		try {

			Class SystemProperty = Class.forName("android.os.SystemProperties");

			Class[] paramTypes = new Class[1];

			paramTypes[0] = String.class;

			Method get = SystemProperty.getMethod("get", paramTypes);

			Object[] params = new Object[1];

			params[0] = new String(key);

			value = (String) get.invoke(SystemProperty, params);
			Logger.d("value --> sadip ", value + "");
		} catch (NoSuchMethodException e) {

			// TODO Auto-generated catch block

			e.printStackTrace();

		} catch (ClassNotFoundException e) {

			// TODO Auto-generated catch block

			e.printStackTrace();

		} catch (IllegalAccessException e) {

			// TODO Auto-generated catch block

			e.printStackTrace();

		} catch (IllegalArgumentException e) {

			// TODO Auto-generated catch block

			e.printStackTrace();

		} catch (InvocationTargetException e) {

			// TODO Auto-generated catch block

			e.printStackTrace();

		}

		Logger.e("winwin", "value >>>>>>>>>>>>>>>>>>>>>> " + value);


		return value;

	}
	public static void showToast(Context context ,String string) {
		// TODO Auto-generated method stub
		Toast.makeText(context, string, Toast.LENGTH_LONG).show();

	}
}

	