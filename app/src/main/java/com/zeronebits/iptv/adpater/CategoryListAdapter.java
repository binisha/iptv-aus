package com.zeronebits.iptv.adpater;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zeronebits.iptv.R;
import com.zeronebits.iptv.entity.Category;

import java.util.ArrayList;

public class CategoryListAdapter extends ArrayAdapter<Category> {
	private final Activity context;

	int pos;
	private ArrayList<Category> categoryList;

	public CategoryListAdapter(Activity context, ArrayList<Category> categoryList) {
		super(context, R.layout.category_list_row, categoryList);
		this.context = context;
		this.categoryList = categoryList;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;

		if (convertView == null) {

			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.category_list_row, null);

			holder = new ViewHolder();
			holder.categoryNameView = (TextView) convertView.findViewById(R.id.title);
			holder.linear = (LinearLayout) convertView.findViewById(R.id.linear);
			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		if(pos==position){
			Typeface medium = Typeface.createFromAsset(context.getAssets(), "font/Exo2-Medium.otf");
			holder.categoryNameView.setTypeface(medium);
			holder.categoryNameView.setScaleX(1.0f);
			holder.categoryNameView.setScaleY(1.0f);
		}else{
			Typeface light = Typeface.createFromAsset(context.getAssets(), "font/Exo2-Light.otf");
			holder.categoryNameView.setTypeface(light);

			holder.categoryNameView.setScaleX(0.8f);
			holder.categoryNameView.setScaleY(0.8f);
		}

		Category category = categoryList.get(position);
		holder.categoryNameView.setText(category.getCategoryName());
		return convertView;
	}

	public void getPosition(int position) {
		this.pos = position;
	}

	static class ViewHolder {
		TextView categoryNameView;
		LinearLayout linear;
	}

}
