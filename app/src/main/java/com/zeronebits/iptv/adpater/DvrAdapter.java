package com.zeronebits.iptv.adpater;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zeronebits.iptv.R;
import com.zeronebits.iptv.util.EPGUtils.EPGMini;
import com.zeronebits.iptv.util.common.DateUtils;

import java.util.ArrayList;
import java.util.Calendar;


public class DvrAdapter extends ArrayAdapter<EPGMini> {

    private Context context;
    private ArrayList<EPGMini> epgChannelList;
    private int rowLayoutResourceId;

    public DvrAdapter(Context context,
                      int rowLayoutResourceId, ArrayList<EPGMini> channelList) {
        super(context, rowLayoutResourceId, channelList);

        this.context = context;
        this.epgChannelList = channelList;
        this.rowLayoutResourceId = rowLayoutResourceId;
    }

    public void setPosition(int position) {
        this.positionSelected = position;
    }
    private int positionSelected;
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(rowLayoutResourceId, null);

            holder = new ViewHolder();

            holder.LayoutTxtImgHor = (LinearLayout)convertView.findViewById(R.id.layout_txt_img_hor);

            holder.prgmName = (TextView) convertView
                    .findViewById(R.id.txt_prgm_name);
            holder.prgmTime = (TextView) convertView
                    .findViewById(R.id.txt_prgm_time);
            holder.alarmPlay = (ImageView) convertView.findViewById(R.id.ico_alarm_play);

            convertView.setTag(holder);





        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        EPGMini epgMini = epgChannelList.get(position);

        holder.prgmName.setText(epgMini.getPrgmName());
        StringBuilder sbTime = new StringBuilder().append(DateUtils._24HrsTimeFormat.format(epgMini.getStartCalendar().getTime())).append(" - ")
                .append(DateUtils._24HrsTimeFormat.format(epgMini.getEndCalendar().getTime()));
        holder.prgmTime.setText(sbTime.toString());
        if(positionSelected==position){
            Typeface medium = Typeface.createFromAsset(context.getAssets(), "font/Exo2-Medium.otf");
            holder.prgmTime.setTypeface(medium);
            holder.prgmName.setTypeface(medium);
        }else{
            Typeface light = Typeface.createFromAsset(context.getAssets(), "font/Exo2-Light.otf");
            holder.prgmTime.setTypeface(light);
            holder.prgmName.setTypeface(light);
        }

        if(epgMini.getEndCalendar().after(Calendar.getInstance())){
            //onAir
            holder.alarmPlay.setImageResource(R.drawable.on_air);
            holder.LayoutTxtImgHor.setBackgroundColor(getContext().getResources().getColor(R.color.transp));
        }else{
            //dvr
            holder.alarmPlay.setImageResource(R.drawable.icon_play);
            holder.LayoutTxtImgHor.setBackgroundColor(getContext().getResources().getColor(R.color.dvr_transp));
        }
        return convertView;
    }

    static class ViewHolder {
        LinearLayout LayoutTxtImgHor;
        TextView prgmName;
        TextView prgmTime;
        ImageView alarmPlay;
    }

}
