package com.zeronebits.iptv.adpater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.zeronebits.iptv.R;
import com.zeronebits.iptv.entity.Channel;
import com.zeronebits.iptv.util.common.AppConfig;
import com.zeronebits.iptv.util.common.logger.Logger;

import java.util.ArrayList;

/**
 * Created by NITV on 11/08/2016.
 */
public class ChannelListFragmentMainAdapter extends ArrayAdapter<Channel> {

    private Context context;
    private ArrayList<Channel> channelList;
    private int rowLayoutResourceId;
    private int pos = 0;

    public ChannelListFragmentMainAdapter(Context context, int rowLayoutResourceId,
                                          ArrayList<Channel> channelList ) {
        super(context, rowLayoutResourceId, channelList);

        this.context = context;
        this.channelList = channelList;
        this.rowLayoutResourceId = rowLayoutResourceId;
    }

    @Override
    public boolean isEnabled(int position) {
        return true;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if( convertView == null ) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
            convertView = inflater.inflate( rowLayoutResourceId, null);

            holder = new ViewHolder();
//            holder.channelName = (TextView)convertView.findViewById(R.id.txt_title);
            holder.relativeLayout = (RelativeLayout) convertView.findViewById(R.id.relative_layout);
            holder.view = convertView.findViewById(R.id.view);
            holder.fav = (ImageView)convertView.findViewById(R.id.fav);
            holder.channelLogo = (ImageView)convertView.findViewById(R.id.img);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder) convertView.getTag();
        }

        Channel currentChannel = channelList.get(position );
//        holder.channelName.setText(currentChannel.getName());
//        holder.channelName.setVisibility(View.GONE);

        UrlImageViewHelper.setUrlDrawable(holder.channelLogo, currentChannel.getImgLink(), R.drawable.placeholder_logo, AppConfig.CACHE_HOLD_DURATION);
        Logger.d("Image link",currentChannel.getImgLink());

        if(currentChannel.getFavourite() == 1){
            holder.fav.setVisibility(View.VISIBLE);
        }else{
            holder.fav.setVisibility(View.GONE);
        }

        if(pos == position){
            holder.relativeLayout.setBackgroundResource(R.drawable.main_channel_list_selector);
//            holder.relativeLayout.setScaleX(1.25f);
//            holder.relativeLayout.setScaleY(1.2f);
            holder.channelLogo.setScaleX(1.2f);
            holder.channelLogo.setScaleY(1.05f);
            holder.channelLogo.setAlpha(1f);

//            holder.channelName.setTextSize(22);
//            holder.channelName.setTextColor(Color.parseColor("#a9a9a9"));
            holder.view.setVisibility(View.INVISIBLE);
        }else{
            holder.relativeLayout.setBackgroundResource(R.color.transp);

//            holder.relativeLayout.setScaleX(1f);
//            holder.relativeLayout.setScaleY(1f);
            holder.channelLogo.setScaleX(0.8f);
            holder.channelLogo.setScaleY(0.8f);
            holder.channelLogo.setAlpha(0.37f);

//            holder.channelName.setTextSize(20);
//            holder.channelName.setTextColor(Color.parseColor("#a9a9a9"));
            holder.view.setVisibility(View.VISIBLE);
        }


        Logger.d(context.getClass().getName(), context.getClass().getSimpleName());
        return convertView;
    }

    public void setPosition(int position) {
        this.pos = position;
    }

    static class ViewHolder {
        View view;
//        TextView channelName;
        RelativeLayout relativeLayout;
        ImageView fav;
        ImageView channelLogo;
    }
}

