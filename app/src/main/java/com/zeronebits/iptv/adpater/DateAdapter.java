package com.zeronebits.iptv.adpater;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zeronebits.iptv.R;
import com.zeronebits.iptv.util.common.DateUtils;

import java.util.ArrayList;
import java.util.Calendar;


public class DateAdapter extends ArrayAdapter<Calendar> {

    private Context context;
    private ArrayList<Calendar> calendarArrayList;
    private int rowLayoutResourceId;
    private int positionSelected;
    private int positionClicked;

    public DateAdapter(Context context,
                       int rowLayoutResourceId, ArrayList<Calendar> calendarArrayList) {
        super(context, rowLayoutResourceId, calendarArrayList);

positionClicked =0;
        positionSelected = 0;
        this.context = context;
        this.calendarArrayList = calendarArrayList;
        this.rowLayoutResourceId = rowLayoutResourceId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(rowLayoutResourceId, null);

            holder = new ViewHolder();


            holder.prgmDetails = (TextView) convertView
                    .findViewById(R.id.txt_prgm_name);
            holder.layoutTxtImgHor = (LinearLayout) convertView
                    .findViewById(R.id.layout_txt_img_hor);

            convertView.setTag(holder);


        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Calendar c = calendarArrayList.get(position);

        String txtToSet = DateUtils.smalldateFormat.format(c.getTime());

        String inDate = DateUtils.dateFormat.format(c.getTime());
        String curDate = DateUtils.dateFormat.format(Calendar.getInstance().getTime());
        /*if (inDate.equalsIgnoreCase(curDate ) )  {
            holder.prgmName.setBackgroundResource(R.color.red);
        }else {
            holder.prgmName.setBackgroundResource(R.color.no_color);
        }*/
        holder.prgmDetails.setText(txtToSet);


        if(positionClicked==position){
            Typeface medium = Typeface.createFromAsset(context.getAssets(), "font/Exo2-Medium.otf");
            holder.prgmDetails.setTypeface(medium);
            holder.prgmDetails.setTextColor(Color.parseColor("#FFFFFF"));
            holder.layoutTxtImgHor.setBackgroundColor(Color.parseColor("#80013C56"));
            holder.prgmDetails.setScaleX(1.0f);
            holder.prgmDetails.setScaleY(1.0f);
        }else{
            Typeface light = Typeface.createFromAsset(context.getAssets(), "font/Exo2-Light.otf");
            holder.prgmDetails.setTypeface(light);
            holder.prgmDetails.setTextColor(Color.parseColor("#ddefde"));
            holder.layoutTxtImgHor.setBackgroundColor(getContext().getResources().getColor(R.color.no_color));
            holder.prgmDetails.setScaleX(0.85f);
            holder.prgmDetails.setScaleY(0.85f);
        }

        if(positionSelected==position){
            Typeface medium = Typeface.createFromAsset(context.getAssets(), "font/Exo2-Medium.otf");
            holder.prgmDetails.setTypeface(medium);
        }else{
            Typeface light = Typeface.createFromAsset(context.getAssets(), "font/Exo2-Light.otf");
            holder.prgmDetails.setTypeface(light);
        }



        return convertView;
    }

    public void setSelectedPosition(int position) {
        this.positionSelected = position;
    }
    public void setClickedPosition(int position) {
        this.positionClicked = position;
    }

    static class ViewHolder {

        TextView prgmDetails;
        LinearLayout layoutTxtImgHor;
    }
}
