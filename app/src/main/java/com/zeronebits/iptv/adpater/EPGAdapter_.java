package com.zeronebits.iptv.adpater;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zeronebits.iptv.R;
import com.zeronebits.iptv.entity.EPG;
import com.zeronebits.iptv.util.common.DateUtils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


public class EPGAdapter_ extends ArrayAdapter<EPG> {

    private Context context;
    private ArrayList<EPG> epgChannelList;
    private int rowLayoutResourceId;
    private Calendar calendar;

    public EPGAdapter_(Context context,
                       int rowLayoutResourceId, ArrayList<EPG> channelList, Calendar calendar) {
        super(context, rowLayoutResourceId, channelList);

        this.context = context;
        this.epgChannelList = channelList;
        this.rowLayoutResourceId = rowLayoutResourceId;
        this.calendar = calendar;
    }

    public void setPosition(int position) {
        this.positionSelected = position;
    }
    private int positionSelected;
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(rowLayoutResourceId, null);

            holder = new ViewHolder();

            holder.LayoutTxtImgHor = (LinearLayout)convertView.findViewById(R.id.layout_txt_img_hor);

            holder.prgmName = (TextView) convertView
                    .findViewById(R.id.txt_prgm_name);
            holder.prgmTime = (TextView) convertView
                    .findViewById(R.id.txt_prgm_time);
            holder.alarmPlay = (ImageView) convertView.findViewById(R.id.ico_alarm_play);

            convertView.setTag(holder);





        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        EPG currentChannel = epgChannelList.get(position);

        holder.prgmName.setText(currentChannel.getProgram_name());
        holder.prgmTime.setText(currentChannel.getPrgmTime());
        if(positionSelected==position){
            Typeface medium = Typeface.createFromAsset(context.getAssets(), "font/Exo2-Medium.otf");
            holder.prgmTime.setTypeface(medium);
            holder.prgmName.setTypeface(medium);
        }else{
            Typeface light = Typeface.createFromAsset(context.getAssets(), "font/Exo2-Light.otf");
            holder.prgmTime.setTypeface(light);
            holder.prgmName.setTypeface(light);
        }


        String inDate = DateUtils.dateFormat.format(calendar.getTime());
            String curDate = DateUtils.dateFormat.format(Calendar.getInstance().getTime());
            if (inDate.equalsIgnoreCase(curDate)) {
                //today
                String currentTimeInString = DateUtils._24HrsTimeFormat.format(Calendar.getInstance().getTime());
                Date currentTime = null;
                try {
                    currentTime = DateUtils._24HrsTimeFormat.parse(currentTimeInString);
                    Date prgmStartTime = DateUtils._24HrsTimeFormat.parse(currentChannel.getStart_time());
                    Date prgmEndTime = DateUtils._24HrsTimeFormat.parse(currentChannel.getEnd_time());
                    if (prgmStartTime.before(currentTime) && prgmEndTime.after(currentTime)||
                            prgmStartTime==currentTime||
                            prgmEndTime==currentTime) {
                        //onAir
                        holder.alarmPlay.setImageResource(R.drawable.on_air);
                        holder.LayoutTxtImgHor.setBackgroundColor(getContext().getResources().getColor(R.color.transp));

                    } else if (prgmEndTime.before(currentTime)) {
                        //dvr
                        holder.alarmPlay.setImageResource(R.drawable.icon_play);
                        holder.LayoutTxtImgHor.setBackgroundColor(getContext().getResources().getColor(R.color.dvr_transp));
                    }else /*if (prgmStartTime.after(currentTime))*/ {
                        //epg
                        holder.LayoutTxtImgHor.setBackgroundColor(getContext().getResources().getColor(R.color.epg_transp));
                        holder.alarmPlay.setImageResource(R.drawable.icon_alarm);

                    } /*else {
                        holder.alarmPlay.setVisibility(View.INVISIBLE);
                        holder.LayoutTxtImgHor.setBackgroundColor(getContext().getResources().getColor(R.color.no_color));
                    }*/
                } catch (ParseException e) {
                    e.printStackTrace();
                }


            } else if (calendar.compareTo(Calendar.getInstance()) == -1) {
                //before today
                holder.alarmPlay.setImageResource(R.drawable.icon_play);
            } else if (calendar.compareTo(Calendar.getInstance()) == 1) {
                //after today
                holder.alarmPlay.setImageResource(R.drawable.icon_alarm);
            }else {
                /*holder.alarmPlay.setImageResource(R.drawable.on_air);*/
            }
        return convertView;
    }

    static class ViewHolder {
        LinearLayout LayoutTxtImgHor;
        TextView prgmName;
        TextView prgmTime;
        ImageView alarmPlay;
    }

}
