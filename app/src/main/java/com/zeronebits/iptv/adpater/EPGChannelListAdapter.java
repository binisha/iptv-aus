package com.zeronebits.iptv.adpater;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;
import com.zeronebits.iptv.R;
import com.zeronebits.iptv.entity.Channel;
import com.zeronebits.iptv.util.common.AppConfig;

import java.util.ArrayList;

/**
 * Created by NITV on 11/08/2016.
 */
public class EPGChannelListAdapter extends ArrayAdapter<Channel> {

    private Context context;
    private ArrayList<Channel> channelList;
    private int rowLayoutResourceId;
    private int pos = 0;

    public EPGChannelListAdapter(Context context, int rowLayoutResourceId,
                                 ArrayList<Channel> channelList) {
        super(context, rowLayoutResourceId, channelList);

        this.context = context;
        this.channelList = channelList;
        this.rowLayoutResourceId = rowLayoutResourceId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(rowLayoutResourceId, null);
            holder = new ViewHolder();
//            holder.channelName = (TextView) convertView.findViewById(R.id.txt_title);
            holder.imageView = (ImageView) convertView.findViewById(R.id.img);
            holder.relativeLayout = (RelativeLayout)convertView.findViewById(R.id.relativeLayout);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (pos == position) {
            holder.relativeLayout.setBackgroundResource(R.drawable.main_channel_list_selector);

//            holder.channelName.setScaleY(1.2f);
//            holder.channelName.setScaleX(1.2f);
            holder.imageView.setScaleY(1.2f);
            holder.imageView.setScaleX(1.2f);
            holder.imageView.setAlpha(1f);
        } else {
            holder.relativeLayout.setBackgroundResource(R.color.transp);
//            holder.channelName.setScaleY(0.9f);
//            holder.channelName.setScaleX(0.9f);
            holder.imageView.setScaleY(1f);
            holder.imageView.setScaleX(1f);
            holder.imageView.setAlpha(0.5f);
        }
        UrlImageViewHelper.setUrlDrawable(holder.imageView, channelList.get(position).getImgLink(), R.drawable.placeholder_logo, AppConfig.CACHE_HOLD_DURATION);
//        holder.channelName.setText(channelList.get(position).getName());
        return convertView;
    }

    public void setPosition(int position) {
        this.pos = position;
    }

    static class ViewHolder {
        ImageView imageView;
        RelativeLayout relativeLayout;
//        TextView channelName;
    }
}

