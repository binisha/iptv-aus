package com.zeronebits.iptv.asyn;

import android.content.ComponentName;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.view.View;
import android.widget.Toast;


import com.zeronebits.iptv.EntryPoint;
import com.zeronebits.iptv.R;
import com.zeronebits.iptv.util.MarketAppDetailParser;
import com.zeronebits.iptv.util.common.CustomDialogManager;
import com.zeronebits.iptv.util.common.DownloadUtil;
import com.zeronebits.iptv.util.common.LinkConfig;
import com.zeronebits.iptv.util.common.logger.Logger;

import org.json.JSONException;

/**
 * Created by NITV on 21/09/2016.
 */
public class ApkInServer extends AsyncTask<String, String, String> {


    private static int RETRY_COUNT = 0;
    String url;
    EntryPoint entryPoint;

    public ApkInServer(EntryPoint entryPoint){
        this.entryPoint = entryPoint;
    }

    @Override
    protected String doInBackground(String... params) {
        Logger.d(".APKInServer", params[0]);
        DownloadUtil dUtil = new DownloadUtil(params[0], entryPoint);
        url = params[0];
        return dUtil.downloadStringContent();
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (result.equalsIgnoreCase(DownloadUtil.NotOnline) || result.equalsIgnoreCase(DownloadUtil.ServerUnrechable)) {
                final CustomDialogManager noInternet = new CustomDialogManager(entryPoint, CustomDialogManager.ALERT);
                noInternet.build();
                noInternet.setTitle("No Internet");
                noInternet.setMessage("Please Check Your Internet Connection");
                noInternet.getInnerObject().setCancelable(false);
                noInternet.setPositiveButton(entryPoint.getString(R.string.btn_dismiss), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        noInternet.dismiss();
                        entryPoint.finish();
                    }
                });
                noInternet.setExtraButton("Re-Connect", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = entryPoint.getBaseContext().getPackageManager()
                                .getLaunchIntentForPackage(entryPoint.getBaseContext().getPackageName());
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        noInternet.dismiss();
                        entryPoint.startActivity(i);


                    }
                });
                noInternet.setNegativeButton("Settings", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        openSetting();
                        noInternet.dismiss();

                    }
                });
                noInternet.show();

        } else {
            try {
                MarketAppDetailParser parser = new MarketAppDetailParser(
                        entryPoint, result);
                parser.parse();
                entryPoint.apkVerSionCheck();

            } catch (JSONException e1) {
                Logger.printStackTrace(e1);
                MarketAppDetailParser.APKs = null;
                MarketAppDetailParser.packageNames = null;
                //Toast.makeText(entryPoint, "Could not check version!!!", Toast.LENGTH_SHORT).show();

                JSONParserValidCheck validCheck = new JSONParserValidCheck(entryPoint);
                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1) {
                    validCheck
                            .execute(LinkConfig.getString(
                                            entryPoint,
                                            LinkConfig.CHECK_VALIDITY_ACTIVATION_APPROVAL)
                                    + "?boxId=" + EntryPoint.macAddress);

                } else {
                    validCheck.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, LinkConfig.getString( entryPoint, LinkConfig.CHECK_VALIDITY_ACTIVATION_APPROVAL) + "?boxId=" + EntryPoint.macAddress);
                }
            }
        }
    }

    public void openSetting() {
        try {
            try {

                Intent intent = new Intent();
                intent.setComponent(new ComponentName("com.rk_itvui.settings", "com.rk_itvui.settings.Settings"));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                entryPoint.startActivity(intent);
                entryPoint.finish();
            } catch (Exception e) {
                try {
                    Intent LaunchIntent = entryPoint.getPackageManager().getLaunchIntentForPackage("com.giec.settings");
                    entryPoint.startActivity(LaunchIntent);
                    entryPoint.finish();
                } catch (Exception c) {
                    Intent intent = new Intent();
                    intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings"));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    entryPoint.startActivity(intent);
                    entryPoint.finish();
                }
            }


        } catch (Exception a) {
            entryPoint.startActivity(
                    new Intent(Settings.ACTION_SETTINGS));
            entryPoint.finish();
        }
    }

}
