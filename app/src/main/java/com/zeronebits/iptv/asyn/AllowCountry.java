package com.zeronebits.iptv.asyn;

import android.content.Intent;
import android.os.AsyncTask;

import com.zeronebits.iptv.EntryPoint;
import com.zeronebits.iptv.UnauthorizedAccess;
import com.zeronebits.iptv.util.common.CustomDialogManager;
import com.zeronebits.iptv.util.common.DownloadUtil;
import com.zeronebits.iptv.util.common.LinkConfig;
import com.zeronebits.iptv.util.common.logger.Logger;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by NITV on 02/06/2016.
 */
public class AllowCountry extends AsyncTask<Void, Void, String> {

    EntryPoint activity;

    public AllowCountry(EntryPoint activity){
        this.activity = activity;
    }
    @Override
    protected String doInBackground(Void... params) {
        String link = LinkConfig.getString(activity, LinkConfig.ALLOW_COUNTRY);
        System.out.println(link);
        return new DownloadUtil(link, activity).downloadStringContent();
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (result.equals(DownloadUtil.NotOnline) || result.equals(DownloadUtil.ServerUnrechable)) {
            //CustomDialogManager.ReUsedCustomDialogs.noInternet(EntryPoint.this);
        } else {
            try {
                JSONObject jo = new JSONObject(result);
                if (jo.getBoolean("allow")) {
                    Logger.d("AllowCountry","TRUE");

                    new ApkInServer(activity).execute(LinkConfig.LINK_SEVER_APKs+"?mac="+ EntryPoint.macAddress);

                } else {

                    Intent i = new Intent(activity, UnauthorizedAccess.class);
                    i.putExtra("error_code", jo.getString("error_code"));
                    i.putExtra("error_message", jo.getString("error_msg"));
                    i.putExtra("ipAddress", jo.getString("IP"));
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    activity.startActivity(i);
                    activity.finish();

                }
            } catch (JSONException je) {
                Logger.printStackTrace(je);
                CustomDialogManager.dataNotFetched(activity);
            }
        }
    }
}
