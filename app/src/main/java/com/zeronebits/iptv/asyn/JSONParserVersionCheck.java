package com.zeronebits.iptv.asyn;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.view.View;

import com.zeronebits.iptv.EntryPoint;
import com.zeronebits.iptv.R;
import com.zeronebits.iptv.UnauthorizedAccess;
import com.zeronebits.iptv.common.ApkDownloader;
import com.zeronebits.iptv.util.common.CustomDialogManager;
import com.zeronebits.iptv.util.common.DownloadUtil;
import com.zeronebits.iptv.util.common.LinkConfig;
import com.zeronebits.iptv.util.common.logger.Logger;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by NITV on 30/05/2016.
 */
public class JSONParserVersionCheck extends
        AsyncTask<String, String, String> {

    Activity activity = null;
    String TAG = "JSONParseVersionCheck";

    public JSONParserVersionCheck(Activity activity) {
        this.activity = activity;
    }

    @Override
    protected String doInBackground(String... params) {
        Logger.d("EntryPoint: JsonParserVersionCheck", params[0]);
        DownloadUtil dUtil = new DownloadUtil(params[0], activity);

        return dUtil.downloadStringContent();

    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (result.equalsIgnoreCase(DownloadUtil.NotOnline)) {


            final CustomDialogManager noInternet = new CustomDialogManager(activity, CustomDialogManager.ALERT);
            noInternet.build();
            noInternet.setTitle("No Internet");
            noInternet.getInnerObject().setCancelable(false);
            noInternet.setMessage("Please Check Your Internet Connection");
            noInternet.setPositiveButton(activity.getString(R.string.btn_dismiss), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    noInternet.dismiss();
                    activity.finish();
                }
            });
            noInternet.setExtraButton("Re-Connect", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = activity.getBaseContext().getPackageManager()
                            .getLaunchIntentForPackage( activity.getBaseContext().getPackageName() );
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    noInternet.dismiss();
                    activity.startActivity(i);
                }
            });
            noInternet.setNegativeButton("Settings", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openSetting();
                    noInternet.dismiss();

                }
            });
            noInternet.show();

            // server not rechable
        } else if (result.equalsIgnoreCase(DownloadUtil.ServerUnrechable)) {
            Logger.d(TAG, "server_error");
            Intent i = new Intent(activity, UnauthorizedAccess.class);
            i.putExtra("error_code", "");
            i.putExtra("error_message",
                    activity.getString(R.string.err_server_unreachable));
           // i.putExtra("username", username);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            activity.startActivity(i);
            activity.finish();

        } else {

            try {
                JSONObject jObj = new JSONObject(result);
                boolean updateRequired = jObj.getBoolean("updateRequired");

                if (updateRequired) {
                    final String updateLink = jObj
                            .getString("updateLocation");

                    final CustomDialogManager update = new CustomDialogManager(activity, CustomDialogManager.ALERT);
                    update.build();
                    update.setTitle(activity.getString(R.string.app_name));
                    update.getInnerObject().setCancelable(false);
                    update.setMessage(activity.getString(R.string.update_message));
                    update.setPositiveButton("Update", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ApkDownloader installer = new ApkDownloader(
                                    activity);
                            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1) {
                                installer
                                        .execute(updateLink);
                            } else {
                                installer
                                        .executeOnExecutor(
                                                AsyncTask.THREAD_POOL_EXECUTOR,
                                                updateLink);
                            }
                        }
                    });

                    update.setNegativeButton("Skip", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            update.dismiss();
                            JSONParserValidCheck validCheck = new JSONParserValidCheck(activity);
                            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1) {
                                validCheck
                                        .execute(LinkConfig
                                                .getString(
                                                        activity,
                                                        LinkConfig.CHECK_VALIDITY_ACTIVATION_APPROVAL)
                                                + "?boxId=" + EntryPoint.macAddress);
							/*
							 * validCheck.execute(getString(R.string.base_url) +
							 * "check_json_test.php?boxId=" + macAddress);
							 */
                            } else {
                                validCheck
                                        .executeOnExecutor(
                                                AsyncTask.THREAD_POOL_EXECUTOR,
                                                LinkConfig
                                                        .getString(
                                                                activity,
                                                                LinkConfig.CHECK_VALIDITY_ACTIVATION_APPROVAL)
                                                        + "?boxId=" + EntryPoint.macAddress);

							/*
							 * validCheck.executeOnExecutor(
							 * AsyncTask.THREAD_POOL_EXECUTOR,
							 * getString(R.string.base_url) +
							 * "check_json_test.php?boxId=" + macAddress);
							 */
                            }
//								finish();
                        }
                    });
                    update.show();

                } else {
//						if( version.trim().equalsIgnoreCase("1.0.0")||version.trim()=="1.0.0"){
//							System.out.println("I am here");
//							Intent i = new Intent(EntryPoint.this, ComingSoon.class);
//						i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//
//						i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//
//							startActivity(i);
//
//						}
                    JSONParserValidCheck validCheck = new JSONParserValidCheck(activity);
                    if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1) {
                        validCheck
                                .execute(LinkConfig
                                        .getString(
                                                activity,
                                                LinkConfig.CHECK_VALIDITY_ACTIVATION_APPROVAL)
                                        + "?boxId=" + EntryPoint.macAddress);
							/*
							 * validCheck.execute(getString(R.string.base_url) +
							 * "check_json_test.php?boxId=" + macAddress);
							 */
                    } else {
                        validCheck
                                .executeOnExecutor(
                                        AsyncTask.THREAD_POOL_EXECUTOR,
                                        LinkConfig
                                                .getString(
                                                        activity,
                                                        LinkConfig.CHECK_VALIDITY_ACTIVATION_APPROVAL)
                                                + "?boxId=" + EntryPoint.macAddress);

							/*
							 * validCheck.executeOnExecutor(
							 * AsyncTask.THREAD_POOL_EXECUTOR,
							 * getString(R.string.base_url) +
							 * "check_json_test.php?boxId=" + macAddress);
							 */
                    }
                }
            } catch (JSONException e) {
                // Log.e("JSON Parser", "Error parsing data " +
                // e.toString());
            }
        }
    }
    public void openSetting() {
        try {
            try {

                Intent intent = new Intent();
                intent.setComponent(new ComponentName("com.rk_itvui.settings", "com.rk_itvui.settings.Settings"));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(intent);
                activity.finish();
            } catch (Exception e) {
                try {
                    Intent LaunchIntent = activity.getPackageManager().getLaunchIntentForPackage("com.giec.settings");
                    activity.startActivity(LaunchIntent);
                    activity.finish();
                } catch (Exception c) {
                    Intent intent = new Intent();
                    intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings"));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    activity.startActivity(intent);
                    activity.finish();
                }
            }


        } catch (Exception a) {
            activity.startActivity(
                    new Intent(Settings.ACTION_SETTINGS));
            activity.finish();
        }
    }
}