package com.zeronebits.iptv.asyn;

import android.content.ComponentName;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.view.View;

import com.zeronebits.iptv.EntryPoint;
import com.zeronebits.iptv.R;
import com.zeronebits.iptv.UnauthorizedAccess;
import com.zeronebits.iptv.util.common.CustomDialogManager;
import com.zeronebits.iptv.util.common.DownloadUtil;
import com.zeronebits.iptv.util.common.LinkConfig;
import com.zeronebits.iptv.util.common.logger.Logger;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by NITV on 30/05/2016.
 */
public class CheckIfValidMacAddress extends
        AsyncTask<String, String, String> {
    String url = "";
    String TAG = "CheckIfValidMacAddress";
    private String macExists;
    EntryPoint activity;
    String version;
    private boolean isTaskCancelled = false;

    public CheckIfValidMacAddress(EntryPoint activity){
        this.activity = activity;
    }

    public void cancelTask(){
        isTaskCancelled = true;
    }

    private boolean isTaskCancelled(){
        return isTaskCancelled;
    }
    @Override
    protected String doInBackground(String... params) {
        Logger.d(TAG, params[0]);
        url = params[0];
        if (isTaskCancelled()){
            return "nothing";
        }else {
            DownloadUtil dutil = new DownloadUtil(params[0], activity);
            String jsonString = dutil.downloadStringContent();
            return jsonString;
        }
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        // offline
        if (isTaskCancelled()){
        }else {
            if (result.equalsIgnoreCase(DownloadUtil.NotOnline)) {
                Logger.d(TAG, "internet not available");
                final CustomDialogManager noInternet = new CustomDialogManager(activity, CustomDialogManager.ALERT);
                noInternet.build();
                noInternet.setTitle("No Internet");
                noInternet.setMessage("Please Check Your Internet Connection");
                noInternet.getInnerObject().setCancelable(false);
                noInternet.setPositiveButton(activity.getString(R.string.btn_dismiss), new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        noInternet.dismiss();
                        activity.finish();
                    }
                });
                noInternet.setExtraButton("Re-Connect", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent i = activity.getBaseContext().getPackageManager()
                                .getLaunchIntentForPackage(activity.getBaseContext().getPackageName());
                        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        noInternet.dismiss();
                        activity.startActivity(i);


                    }
                });
                noInternet.setNegativeButton("Settings", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        openSetting();
                        noInternet.dismiss();

                    }
                });
                noInternet.show();

                // server not rechable
            } else if (result.equalsIgnoreCase(DownloadUtil.ServerUnrechable)) {
                Logger.d(TAG, "server_error");
                Intent i = new Intent(activity, UnauthorizedAccess.class);
                i.putExtra("error_code", "");
                i.putExtra("error_message",
                        activity.getString(R.string.err_server_unreachable));
                // i.putExtra("username", username);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                activity.startActivity(i);
                activity.finish();

            } else {
                try {
                    JSONObject jObj = new JSONObject(result);
                    macExists = jObj.getString("mac_exists");
                    if (macExists.equals("no")) {

                        JSONObject ob1 = jObj.getJSONObject("message");
                        String error_message = ob1.getString("message_body");
                        String error_code = ob1.getString("error_code");
                        Intent i1 = new Intent(activity,
                                UnauthorizedAccess.class);
                        i1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        i1.putExtra("error_code", error_code);
                        // i1.putExtra("username", username);
                        i1.putExtra("error_message", error_message);
                        i1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        activity.startActivity(i1);
                        activity.finish();
                    } else {
                        System.out.println("I am here hhdh");
                         //new AllowCountry(activity).execute();
                        JSONParserValidCheck validCheck = new JSONParserValidCheck(activity);
                        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.GINGERBREAD_MR1) {
                            validCheck.execute(LinkConfig.getString(activity,LinkConfig.CHECK_VALIDITY_ACTIVATION_APPROVAL)+ "?boxId=" + EntryPoint.macAddress);

                        } else {
                            validCheck.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, LinkConfig.getString(activity, LinkConfig.CHECK_VALIDITY_ACTIVATION_APPROVAL) + "?boxId=" + EntryPoint.macAddress);
                        }
                        //new ApkInServer(activity).execute(LinkConfig.LINK_SEVER_APKs+"?mac="+ EntryPoint.macAddress);

                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    // CustomDialogManager.dataNotFetched(EntryPoint.this);
                }


            }
        }

    }

    public void openSetting() {
        try {
            try {

                Intent intent = new Intent();
                intent.setComponent(new ComponentName("com.rk_itvui.settings", "com.rk_itvui.settings.Settings"));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(intent);
                activity.finish();
            } catch (Exception e) {
                try {
                    Intent LaunchIntent = activity.getPackageManager().getLaunchIntentForPackage("com.giec.settings");
                    activity.startActivity(LaunchIntent);
                    activity.finish();
                } catch (Exception c) {
                    Intent intent = new Intent();
                    intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings"));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    activity.startActivity(intent);
                    activity.finish();
                }
            }


        } catch (Exception a) {
            activity.startActivity(
                    new Intent(Settings.ACTION_SETTINGS));
            activity.finish();
        }
    }
}