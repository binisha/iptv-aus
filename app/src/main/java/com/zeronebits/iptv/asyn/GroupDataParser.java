package com.zeronebits.iptv.asyn;

import com.zeronebits.iptv.entity.GroupData;
import com.zeronebits.iptv.util.common.logger.Logger;

import org.json.JSONObject;

public class GroupDataParser {
	
	private static final String TAG = "com.livechannels.util.GroupDataParser";
	
	private static final String GROUP_NAME = "group_name";
	private static final String GROUP_ID = "group_id";
	private static final String GROUP_LOGO = "group_logo";
	private static final String USER_ID = "user_id";
	private static final String DISPLAY_NAME = "display_name";
	
	private String jsonString;
	public static GroupData groupData;
	
	public GroupDataParser(String jsonString ) {
		this.jsonString = jsonString;
	}
	
	public void parse() {
		try {
			JSONObject root = new JSONObject( jsonString );
			
			JSONObject groupDataArray = root.getJSONArray( "group" ).getJSONObject( 0 );
			
			groupData = new GroupData();
			
			groupData.setGroupId( groupDataArray.getInt( GROUP_ID ) );
			groupData.setGroupName( groupDataArray.getString( GROUP_NAME ) );
			groupData.setGroupLogoLink(groupDataArray.getString( GROUP_LOGO) );
			groupData.setDisplayName( groupDataArray.getString( DISPLAY_NAME ) );
			groupData.setUserId( groupDataArray.getInt( USER_ID ) );
			
		}catch( Exception e ) {
			Logger.d( TAG, "Exception: " + e.getMessage() );
		}
	}
	
	public GroupData getGroupData() {
		return groupData;
	}
}
