package com.zeronebits.iptv.asyn;

import android.app.Activity;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.provider.Settings;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import com.zeronebits.iptv.EntryPoint;
import com.zeronebits.iptv.R;
import com.zeronebits.iptv.UnauthorizedAccess;
import com.zeronebits.iptv.util.common.AlertDialogManager;
import com.zeronebits.iptv.util.common.CustomDialogManager;
import com.zeronebits.iptv.util.common.DownloadUtil;
import com.zeronebits.iptv.util.common.LinkConfig;
import com.zeronebits.iptv.util.common.logger.Logger;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by NITV on 30/05/2016.
 */
public class PerformTask extends AsyncTask<String, String, String> {

    private boolean hasExecption = false;
    private String exceptionMessage = "";
    String error_message = "", error_code = "";
    String userId,username;
//		String encrypted_password;
    Activity activity;
    String isActive;
    AlertDialogManager alertDialogManager = new AlertDialogManager();
    String TAG = "PERFORMTASK";

    public PerformTask(Activity activity){
        this.activity = activity;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (EntryPoint.loginLayout.getVisibility() == View.VISIBLE) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    EntryPoint.loadingLayout.setVisibility(View.VISIBLE);
                    EntryPoint.loginLayout.setVisibility(View.GONE);
                }
            });
        }
    }

    @Override
    protected String doInBackground(String... params) {

        try {
            System.out.println(params[0]);
            DownloadUtil dUtil = new DownloadUtil(params[0], activity);
            String jsonString = dUtil.downloadStringContent();

            return jsonString;

        } catch (Exception e) {
            Logger.printStackTrace(e);
            hasExecption = true;
            exceptionMessage = e.getMessage();
            return null;
        }

    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        if (result.equalsIgnoreCase(DownloadUtil.NotOnline)) {
            Logger.d(TAG, "internet not available");
            final CustomDialogManager noInternet = new CustomDialogManager(activity, CustomDialogManager.ALERT);
            noInternet.build();
            noInternet.setTitle("No Internet");
            noInternet.getInnerObject().setCancelable(false);
            noInternet.setMessage("Please Check Your Internet Connection");
            noInternet.setPositiveButton(activity.getString(R.string.btn_dismiss), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    noInternet.dismiss();
                    activity.finish();
                }
            });
            noInternet.setExtraButton("Re-Connect", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = activity.getBaseContext().getPackageManager()
                            .getLaunchIntentForPackage( activity.getBaseContext().getPackageName() );
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    noInternet.dismiss();
                    activity.startActivity(i);



                }
            });
            noInternet.setNegativeButton("Settings", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openSetting();
                    noInternet.dismiss();

                }
            });
            noInternet.show();

            // server not rechable
        } else if (result.equalsIgnoreCase(DownloadUtil.ServerUnrechable)) {
            Logger.d(TAG, "server_error");
            Intent i = new Intent(activity, UnauthorizedAccess.class);
            i.putExtra("error_code", error_code);
            i.putExtra("error_message",
                    activity.getString(R.string.err_server_unreachable));
            i.putExtra("username",EntryPoint.display_name);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            activity.startActivity(i);
            activity.finish();

        } else {

            if (result != null) {
                JSONObject jObject;
                try {
                    jObject = new JSONObject(result);

                    final JSONArray jArray = jObject.getJSONArray("login");
                    final JSONObject jFObject = jArray.getJSONObject(0);
                    isActive = jFObject.getString("is_active");
                    if (isActive.equals("1")) {
                        EntryPoint.userId = jFObject.getString("user_id");
                        EntryPoint.username = jFObject.getString("display_name");
                        EntryPoint.display_name = EntryPoint.username;
                    } else {
                        JSONObject error = jFObject
                                .getJSONObject("message");
                        error_message = error.getString("message_body");
                        error_code = error.getString("error_code");
                    }
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    Logger.printStackTrace(e);
                    hasExecption = true;
                    exceptionMessage = e.getMessage();
                }
            }

            if (hasExecption) {
                activity.runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        EntryPoint.loadingLayout.setVisibility(View.GONE);
                        EntryPoint.loginLayout.setVisibility(View.VISIBLE);
                    }
                });

                alertDialogManager.showAlertDialog(activity, "Login Failed!!!","Unexpected exception occured \n"+ exceptionMessage, false);
            } else {

                if (isActive.equals("1")) {
                    // Creating the myfile and writing the datas :
                    // macaddress,username, password and idValue
                    EntryPoint.email = EntryPoint.et_username.getText().toString().trim()
                            + "";

                    // FileAuthentication.LoginButtonFileCreate(macAddress,usernameEe,
                    // email,encrypted_password, id);

                    /*Log.e("written to file", macAddress + "\n" + username
                            + "\n" + email + "\n" + encrypted_password
                            + "\n" + userId);*/

						/*FileAuthentication.rewriteUserInfo(macAddress,
								email, encrypted_password, userId);*/
//						g macAddress, validity, username, userId;

                    // login successful but get groupdata and session id
                    // first
                    // and forward to
                    // app


                   new GetSession(activity).execute(LinkConfig.getString(
                            activity, LinkConfig.GET_SESSION)
                            + "?"
                            + "platform="
                            + EntryPoint.platform
                            + "&uname="
                            + EntryPoint.et_username.getText() + "&boxId=" + EntryPoint.macAddress);
                    isActive = "-4";
                } else if (isActive.equals("-4")) {
                    activity.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            EntryPoint.loadingLayout.setVisibility(View.GONE);
                            EntryPoint.loginLayout.setVisibility(View.VISIBLE);
                        }
                    });

                    final Dialog d = new Dialog(activity);
                    d.setContentView(R.layout.condn_boxid);
                    d.setTitle(activity.getResources().getString(R.string.app_name) + ":      Login Failed...");
                    ((TextView) d.findViewById(R.id.boxid)).setText(EntryPoint.macAddress);
                    ((TextView) d.findViewById(R.id.boxid2)).setText("BoxId: " + EntryPoint.macAddress);
                    ((TextView) d.findViewById(R.id.error_code)).setText("Error Code: " + error_code);
                    ((TextView) d.findViewById(R.id.error_msg)).setText(error_message + "");
                    ((TextView) d.findViewById(R.id.app_version)).setText("V" + EntryPoint.version + "");
                    d.show();
                    d.setOnKeyListener(new DialogInterface.OnKeyListener() {

                        @Override
                        public boolean onKey(DialogInterface dialog,
                                             int keyCode, KeyEvent event) {
                            d.dismiss();
                            return true;
                        }
                    });
                } else {
                    Intent i = new Intent(activity,
                            UnauthorizedAccess.class);
                    i.putExtra("error_code", error_code);
                    i.putExtra("error_message", error_message);
                    Logger.e("display_name", EntryPoint.display_name + "");
                    i.putExtra("username", EntryPoint.display_name);
//						i.putExtra("email", email);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    activity.startActivity(i);
                    activity.finish();

                }
            }
        }
    }

    public void openSetting() {
        try {
            try {

                Intent intent = new Intent();
                intent.setComponent(new ComponentName("com.rk_itvui.settings", "com.rk_itvui.settings.Settings"));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(intent);
                activity.finish();
            } catch (Exception e) {
                try {
                    Intent LaunchIntent = activity.getPackageManager().getLaunchIntentForPackage("com.giec.settings");
                    activity.startActivity(LaunchIntent);
                    activity.finish();
                } catch (Exception c) {
                    Intent intent = new Intent();
                    intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings"));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    activity.startActivity(intent);
                    activity.finish();
                }
            }


        } catch (Exception a) {
            activity.startActivity(
                    new Intent(Settings.ACTION_SETTINGS));
            activity.finish();
        }
    }
}