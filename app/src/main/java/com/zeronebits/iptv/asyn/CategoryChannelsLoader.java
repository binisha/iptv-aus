package com.zeronebits.iptv.asyn;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.os.AsyncTask;
import android.provider.Settings;
import android.view.View;

import com.zeronebits.iptv.EntryPoint;
import com.zeronebits.iptv.R;
import com.zeronebits.iptv.UnauthorizedAccess;
import com.zeronebits.iptv.VideoPlayer.TVPlayCustomController_;
import com.zeronebits.iptv.util.AllParser;
import com.zeronebits.iptv.util.common.CustomDialogManager;
import com.zeronebits.iptv.util.common.DownloadUtil;
import com.zeronebits.iptv.util.common.LinkConfig;
import com.zeronebits.iptv.util.common.logger.Logger;

import org.json.JSONException;


/**
 * Created by NITV on 30/05/2016.
 */
public class CategoryChannelsLoader extends
        AsyncTask<String, Void, String> {
    String TAG = "CategoryChannelIsLoader";
    Activity activity;

    public CategoryChannelsLoader(Activity activity){
        this.activity = activity;
    }
    @Override
    protected String doInBackground(String... params) {
        Logger.d(TAG, params[0]);

//			DownloadUtil dUtil = new DownloadUtil(params[0], EntryPoint.this);
//
//			return dUtil.downloadStringContent();
        DownloadUtil getUtc = new DownloadUtil(LinkConfig.GET_UTC,activity);
        String utc = getUtc.downloadStringContent();
        System.out.println(utc);
        if (!utc.equals(DownloadUtil.NotOnline)) {

            System.out.println("I am  here");
            DownloadUtil dUtil = new DownloadUtil(params[0]
                    + LinkConfig.getHashCode(utc), activity);
            Logger.d(TAG, params[0] + LinkConfig.getHashCode(utc));
            return dUtil.downloadStringContent();
        } else
            System.out.println("I am not here");
            return utc;
    }

    @Override
    protected void onPostExecute(String result) {

        if (result.equalsIgnoreCase(DownloadUtil.NotOnline)) {
            Logger.d(TAG, "internet not available");
            final CustomDialogManager noInternet = new CustomDialogManager(activity, CustomDialogManager.ALERT);
            noInternet.build();
            noInternet.setTitle("No Internet");
            noInternet.getInnerObject().setCancelable(false);
            noInternet.setMessage("Please Check Your Internet Connection");
            noInternet.setPositiveButton(activity.getString(R.string.btn_dismiss), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    noInternet.dismiss();
                    activity.finish();
                }
            });
            noInternet.setExtraButton("Re-Connect", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = activity.getBaseContext().getPackageManager()
                            .getLaunchIntentForPackage( activity.getBaseContext().getPackageName() );
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    noInternet.dismiss();
                    activity.startActivity(i);




                }
            });
            noInternet.setNegativeButton("Settings", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openSetting();
                    noInternet.dismiss();

                }
            });
            noInternet.show();

            // server not rechable
        } else if (result.equalsIgnoreCase(DownloadUtil.ServerUnrechable)) {
            Logger.d(TAG, "server_error");
            Intent i = new Intent(activity, UnauthorizedAccess.class);
            //i.putExtra("error_code", error_code);
            i.putExtra("error_message",
                    activity.getString(R.string.err_server_unreachable));
            i.putExtra("username", EntryPoint.display_name);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            activity.startActivity(i);
            activity.finish();

        } else {///yo code pani bujena

            AllParser parser = new AllParser(activity, result);

            try {
                parser.parse();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (AllParser.categorynames.size() < 3) {// excluding the
                // category
                // ALL and Favourite
                Intent i = new Intent(activity,
                        UnauthorizedAccess.class);
                i.putExtra(
                        "error_message",
                       activity.getString(R.string.uner_maintainence));
                i.putExtra("error_code", "100");
                i.putExtra("username", EntryPoint.display_name);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                activity.startActivity(i);
                activity.finish();

            } else {
                Intent i1 = new Intent(activity, TVPlayCustomController_.class);
                i1.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i1.putExtra("version", EntryPoint.version);
                i1.putExtra("sessionId", EntryPoint.sessionId);
                i1.putExtra("email", EntryPoint.email);
                i1.putExtra("platform", EntryPoint.platform);
                activity.startActivity(i1);
                activity.finish();
            }


        }
    }


    public void openSetting() {
        try {
            try {

                Intent intent = new Intent();
                intent.setComponent(new ComponentName("com.rk_itvui.settings", "com.rk_itvui.settings.Settings"));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(intent);
                activity.finish();
            } catch (Exception e) {
                try {
                    Intent LaunchIntent = activity.getPackageManager().getLaunchIntentForPackage("com.giec.settings");
                    activity.startActivity(LaunchIntent);
                    activity.finish();
                } catch (Exception c) {
                    Intent intent = new Intent();
                    intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings"));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    activity.startActivity(intent);
                    activity.finish();
                }
            }


        } catch (Exception a) {
            activity.startActivity(
                    new Intent(Settings.ACTION_SETTINGS));
            activity.finish();
        }
    }


}
