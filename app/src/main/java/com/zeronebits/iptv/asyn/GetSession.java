package com.zeronebits.iptv.asyn;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.os.AsyncTask;
import android.provider.Settings;
import android.util.Log;
import android.view.View;

import com.zeronebits.iptv.EntryPoint;
import com.zeronebits.iptv.R;
import com.zeronebits.iptv.UnauthorizedAccess;
import com.zeronebits.iptv.util.common.CustomDialogManager;
import com.zeronebits.iptv.util.common.DownloadUtil;
import com.zeronebits.iptv.util.common.FileAuthentication;
import com.zeronebits.iptv.util.common.LinkConfig;
import com.zeronebits.iptv.util.common.logger.Logger;

import org.json.JSONException;
import org.json.JSONObject;

import static com.zeronebits.iptv.EntryPoint.macAddress;
import static com.zeronebits.iptv.EntryPoint.sessionId;
import static com.zeronebits.iptv.EntryPoint.userId;

/**
 * Created by NITV on 30/05/2016.
 */
public class GetSession extends AsyncTask<String, Void, String> {

    /**

     accessing the season id before passing the login activity

     * @param params session url
     * @return session Id
     */

    Activity activity;
    String TAG = "GETSESSION";
    public GetSession(Activity activity){
        this.activity = activity;
    }

    @Override
    protected String doInBackground(String... params) {
        Logger.e("GET SESSION URL", params[0] + "");
        DownloadUtil dutil = new DownloadUtil(params[0],activity);
        String json = dutil.downloadStringContent();
        return json;
    }

    @Override
    protected void onPostExecute(String result) {
        if (result.equalsIgnoreCase(DownloadUtil.NotOnline)) {
            Logger.d(TAG, "internet not available");
            final CustomDialogManager noInternet = new CustomDialogManager(activity, CustomDialogManager.ALERT);
            noInternet.build();
            noInternet.getInnerObject().setCancelable(false);
            noInternet.setTitle("No Internet");
            noInternet.setMessage("Please Check Your Internet Connection");
            noInternet.setPositiveButton(activity.getString(R.string.btn_dismiss), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    noInternet.dismiss();
                    activity.finish();
                }
            });
            noInternet.setExtraButton("Re-Connect", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = activity.getBaseContext().getPackageManager()
                            .getLaunchIntentForPackage( activity.getBaseContext().getPackageName() );
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    noInternet.dismiss();
                    activity.startActivity(i);
                }
            });
            noInternet.setNegativeButton("Settings", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openSetting();
                    noInternet.dismiss();
                }
            });
            noInternet.show();
            // server not rechable
        } else {
            if (result.equalsIgnoreCase(DownloadUtil.ServerUnrechable)) {
                Logger.d(TAG, "server_error");
                Intent i = new Intent(activity, UnauthorizedAccess.class);
                //i.putExtra("error_code", EntryPoint.error_code);
                i.putExtra("error_message",
                        activity.getString(R.string.err_server_unreachable));
                i.putExtra("username", EntryPoint.display_name);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                activity.startActivity(i);
                activity.finish();

            } else {
                try {
                    JSONObject jobject = new JSONObject(result);
                    sessionId = jobject.getString("session");


                    Log.d("herau", macAddress+userId+sessionId);

                    FileAuthentication.reWriteLoginDetailsToFile(macAddress, EntryPoint.username,
                            EntryPoint.email, EntryPoint.encrypted_password, sessionId, userId);


                    Logger.d("CheckingFileAuthocaitioan", macAddress + " " + EntryPoint.username + " " +
                            EntryPoint.email + " " + EntryPoint.encrypted_password + " " + sessionId + " " +
                            userId);
                    new GroupDataLoader(activity).execute(LinkConfig.getString(
                            activity, LinkConfig.GROUP_DATA)
                            + "?userID=" + userId+"&boxId="+ macAddress);

                } catch (JSONException e) {
                    Logger.printStackTrace(e);
                }


            }
        }

    }
    public void openSetting() {
        try {
            try {

                Intent intent = new Intent();
                intent.setComponent(new ComponentName("com.rk_itvui.settings", "com.rk_itvui.settings.Settings"));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(intent);
                activity.finish();
            } catch (Exception e) {
                try {
                    Intent LaunchIntent = activity.getPackageManager().getLaunchIntentForPackage("com.giec.settings");
                    activity.startActivity(LaunchIntent);
                    activity.finish();
                } catch (Exception c) {
                    Intent intent = new Intent();
                    intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings"));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    activity.startActivity(intent);
                    activity.finish();
                }
            }


        } catch (Exception a) {
            activity.startActivity(
                    new Intent(Settings.ACTION_SETTINGS));
            activity.finish();
        }
    }

}
