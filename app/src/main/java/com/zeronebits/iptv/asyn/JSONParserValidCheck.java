package com.zeronebits.iptv.asyn;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.Settings;
import android.view.View;

import com.zeronebits.iptv.EntryPoint;
import com.zeronebits.iptv.R;
import com.zeronebits.iptv.UnauthorizedAccess;
import com.zeronebits.iptv.util.SecurityUtils;
import com.zeronebits.iptv.util.common.CustomDialogManager;
import com.zeronebits.iptv.util.common.DownloadUtil;
import com.zeronebits.iptv.util.common.FileAuthentication;
import com.zeronebits.iptv.util.common.LinkConfig;
import com.zeronebits.iptv.util.common.logger.Logger;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

/**
 * Created by NITV on 30/05/2016.
 */
public class JSONParserValidCheck extends AsyncTask<String, String, String> {

    JSONObject jObj = null;
    private String id;
    private String isactive = "0", isapproved = "0";
    String TAG = "JSONParserValidCheck";
    Activity activity;

    public JSONParserValidCheck(Activity activity){
        this.activity =  activity;
        //this.validCheckResponse = activity;
    }


//    public JSONParserValidCheck(ValidCheckResponse validCheckResponse){
//        this.validCheckResponse=validCheckResponse;
//    }
    @Override
    protected String doInBackground(String... params) {

        Logger.d("EntryPoint: JSONParserValidCheck", params[0]);
        DownloadUtil dUtil = new DownloadUtil(params[0], activity);
        String jsonString = dUtil.downloadStringContent();

//			Log.d(TAG, jsonString+"herau");

        return jsonString;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if (result.equalsIgnoreCase(DownloadUtil.NotOnline)) {
            final CustomDialogManager noInternet = new CustomDialogManager(activity, CustomDialogManager.ALERT);
            noInternet.build();
            noInternet.getInnerObject().setCancelable(false);
            noInternet.setTitle("No Internet");
            noInternet.setMessage("Please Check Your Internet Connection");
            noInternet.setPositiveButton(activity.getString(R.string.btn_dismiss), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    noInternet.dismiss();
                    activity.finish();
                }
            });
            noInternet.setExtraButton("Re-Connect", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = activity.getBaseContext().getPackageManager()
                            .getLaunchIntentForPackage( activity.getBaseContext().getPackageName() );
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    noInternet.dismiss();
                    activity.startActivity(i);
                }
            });
            noInternet.setNegativeButton("Settings", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openSetting();
                    noInternet.dismiss();

                }
            });
            noInternet.show();

            // server not rechable
        } else if (result.equalsIgnoreCase(DownloadUtil.ServerUnrechable)) {
            Logger.d(TAG, "server_error");
            Intent i = new Intent(activity, UnauthorizedAccess.class);
            i.putExtra("error_code", "");
            i.putExtra("error_message",
                    activity.getString(R.string.err_server_unreachable));
            //i.putExtra("username", username);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            activity.startActivity(i);
            activity.finish();

        } else {

            try {
                JSONObject jObj = new JSONObject(result);

                EntryPoint.validity = jObj.getString("valid");
                if (EntryPoint.validity.equals("1")) {
                    EntryPoint.username = jObj.getString("user_name");
                    EntryPoint.display_name = jObj.getString("display_name");
                    Logger.e("display name while parsing", EntryPoint.display_name + "");
                    try {
                        id = jObj.getString("user_id");
                        isapproved = jObj.getString("activation_status");
                        isactive = jObj.getString("is_active");

                    } catch (JSONException je) {
                        Logger.printStackTrace(je);
                        JSONObject message = jObj.getJSONObject("message");
                        EntryPoint.error_code = message.getString("error_code");
                        EntryPoint.error_message = message.getString("message_body");
                    }

                    if (!isactive.equals("1") || !isapproved.equals("1")) {
                        JSONObject message = jObj.getJSONObject("message");
                        EntryPoint.error_code = message.getString("error_code");
                        EntryPoint.error_message = message.getString("message_body");
                    }
                }else{


                    JSONObject message = jObj.getJSONObject("message");
                    EntryPoint.error_code = message.getString("error_code");
                    EntryPoint.error_message = message.getString("message_body");


                    FileAuthentication.checkFileExists();
                    Intent i3 = new Intent(activity,
                            UnauthorizedAccess.class);
                    i3.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    i3.putExtra("error_code",  EntryPoint.error_code);
                    i3.putExtra("error_message", message.getString("error_code"));
                    i3.putExtra("username",  "Not Activated");

                    activity.startActivity(i3);
                    activity.finish();
                }

            } catch (JSONException e) {
                EntryPoint.validity = null;
            }

            String checkValidity =  EntryPoint.validity;
            EntryPoint.et_username.setText( EntryPoint.username);
            if (checkValidity == null) {
                checkValidity = "0";
            }
            // mac address exists but has not been assigned to any user so
            // register user
            if (checkValidity.equals("-3")) {
                try {

                    JSONObject message = jObj.getJSONObject("message");
                    EntryPoint.error_code = message.getString("error_code");
                    EntryPoint.error_message = message.getString("message_body");


                FileAuthentication.checkFileExists();
                Intent i3 = new Intent(activity,
                        UnauthorizedAccess.class);
                i3.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i3.putExtra("error_code",  EntryPoint.error_code);

                i3.putExtra("error_message", message.getString("error_code"));

                activity.startActivity(i3);
                activity.finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
            // everything except expiry is fine so start procedure to login
            else if (checkValidity.equals("1") && isactive.equals("1")
                    && isapproved.equals("1")) {
//                EntryPoint.userMacAddressView.setText( EntryPoint.display_name + " | "
//                        + EntryPoint.macAddress);

                File externalStorageDir = Environment
                        .getExternalStorageDirectory();
                File myFile = new File(externalStorageDir,
                        FileAuthentication.APP_CONFIG_FILE_NAME);
                System.out.println(myFile+"ma aye");

                if (myFile.exists()) {
                    // try to login from file
                    Logger.e(myFile.getName(), "exists");
                    try {
                        if (!FileAuthentication  .checkMountAndFile( EntryPoint.macAddress))
                            FileAuthentication.setUserNameandId();
                        EntryPoint.encrypted_password = FileAuthentication
                                .getUserPassword() + "";

                        Logger.d(TAG, EntryPoint.encrypted_password + " enc");

                        SecurityUtils sUtils = new SecurityUtils();

                        EntryPoint.decrypted_password = sUtils.getDecryptedToken(EntryPoint.encrypted_password);

                        Logger.d(TAG, "##" + EntryPoint.decrypted_password);

                        String URL = LinkConfig.getString(activity,
                                LinkConfig.LOGIN_BUTTON_CLICK)
                                + "?uname="
                                +  EntryPoint.username
                                + "&pswd="
                                + EntryPoint.decrypted_password
                                + "&boxId=" +  EntryPoint.macAddress;

							/*
							 * String URL = getString(R.string.base_url) +
							 * "login_json_full_test.php?uname=" +
							 * FileAuthentication.getUserEmail() + "&pswd=" +
							 * decrypted_password + "&boxId=" + macAddress;
							 */
                        Logger.d("Login Activity: URL login check", URL);
                        Logger.d("Login Activity: encrypt password", EntryPoint.encrypted_password);

                        new PerformTask(activity).execute(URL);

                    } catch (Exception e) {
                        Logger.printStackTrace(e);
                        EntryPoint.loadingLayout.setVisibility(View.GONE);
                        EntryPoint.loginLayout.setVisibility(View.VISIBLE);
                        EntryPoint.et_password.requestFocus();
                    }
                } else {
                    // file not found so login from details provided by user
                    EntryPoint.loadingLayout.setVisibility(View.GONE);
                    EntryPoint.loginLayout.setVisibility(View.VISIBLE);
                    EntryPoint.et_password.requestFocus();
                }

            } else {
                // not approved or not activated so show error message
                FileAuthentication.checkFileExists();
                Intent i3 = new Intent(activity,
                        UnauthorizedAccess.class);
                i3.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i3.putExtra("error_code",  EntryPoint.error_code);
                i3.putExtra("error_message",  EntryPoint.error_message);
                Logger.e("display name befre pass",  EntryPoint.display_name + "");
                i3.putExtra("username",  EntryPoint.display_name + "");
                activity.startActivity(i3);
                activity.finish();
            }
        }
    }

    public void openSetting() {
        try {
            try {

                Intent intent = new Intent();
                intent.setComponent(new ComponentName("com.rk_itvui.settings", "com.rk_itvui.settings.Settings"));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(intent);
                activity.finish();
            } catch (Exception e) {
                try {
                    Intent LaunchIntent = activity.getPackageManager().getLaunchIntentForPackage("com.giec.settings");
                    activity.startActivity(LaunchIntent);
                    activity.finish();
                } catch (Exception c) {
                    Intent intent = new Intent();
                    intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings"));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    activity.startActivity(intent);
                    activity.finish();
                }
            }


        } catch (Exception a) {
            activity.startActivity(
                    new Intent(Settings.ACTION_SETTINGS));
            activity.finish();
        }
    }
}
