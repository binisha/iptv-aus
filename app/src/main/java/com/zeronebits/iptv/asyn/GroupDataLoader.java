package com.zeronebits.iptv.asyn;

import android.app.Activity;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.provider.Settings;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import com.zeronebits.iptv.EntryPoint;
import com.zeronebits.iptv.R;
import com.zeronebits.iptv.UnauthorizedAccess;
import com.zeronebits.iptv.entity.GroupData;
import com.zeronebits.iptv.util.common.CustomDialogManager;
import com.zeronebits.iptv.util.common.DownloadUtil;
import com.zeronebits.iptv.util.common.LinkConfig;
import com.zeronebits.iptv.util.common.logger.Logger;



/**
 * Created by NITV on 30/05/2016.
 */

public class GroupDataLoader extends AsyncTask<String, Void, String> {

    String TAG = "GROUPDATALOADER";
    Activity activity;

    public GroupDataLoader(Activity activity){
        this.activity = activity;
    }
    @Override
    protected String doInBackground(String... params) {

        Logger.d(TAG, params[0]);

        DownloadUtil dUtil = new DownloadUtil(params[0], activity);

        return dUtil.downloadStringContent();

    }

    @Override
    protected void onPostExecute(String result) {
        if (result.equalsIgnoreCase(DownloadUtil.NotOnline)) {
            Logger.d(TAG, "internet not available");
            final CustomDialogManager noInternet = new CustomDialogManager(activity, CustomDialogManager.ALERT);
            noInternet.build();
            noInternet.setTitle("No Internet");
            noInternet.getInnerObject().setCancelable(false);
            noInternet.setMessage("Please Check Your Internet Connection");
            noInternet.setPositiveButton(activity.getString(R.string.btn_dismiss), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    noInternet.dismiss();
                    activity.finish();
                }
            });
            noInternet.setExtraButton("Re-Connect", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = activity.getBaseContext().getPackageManager()
                            .getLaunchIntentForPackage( activity.getBaseContext().getPackageName() );
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    noInternet.dismiss();
                    activity.startActivity(i);




                }
            });
            noInternet.setNegativeButton("Settings", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openSetting();
                    noInternet.dismiss();
                }
            });
            noInternet.show();

            // server not rechable
        } else if (result.equalsIgnoreCase(DownloadUtil.ServerUnrechable)) {
            Logger.d(TAG, "server_error");
            Intent i = new Intent(activity, UnauthorizedAccess.class);
           // i.putExtra("error_code", error_code);
            i.putExtra("error_message",
                    activity.getString(R.string.err_server_unreachable));
            i.putExtra("username", EntryPoint.display_name);
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            activity.startActivity(i);
            activity.finish();

        } else {
            try {
                GroupDataParser parser = new GroupDataParser(result);
                parser.parse();
                GroupData groupData = GroupDataParser.groupData;
                String categoryUrl = LinkConfig.getString(activity,
                        LinkConfig.CATEGORY_URL);

                String category_channel_url = LinkConfig.getString(activity, LinkConfig.CATEGORY_CHANNEL_URL);
                category_channel_url += "?groupId=" + GroupDataParser.groupData.getGroupId()+ "&userId="
                        + GroupDataParser.groupData.getUserId();
//                Intent i = new Intent(activity, TVPlayCustomController_.class);
//                activity.startActivity(i);

                new CategoryChannelsLoader(activity).execute(categoryUrl
                        + "?groudID=" + groupData.getGroupId() + "&userId="
                        + groupData.getUserId()+"&");

            } catch (Exception e) {
                Logger.printStackTrace(e);
                Dialog d = new Dialog(activity);

                d.setContentView(R.layout.condn_boxid);
                d.setTitle(activity.getResources().getString(R.string.app_name));

                TextView BoxId = (TextView) d.findViewById(R.id.boxid);
                TextView BoxId2 = (TextView) d.findViewById(R.id.boxid2);
                TextView App_Version = (TextView) d
                        .findViewById(R.id.app_version);
                TextView ErrorCode = (TextView) d
                        .findViewById(R.id.error_code);
                TextView ErrorMsg = (TextView) d
                        .findViewById(R.id.error_msg);
                ErrorCode.setText("Unknown Error");
                ErrorMsg.setText("If you continue to see this error please contact your service provider...");
                BoxId.setText("Box Id: " + EntryPoint.macAddress);
                BoxId2.setText("Box Id: " + EntryPoint.macAddress);

                try {
                    PackageInfo pInfo = activity.getPackageManager().getPackageInfo(
                            activity.getPackageName(), 0);
                    String versionName = pInfo.versionName;

                    App_Version.setText(versionName);
                } catch (PackageManager.NameNotFoundException nnfe) {
                    Logger.printStackTrace(e);
                }
                d.show();
                d.setOnKeyListener(new DialogInterface.OnKeyListener() {

                    @Override
                    public boolean onKey(DialogInterface dialog,
                                         int keyCode, KeyEvent event) {
                        // TODO Auto-generated method stub
                       activity.finish();
                        return false;
                    }
                });

            }
        }

    }

    public void openSetting() {
        try {
            try {

                Intent intent = new Intent();
                intent.setComponent(new ComponentName("com.rk_itvui.settings", "com.rk_itvui.settings.Settings"));
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                activity.startActivity(intent);
                activity.finish();
            } catch (Exception e) {
                try {
                    Intent LaunchIntent = activity.getPackageManager().getLaunchIntentForPackage("com.giec.settings");
                    activity.startActivity(LaunchIntent);
                    activity.finish();
                } catch (Exception c) {
                    Intent intent = new Intent();
                    intent.setComponent(new ComponentName("com.android.settings", "com.android.settings.Settings"));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    activity.startActivity(intent);
                    activity.finish();
                }
            }


        } catch (Exception a) {
            activity.startActivity(
                    new Intent(Settings.ACTION_SETTINGS));
            activity.finish();
        }
    }

}
