package com.zeronebits.iptv;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.zeronebits.iptv.util.common.AppConfig;
import com.zeronebits.iptv.util.common.GetMac;
import com.zeronebits.iptv.util.common.logger.Logger;


public class UnauthorizedAccess extends Activity {

	private TextView messageView,txt_username, version;
	private Button retry, finishBut;
	private String username,macAddress="",error_code="",error_message="", ipAddress = "";
	private StringBuilder mb = new StringBuilder();
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.unauthorized_access);
		messageView = (TextView) findViewById(R.id.unauthorized_message);
		txt_username = (TextView) findViewById(R.id.username);
		version= (TextView) findViewById(R.id.version);
		retry =(Button)findViewById(R.id.retry);

		retry.requestFocus();


		if (AppConfig.isDevelopment()) {
			macAddress = AppConfig.getMacAddress();
		} else {
			macAddress = GetMac.getMac(this); // Getting mac addresss
		}
		try {
			PackageInfo pInfo = getPackageManager().getPackageInfo(
					getPackageName(), 0);
			String versionName = pInfo.versionName;

			version.setText(versionName);
		} catch (PackageManager.NameNotFoundException e) {
			Logger.printStackTrace(e);
		}

		error_code=getIntent().getStringExtra("error_code");
		error_message = getIntent().getStringExtra("error_message");
		Logger.e(error_code+"code", error_message+"");
		try{
			username = getIntent().getStringExtra("username");
			System.out.println(username);
			if(username==null|| username.equals("null")) {
				System.out.println("I am here");
				txt_username.setVisibility(View.INVISIBLE);

			}

			else {
				txt_username.setVisibility(View.VISIBLE);
				txt_username.setText(username);

			}
//			txt_username.setVisibility(View.VISIBLE);
			Logger.e("username", username);
		}catch(Exception e){
			txt_username.setVisibility(View.GONE);
		}

		try {
			ipAddress = getIntent().getStringExtra("ipAddress");
			if(ipAddress!=null ){
				mb.append("Ip Adress:\t").append(ipAddress).append("\n\n");
			}
		} catch (Exception e) {
			Logger.printStackTrace(e);
			ipAddress = "";
		}

		if(error_code.equals(""))
			mb.append("Box ID: "+ macAddress. toUpperCase())
					.append( "\n\n" )
					.append( error_message );

		else
			mb.append("Box ID: "+ macAddress. toUpperCase())
					.append( "\n" )
					.append("Error Code: "+ error_code)
					.append("\n")
					.append( error_message );

		messageView.setText( mb.toString() );
		retry.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {

				finish();
			}
		});
	}


	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		finish();
	}
}
